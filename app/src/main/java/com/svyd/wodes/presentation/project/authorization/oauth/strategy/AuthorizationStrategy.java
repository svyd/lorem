package com.svyd.wodes.presentation.project.authorization.oauth.strategy;

import android.content.Intent;

import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.project.authorization.model.OauthRequest;

/**
 * Created by Svyd on 18.07.2016.
 */
public interface AuthorizationStrategy {

    void logIn(BaseFragment _fragment, OauthCallback _callback);
    void onActivityResult(int _resultCode, int _requestCode, Intent _intent);

    interface OauthCallback {
        void onSuccess(OauthRequest request);
        void onError(Throwable error);
    }

}
