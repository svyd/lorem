package com.svyd.wodes.presentation.base;

import android.view.View;

/**
 * Created by Svyd on 05.07.2016.
 */
public interface OnItemClickListener<T> {
    void onItemClick(int index, T item, View view);
}
