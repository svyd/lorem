package com.svyd.wodes.presentation.project.delivery.all.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.OnItemClickListener;
import com.svyd.wodes.presentation.project.delivery.base.BaseDeliveryAdapter;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 24.08.2016.
 */
public class DeliveryAdapter extends BaseDeliveryAdapter {

    public DeliveryAdapter(Activity context) {
        super(context);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.list_item_delivery;
    }
}