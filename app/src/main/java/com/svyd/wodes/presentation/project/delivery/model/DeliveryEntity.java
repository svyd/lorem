package com.svyd.wodes.presentation.project.delivery.model;

import com.google.gson.annotations.SerializedName;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Svyd on 08.09.2016.
 */
public class DeliveryEntity implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("visits")
    private int visits;

    @SerializedName("startDate")
    private String startDate;

    @SerializedName("from")
    private SimpleLocationModel fromLocation;

    @SerializedName("to")
    private SimpleLocationModel toLocation;

    @SerializedName("points")
    private List<float[]> points;

    @SerializedName("size")
    private SizeModel size;

    @SerializedName("owner")
    private ProfileInfo owner;

    public String getId() {
        return id;
    }

    public int getVisits() {
        return visits;
    }

    public List<float[]> getPoints() {
        return points;
    }

    public SimpleLocationModel getFromLocation() {
        return fromLocation;
    }

    public SimpleLocationModel getToLocation() {
        return toLocation;
    }

    public ProfileInfo getOwner() {
        return owner;
    }

    public SizeModel getSize() {
        return size;
    }

    public String getDescription() {
        return description;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getTitle() {
        return title;
    }

    public void setOwner(ProfileInfo owner) {
        this.owner = owner;
    }
}
