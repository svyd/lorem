package com.svyd.wodes.presentation.project.delivery.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Svyd on 08.09.2016.
 */
public class SizeModel implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("deliveryType")
    private String transport;

    @SerializedName("name")
    private String name;

    @SerializedName("parcelType")
    private String parcelSize;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getParcelSize() {
        return parcelSize;
    }

    public String getTransport() {
        return transport;
    }
}
