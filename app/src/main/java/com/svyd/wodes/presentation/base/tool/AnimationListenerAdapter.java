package com.svyd.wodes.presentation.base.tool;

import android.view.animation.Animation;

/**
 * Created by Svyd on 14.08.2016.
 */
public abstract class AnimationListenerAdapter implements Animation.AnimationListener {
    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }
}
