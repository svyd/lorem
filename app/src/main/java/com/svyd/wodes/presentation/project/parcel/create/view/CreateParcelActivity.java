package com.svyd.wodes.presentation.project.parcel.create.view;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.tool.RevealUtil;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.base.PopupListAdapter;
import com.svyd.wodes.presentation.project.delivery.base.SimpleTextWatcher;
import com.svyd.wodes.presentation.project.home.image_picker.ImagePickerFragment;
import com.svyd.wodes.presentation.project.parcel.create.contract.ICreateParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.create.contract.ICreateParcelView;
import com.svyd.wodes.presentation.project.parcel.create.presenter.CreateParcelPresenterFactory;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Svyd on 14.09.2016.
 */
public class CreateParcelActivity extends BaseActivity implements ICreateParcelView, DatePickerDialog.OnDateSetListener, ImagePickerFragment.ImageListener {

    public static final int REQUEST_CREATE_PARCEL = 2123;
    public static final int REQUEST_EDIT_PARCEL = 2124;

    @BindView(R.id.llHeaderContainer_ACP)
    LinearLayout llHeaderContainer;

    @BindView(R.id.appbar_ACP)
    AppBarLayout appbar;

    @BindView(R.id.collapsing_toolbar_ACP)
    CollapsingToolbarLayout toolbarLayout;

    @BindView(R.id.toolbar_ACP)
    Toolbar mToolbar;

    @BindView(R.id.etDeparture_ACP)
    EditText etDeparture;

    @BindView(R.id.etImage_ACP)
    EditText etImage;

    @BindView(R.id.etArrival_ACP)
    EditText etArrival;

    @BindView(R.id.etTitle_ACP)
    EditText etTitle;

    @BindView(R.id.etStartDate_ACP)
    EditText etStartDate;

    @BindView(R.id.etSize_ACP)
    EditText etSize;

    @BindView(R.id.pbDeparture_ACP)
    ProgressBar pbDeparture;

    @BindView(R.id.pbArrival_ACP)
    ProgressBar pbArrival;

    @BindView(R.id.etDescription_ACP)
    EditText etDescription;

    @BindView(R.id.pbSizes_ACP)
    ProgressBar pbSizes;

    @BindView(R.id.etPrice_ACP)
    EditText etPrice;

    @BindView(R.id.clContent_ACP)
    CoordinatorLayout clContent;

    private TextView mToolbarTextView;
    private AdapterView.OnItemClickListener mDepartureListener;
    private AdapterView.OnItemClickListener mArrivalListener;
    private AdapterView.OnItemClickListener mSizesListener;
    private ICreateParcelPresenter mPresenter;
    private TextWatcher mDepartureTextWatcher;
    private TextWatcher mArrivalTextWatcher;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_parcel);
        initListeners();
        enableBackButton(true);
        ButterKnife.bind(this);
        initUi();
        initHeader();
        initPresenter();
        if (checkLollipop()) {
            makeReveal();
        }
    }

    private void makeReveal() {

        if (!getIntent().hasExtra("x"))
            return;

        int x = getIntent().getIntExtra("x", 0);
        int y = getIntent().getIntExtra("y", 0);
        if (x == 0) {
            return;
        }
        RevealUtil.makeReveal(clContent, x, y);
    }

    private void initUi() {
        mProgress = new ProgressDialog(this);
        mProgress.setTitle(getString(R.string.txt_wait));
        mProgress.setMessage(getString(R.string.txt_create_delivery_progress));
        mProgress.setCancelable(false);
        etArrival.addTextChangedListener(mArrivalTextWatcher);
        etDeparture.addTextChangedListener(mDepartureTextWatcher);
    }

    private void initPresenter() {
        mPresenter = new CreateParcelPresenterFactory().providePresenter(getIntent().getExtras(), this);
        mPresenter.initialize();
    }

    private void initListeners() {
        mDepartureListener = (parent, view, position, id) ->
                mPresenter.onDepartureItemSelected(position);

        mArrivalListener = (parent, view, position, id) ->
                mPresenter.onArrivalItemSelected(position);

        mSizesListener = (parent, view, position, id) ->
                mPresenter.onSizeSelected(position);

        mDepartureTextWatcher = new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPresenter.onDepartureTextChanged(s.toString());
            }
        };
        mArrivalTextWatcher = new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPresenter.onArrivalTextChanged(s.toString());
            }
        };
    }

    private void initHeader() {
        mToolbarTextView = getTitleTextView(mToolbar);
        appbar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float percentage = ((float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange());
            llHeaderContainer.setAlpha(Math.abs(percentage - 1));
            mToolbarTextView.setScaleX(percentage);
            mToolbarTextView.setScaleY(percentage);
            getWindow().setStatusBarColor(Color.argb((int) (percentage * 255), 68, 138, 255));
            float min = 0.88f;
            float max = 1;
            if (percentage > min) {
                mToolbarTextView.setAlpha(Math.abs(percentage - min) * (max / (max - min)));
            } else {
                mToolbarTextView.setAlpha(0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.miNext_AI) {
            mPresenter.onNextClick();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public int getContainerId() {
        return 0;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_ACP;
    }

    @Override
    public void showArrivalPopup(List<String> places) {
        showListMenu(etArrival, places, mArrivalListener, false);
    }

    @Override
    public void showDeparturePopup(List<String> places) {
        showListMenu(etDeparture, places, mDepartureListener, false);
    }

    @Override
    public void showArrivalProgress(boolean show) {
        pbArrival.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showDepartureProgress(boolean show) {
        pbDeparture.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showDatePicker(int year, int month, int day) {
        DatePickerDialog.newInstance(this, year, month, day)
                .show(getFragmentManager(), DatePickerDialog.class.getSimpleName());
    }

    @Override
    public void showSizePicker(List<String> sizes) {
        showListMenu(etSize, sizes, mSizesListener, true);
    }

    @Override
    public void setDate(String date) {
        etStartDate.setText(date);
    }

    @Override
    public void setSize(String size) {
        dismissListPopup();
        etSize.setText(size);
    }

    @Override
    public void showPublishAlert() {
        showAcceptDialog();
    }

    @Override
    public void setDeparture(String text) {
        etDeparture.removeTextChangedListener(mDepartureTextWatcher);
        dismissListPopup();
        etDeparture.setText(text);
        etDeparture.addTextChangedListener(mDepartureTextWatcher);
    }

    @Override
    public void setArrival(String text) {
        etArrival.removeTextChangedListener(mArrivalTextWatcher);
        dismissListPopup();
        etArrival.setText(text);
        etArrival.addTextChangedListener(mArrivalTextWatcher);
    }

    @Override
    public void showSizesProgress(boolean show) {
        pbSizes.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void navigateBackWithResult(ParcelItem item) {
        Intent intent = new Intent();
        intent.putExtra(Constants.Extra.EXTRA_PARCEL, item);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void showError(String text) {
        Snackbar.make(clContent, text, Snackbar.LENGTH_SHORT);
    }

    @Override
    public void showImagePicker() {
        ImagePickerFragment.showForActivity(getSupportFragmentManager());
    }

    @Override
    public void setPhotoAttached(String text) {
        etImage.setText(text);
    }

    @Override
    public void setToolbarTitle(String stringResource) {
        setTitle(stringResource);
    }

    @Override
    public void setLocationEnabled(boolean enabled) {
        etArrival.setFocusable(enabled);
        etArrival.setClickable(enabled);
        etArrival.setCursorVisible(enabled);
        etArrival.setEnabled(enabled);
        etDeparture.setFocusable(enabled);
        etDeparture.setClickable(enabled);
        etDeparture.setCursorVisible(enabled);
        etDeparture.setEnabled(enabled);
    }

    @Override
    public void setItemTitle(String title) {
        etTitle.setText(title);
    }

    @Override
    public void setDescription(String description) {
        etDescription.setText(description);
    }

    @Override
    public void setPrice(String price) {
        etPrice.setText(price);
    }

    @Override
    public void showProgress() {
        mProgress.show();

    }

    @Override
    public void hideProgress() {
        mProgress.cancel();

    }

    @OnClick(R.id.vDateSelector_ACP)
    void onDateClick() {
        mPresenter.onDateClick();
    }

    @OnClick(R.id.vSizeSelector_ACP)
    void onSizeClick() {
        mPresenter.onSizeClick();
    }

    @OnClick(R.id.vImageSelector_ACP)
    void onPhotoClick() {
        mPresenter.onPhotoClick();
    }

    @OnTextChanged(R.id.etPrice_ACP)
    void onPriceChanged(CharSequence price) {
        mPresenter.onPriceTextChanged(price.toString());
    }

    @OnTextChanged(R.id.etTitle_ACP)
    void onTitleChanged(CharSequence s) {
        mPresenter.onTitleTextChanged(s.toString());
    }

    @OnTextChanged(R.id.etDescription_ACP)
    void onDescriptionChanged(CharSequence s) {
        mPresenter.onDescriptionTextChanged(s.toString());
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        new TimePickerDialog(
                this,
                (v, hour, minute) -> {
                    calendar.set(Calendar.HOUR_OF_DAY, hour);
                    calendar.set(Calendar.MINUTE, minute);
                    mPresenter.onDepartureDateChanged(calendar);
                },
                0,
                0,
                DateFormat.is24HourFormat(this)
        ).show();

    }

    void showAcceptDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.alert_title_create_delivery)
                .setMessage(R.string.alert_message_create_delivery)
                .setCancelable(false)
                .setPositiveButton(R.string.accept, (dialog1, which) -> {
                    mPresenter.onAccept();
                    dialog1.dismiss();
                })
                .setNegativeButton(R.string.decline,
                        (dialog, id) -> {
                            dialog.cancel();
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onImageReceived(String path) {
        mPresenter.onPhotoAttached(path);
    }
}
