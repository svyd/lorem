package com.svyd.wodes.presentation.project.profile.edit.contract;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;

/**
 * Created by Svyd on 30.08.2016.
 */
public interface IEditProfilePresenter extends BasePresenter {
    void onNameChanged(String name);
    void onNumberChanged(String number);
    void onEmailChanged(String email);
    void onAvatarClick();
    void onGenderMaleSelected();
    void onGenderFemaleSelected();
    void setAvatarPath(String path);
    void onBirthdayChanged(int _year, int _month, int _day);
    void onDone();
    void onBirthdayClick();
}
