package com.svyd.wodes.presentation.project.parcel.create.contract;

import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Svyd on 14.09.2016.
 */
public interface ICreateParcelView extends BaseView {
    void showArrivalPopup(List<String> places);
    void showDeparturePopup(List<String> places);
    void showArrivalProgress(boolean show);
    void showDepartureProgress(boolean show);
    void showDatePicker(int year, int month, int day);
    void showSizePicker(List<String> sizes);
    void setDate(String date);
    void setSize(String size);
    void showPublishAlert();
    void setDeparture(String text);
    void setArrival(String text);
    void showSizesProgress(boolean show);
    void navigateBackWithResult(ParcelItem item);
    void showError(String stringResource);
    void showImagePicker();
    void setPhotoAttached(String text);
    void setToolbarTitle(String stringResource);
    void setLocationEnabled(boolean enabled);
    void setItemTitle(String title);
    void setDescription(String description);
    void setPrice(String price);
}
