package com.svyd.wodes.presentation.base;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by Svyd on 21.09.2016.
 */
public class Utils {

    private static float sHeight = -1;

    public static float getScreenHeight(Activity context) {
        if (sHeight == -1) {
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            sHeight = metrics.heightPixels;
        }
        return sHeight;
    }
}
