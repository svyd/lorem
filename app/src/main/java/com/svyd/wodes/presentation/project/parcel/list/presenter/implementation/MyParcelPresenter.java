package com.svyd.wodes.presentation.project.parcel.list.presenter.implementation;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.parcel.create.view.CreateParcelActivity;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

/**
 * Created by Svyd on 14.09.2016.
 */
public class MyParcelPresenter extends AbstractParcelPresenter {

    public MyParcelPresenter(IParcelView view, PostInteractor<ListRequest> interactor, PostInteractor<SearchRequest> searchInteractor) {
        super(view, interactor, searchInteractor);
    }

    @Override
    public void onAdd() {
        getView().navigateToCreate();
    }

    @Override
    public void handleResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CreateParcelActivity.REQUEST_CREATE_PARCEL:
                    onAddItem(data);
                    break;
                case CreateParcelActivity.REQUEST_EDIT_PARCEL:
                    onEditItem(data);
                    break;
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(MenuInflater inflater, Menu menu) {
        inflater.inflate(R.menu.add_item, menu);

    }

    private void onEditItem(Intent data) {
        ParcelItem item = (ParcelItem) data.getSerializableExtra(Constants.Extra.EXTRA_PARCEL);
        int position = data.getIntExtra(Constants.Extra.EXTRA_POSITION, -1);
        getView().editItem(position, item);
    }

    private void onAddItem(Intent data) {
        ParcelItem item = (ParcelItem) data.getSerializableExtra(Constants.Extra.EXTRA_PARCEL);
        getView().addItem(item);
        getView().hideStubText();
    }

    @Override
    public void initialize() {
        ListRequest request = new ListRequest();
        request.setType(Constants.ITEM_TYPE_MY);
        request.setLimit(10);
        request.setOffset(0);
        setRequest(request);
        requestItems();
        getView().setToolbarTitle("My parcels");
    }
}
