package com.svyd.wodes.presentation.project.parcel.matched.presenter;

import android.os.Bundle;

import com.google.gson.Gson;
import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.ListDataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.dialog.DialogMapper;
import com.svyd.wodes.data.dialog.DialogRepository;
import com.svyd.wodes.data.dialog.DialogService;
import com.svyd.wodes.data.dialog.IDialogRepository;
import com.svyd.wodes.data.dialog.model.DialogEntity;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.data.parcel.mapper.ParcelMapper;
import com.svyd.wodes.data.parcel.mapper.ParcelsMapper;
import com.svyd.wodes.data.parcel.repository.IParcelRepository;
import com.svyd.wodes.data.parcel.repository.ParcelRepository;
import com.svyd.wodes.data.parcel.repository.ParcelService;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.dialog.InviteInteractor;
import com.svyd.wodes.domain.parcel.FindParcelInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.dialog.list.model.DialogItem;
import com.svyd.wodes.presentation.project.parcel.matched.contract.IMatchedParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.matched.contract.IMatchedParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelEntity;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import java.util.List;

/**
 * Created by Svyd on 20.09.2016.
 */
public class MatchedParcelPresenterFactory implements PresenterFactory<IMatchedParcelPresenter, IMatchedParcelView> {

    @Override
    public IMatchedParcelPresenter providePresenter(Bundle args, IMatchedParcelView view) {
        return new MatchedParcelPresenter(args, view, provideFindInteractor(), provideInviteInteractor());
    }

    private PostInteractor<InviteRequest> provideInviteInteractor() {
        return new InviteInteractor(provideDialogRepository());
    }
    private IDialogRepository provideDialogRepository() {
        return new DialogRepository(provideDialogService(), provideDialogMapper());
    }


    private TypeMapper<ListDataWrapper<DialogEntity>, List<DialogItem>> provideDialogMapper() {
        return new DialogMapper();
    }

    private DialogService provideDialogService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), DialogService.class);
    }

    private PostInteractor<SearchRequest> provideFindInteractor() {
        return new FindParcelInteractor(provideParcelRepository());
    }

    private IParcelRepository provideParcelRepository() {
        return new ParcelRepository(new Gson(),
                provideParcelService(),
                provideParcelsMapper(),
                provideParcelMapper());
    }

    private TypeMapper<ParcelEntity, ParcelItem> provideParcelMapper() {
        return new ParcelMapper();
    }

    TypeMapper<DataWrapper<List<ParcelEntity>>, List<ParcelItem>> provideParcelsMapper() {
        return new ParcelsMapper(new ParcelMapper());
    }

    private ParcelService provideParcelService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), ParcelService.class);
    }

}
