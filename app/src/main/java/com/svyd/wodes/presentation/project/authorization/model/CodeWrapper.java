package com.svyd.wodes.presentation.project.authorization.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Svyd on 18.08.2016.
 */
public class CodeWrapper implements Serializable {

    @SerializedName("code")
    private String code;

    public CodeWrapper(String code) {
        this.code = code;
    }
}
