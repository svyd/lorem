package com.svyd.wodes.presentation.project.delivery.details.contract;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import java.util.List;

/**
 * Created by Svyd on 11.09.2016.
 */
public interface IDeliveryDetailsView extends BaseView {
    void drawRoute(List<LatLng> points);
    void setBounds(LatLngBounds bounds);
    void addMarker(MarkerOptions options);
    void showMap();
    void setStart(String text);
    void setDeviation(String text);
    void setDepartureDate(String text);
    void setDepartureCity(String text);
    void setArrivalDate(String text);
    void setArrivalCity(String text);
    void setAvailable(String text);
    void setDescription(String text);
    void setOwnerName(String text);
    void setVisits(String text);
    void setDeliveredParcels(String text);
    void setLastVisit(String text);
    void setUserSince(String text);
    void setAvatar(String text);
    void navigateToEdit(DeliveryItem item);
    void setButtonText(String text);
    void showOwner(boolean show);
    void navigateToOwner(ProfileInfo owner);
    void setResult(DeliveryItem mItem);
    void setViewResult();
    void navigateToMatchedParcels(SearchRequest request, InviteRequest inviteRequest);
    void showAlert();
    void navigateToMyParcels(InviteRequest request);
}
