package com.svyd.wodes.presentation.base.tool;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;

import com.svyd.wodes.presentation.application.WodesApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Svyd on 05.09.2016.
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
public class BitmapReducer {

    public File getReducedBitmap(String source, int desiredMaxSide) {

        ContextWrapper cw = new ContextWrapper(WodesApplication.getContext());
        File directory = cw.getDir("media", Context.MODE_PRIVATE);

        File destination = new File(directory.getPath() + "/" + "avatar");
        Bitmap sourceBitmap = BitmapFactory.decodeFile(source);
        FileOutputStream stream = null;

        try {
            if (!destination.exists()) {
                destination.createNewFile();
            }
            stream = new FileOutputStream(destination);

            Bitmap reducedBitmap = ThumbnailUtils.extractThumbnail(
                    sourceBitmap,
                    desiredMaxSide,
                    desiredMaxSide - desiredMaxSide / 4);

            reducedBitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);

            stream.flush();
            stream.close();

            return destination;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
