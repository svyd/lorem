package com.svyd.wodes.presentation.project.profile.edit.contract;

import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

/**
 * Created by Svyd on 30.08.2016.
 */
public interface IEditProfileView extends BaseView {
    void onProfileUpdated(ProfileInfo info);
    void setName(String name);
    void setNumber(String number);
    void setEmail(String email);
    void setAvatar(String path);
    void setBirthday(String birthday);
    void setGenderFemale();
    void setGenderMale();
    void showImagePicker();
    void showDatePicker(int _year, int _month, int _day);
}
