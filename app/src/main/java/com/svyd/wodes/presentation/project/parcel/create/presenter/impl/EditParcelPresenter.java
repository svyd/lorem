package com.svyd.wodes.presentation.project.parcel.create.presenter.impl;

import android.os.Bundle;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.Interactor;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.parcel.create.contract.ICreateParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelRequest;

import rx.Observer;

/**
 * Created by Svyd on 20.09.2016.
 */
public class EditParcelPresenter extends AbstractCreateParcelPresenter {

    private ParcelItem mItem;
    private PostInteractor<ParcelRequest> mInteractor;

    public EditParcelPresenter(Bundle args, PostInteractor<ParcelRequest> interactor, ICreateParcelView view, PostInteractor<String> placesInteractor, PostInteractor<ParcelRequest> requestInteractor, Interactor sizeInteractor) {
        super(view, placesInteractor, requestInteractor, sizeInteractor);
        mItem = (ParcelItem) args.getSerializable(Constants.Extra.EXTRA_PARCEL);
        mInteractor = interactor;
    }

    @Override
    public void initialize() {
        super.initialize();
        initView();
        initRequest();
    }

    private void initRequest() {
        getRequest().setId(mItem.getEntity().getId());
    }

    @Override
    public void onNextClick() {
        getView().showProgress();
        mInteractor.execute(getRequest(), new EditObserver());
    }

    private void initView() {
        getView().setToolbarTitle(WodesApplication.getStringResource(R.string.edit_parcel));
        getView().setLocationEnabled(false);
        getView().setDeparture(mItem.getEntity().getFromLocation().getCity());
        getView().setArrival(mItem.getEntity().getToLocation().getCity());
        getView().setItemTitle(mItem.getEntity().getTitle());
        getView().setDescription(mItem.getEntity().getDescription());
        getView().setSize(mItem.getEntity().getSize().getName());
        getView().setDate(mItem.getDeliveryTime());
        getView().setPrice(mItem.getEntity().getPrice());
        if (mItem.getEntity().getImage() != null) {
            getView().setPhotoAttached(WodesApplication.getStringResource(R.string.photo_attached));
        }
    }

    private class EditObserver implements Observer<ParcelItem> {

        @Override
        public void onCompleted() {
            getView().hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            getView().hideProgress();
        }

        @Override
        public void onNext(ParcelItem item) {
            getView().navigateBackWithResult(item);
        }
    }
}