package com.svyd.wodes.presentation.project.delivery.my.presenter;

import android.os.Bundle;

import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.ListDataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.delivery.DeliveriesMapper;
import com.svyd.wodes.data.delivery.DeliveryMapper;
import com.svyd.wodes.data.delivery.DeliveryRepository;
import com.svyd.wodes.data.delivery.DeliveryService;
import com.svyd.wodes.data.dialog.DialogMapper;
import com.svyd.wodes.data.dialog.DialogRepository;
import com.svyd.wodes.data.dialog.DialogService;
import com.svyd.wodes.data.dialog.IDialogRepository;
import com.svyd.wodes.data.dialog.model.DialogEntity;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.data.profile.mapper.ProfileMapper;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.delivery.DeliveryInteractor;
import com.svyd.wodes.domain.dialog.InviteInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryEntity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.delivery.my.contract.IMyDeliveryPresenter;
import com.svyd.wodes.presentation.project.delivery.my.contract.IMyDeliveryView;
import com.svyd.wodes.presentation.project.dialog.list.model.DialogItem;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import java.util.List;

/**
 * Created by Svyd on 07.09.2016.
 */
public class MyDeliveryPresenterFactory implements PresenterFactory<IMyDeliveryPresenter, IMyDeliveryView> {
    @Override
    public IMyDeliveryPresenter providePresenter(Bundle args, IMyDeliveryView view) {

        int flow = args.getInt(Constants.Flow.FLOW_KEY);

        switch (flow) {
            case Constants.Flow.CHOOSE_ITEM:
                return new ChooseDeliveryPresenter(view, args, provideInteractor(), provideInviteInteractor());
            case Constants.Flow.SHOW_ITEMS:
                return new MyDeliveryPresenter(view, provideInteractor());
            default:
                throw new IllegalArgumentException("Unsupported flow: " + flow);
        }
    }

    private PostInteractor<InviteRequest> provideInviteInteractor() {
        return new InviteInteractor(provideDialogRepository());
    }

    private IDialogRepository provideDialogRepository() {
        return new DialogRepository(provideDialogService(), provideDialogMapper());
    }


    private TypeMapper<ListDataWrapper<DialogEntity>, List<DialogItem>> provideDialogMapper() {
        return new DialogMapper();
    }

    private DialogService provideDialogService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), DialogService.class);
    }

    PostInteractor<ListRequest> provideInteractor() {
        DeliveryService service = new ServiceProvider().provideWodesService(WodesApplication.getContext(), DeliveryService.class);
        return new DeliveryInteractor(new DeliveryRepository(service, null, provideDeliveryMapper(), provideDeliveriesMapper()));
    }

    private TypeMapper<DataWrapper<List<DeliveryEntity>>, List<DeliveryItem>> provideDeliveriesMapper() {
        return new DeliveriesMapper(provideDeliveryMapper());
    }

    private TypeMapper<DeliveryEntity, DeliveryItem> provideDeliveryMapper() {
        return new DeliveryMapper(provideProfileMapper());
    }

    private TypeMapper<ProfileInfo, ProfileInfo> provideProfileMapper() {
        return new ProfileMapper();
    }
}
