package com.svyd.wodes.presentation.project.parcel.list.presenter.implementation;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.filtering.view.FilteringActivity;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.domain.base.PostInteractor;

import java.util.List;

import rx.Observer;

/**
 * Created by Svyd on 06.07.2016.
 */
public abstract class AbstractParcelPresenter implements IParcelPresenter {

    private final int MODE_FILTER = 0;
    private final int MODE_ALL = 1;

    private PostInteractor<ListRequest> mInteractor;
    private IParcelView mView;
    private ListRequest mRequest;
    private List<ParcelItem> mItems;
    private SearchRequest mSearchRequest;
    private int mMode = MODE_ALL;
    private PostInteractor<SearchRequest> mSearchInteractor;

    public AbstractParcelPresenter(IParcelView view,
                                   PostInteractor<ListRequest> interactor,
                                   PostInteractor<SearchRequest> searchInteractor) {
        mInteractor = interactor;
        mSearchInteractor = searchInteractor;
        mView = view;
    }

    @Override
    public void onCreateOptionsMenu(MenuInflater layoutInflater, Menu menu) {

    }

    @Override
    public void onAccept() {

    }

    @Override
    public void onAdd() {
        throw new UnsupportedOperationException();
    }

    void requestItems() {
        mView.showProgress();
        mInteractor.execute(mRequest, new ParcelObserver());
    }

    protected List<ParcelItem> getItems() {
        return mItems;
    }

    protected void setRequest(ListRequest request) {
        mRequest = request;
    }

    protected IParcelView getView() {
        return mView;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onFabClick() {
        swapMode();
        mView.hideFab();
        mItems.clear();
        mView.notifyChanges();
        mRequest.setOffset(0);
        mView.showProgress();
        mInteractor.execute(mRequest, new ParcelObserver());
    }

    private boolean isFilterMode() {
        return mMode == MODE_FILTER;
    }

    private void swapMode() {
        if (mMode == MODE_FILTER) {
            mMode = MODE_ALL;
        } else {
            mMode = MODE_FILTER;
        }
    }

    @Override
    public void handleResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case FilteringActivity.REQUEST_CODE_FILTER:
                    onFilter(data);
                    break;
            }
        }
    }

    private void onFilter(Intent data) {
        mSearchRequest = getSearchRequest(data);
        if (mSearchRequest == null) {
            return;
        }
        swapMode();
        mView.showFab();
        mItems.clear();
        mView.notifyChanges();
        mView.showProgress();

        mSearchInteractor.execute(mSearchRequest, new ParcelObserver());
    }

    private SearchRequest getSearchRequest(Intent data) {
        if (data != null && data.hasExtra(Constants.Extra.EXTRA_REQUEST)) {
            return (SearchRequest) data.getSerializableExtra(Constants.Extra.EXTRA_REQUEST);
        }
        return null;
    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void onItemClick(int index, ParcelItem item, View view) {
        mView.navigateToDetails(index, view, item);
    }

    @Override
    public void onLoadMore() {
        mView.showListProgress();
        if (isFilterMode()) {
            mSearchRequest.incrementOffset();
            mSearchInteractor.execute(mSearchRequest, new ParcelObserver());
        } else {
            mRequest.incrementOffset();
            mInteractor.execute(mRequest, new ParcelObserver());
        }
    }

    void onSetItems(List<ParcelItem> items) {
        mItems = items;
        mView.setItems(mItems);
    }

    void onAddItems(List<ParcelItem> items) {
        mItems.addAll(items);
        mView.notifyChanges();
    }

    boolean toAdd() {
        if (isFilterMode()) {
            return mSearchRequest.getOffset() > 0;
        } else {
            return mRequest.getOffset() > 0;
        }
    }

    void doOnNext(List<ParcelItem> items) {
        if (items.size() > 0) {
            mView.hideStubText();
            mView.enableLoadMore();
            if (toAdd()) {
                onAddItems(items);
            } else {
                onSetItems(items);
            }
        } else {
            verifyEmptyList();
        }
    }

    private void verifyEmptyList() {
        if (isFilterMode()) {
            if (mSearchRequest.getOffset() == 0) {
                mView.showStubText();
            }
        } else {
            if (mRequest.getOffset() == 0) {
                mView.showStubText();
            }
        }
    }

    private class ParcelObserver implements Observer<List<ParcelItem>> {

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            mView.hideProgress();
            mView.enableLoadMore();
            mView.hideListProgress();
        }

        @Override
        public void onNext(List<ParcelItem> parcelItems) {
            doOnNext(parcelItems);
            mView.hideListProgress();
        }
    }
}
