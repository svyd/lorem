package com.svyd.wodes.presentation.project.delivery.all.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.OnItemClickListener;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.details.DeliveryDetailsActivity;
import com.svyd.wodes.presentation.project.filtering.view.FilteringActivity;
import com.svyd.wodes.presentation.project.delivery.all.adapter.DeliveryAdapter;
import com.svyd.wodes.presentation.project.delivery.all.contract.IDeliveryPresenter;
import com.svyd.wodes.presentation.project.delivery.all.contract.IDeliveryView;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.all.presenter.factory.DeliveryPresenterFactory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

import static android.support.v7.widget.RecyclerView.*;

/**
 * Created by Svyd on 24.08.2016.
 */
public class DeliveryFragment extends BaseFragment implements IDeliveryView, OnItemClickListener<DeliveryItem> {

    @BindView(R.id.rvItems_FL)
    RecyclerView rvDeliveries;

    @BindView(R.id.tvTitle_TL)
    TextView tvTitle;

    @BindView(R.id.tvNoItems_FL)
    TextView tvNoItems;

    @BindView(R.id.fabRemoveFilter_AL)
    FloatingActionButton fabRemoveFilter;


    @BindView(R.id.pbList_FL)
    SmoothProgressBar pbList;

    private IDeliveryPresenter mPresenter;
    private PresenterFactory<IDeliveryPresenter, IDeliveryView> mFactory;
    private DeliveryAdapter mAdapter;
    private OnScrollListener mScrollListener;

    public static DeliveryFragment newInstance(Bundle args) {

        DeliveryFragment fragment = new DeliveryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_list);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.handleResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        providePresenter();
        initPresenter();
        initList();
    }

    private void initList() {
        mAdapter = new DeliveryAdapter(getActivity());
        mScrollListener = new LazyLoadListener(mPresenter);
        mAdapter.setOnItemClickListener(this);
        rvDeliveries.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDeliveries.setAdapter(mAdapter);
        rvDeliveries.addOnScrollListener(mScrollListener);
        mAdapter.notifyDataSetChanged();
    }

    private void providePresenter() {
        mFactory = new DeliveryPresenterFactory();
        mPresenter = mFactory.providePresenter(getArguments(), this);
    }

    private void initPresenter() {
        mPresenter.initialize();
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onItemClick(int index, DeliveryItem item, View view) {
        Intent intent = new Intent(getActivity(), DeliveryDetailsActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_POSITION, index);
        intent.putExtra(Constants.Flow.FLOW_KEY, Constants.Flow.DETAILS_ALL);
        intent.putExtra(Constants.Extra.EXTRA_DELIVERY, item);
        startActivityForResult(intent, DeliveryDetailsActivity.REQUEST_CODE_VIEW);
    }

    void startFilteringActivity(View view) {
        int[] position = new int[2];
        view.getLocationOnScreen(position);
        Intent intent = new Intent(getActivity(), FilteringActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("x", position[0] + view.getWidth() / 2);
        intent.putExtra("y", position[1] + view.getHeight() / 2);
        startActivityForResult(intent, FilteringActivity.REQUEST_CODE_FILTER);
    }

    @OnClick(R.id.btnFilter_TL)
    void onFilterClick(View view) {
        startFilteringActivity(view);
    }

    @Override
    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    @Override
    public void setItems(List<DeliveryItem> items) {
        mAdapter.setItems(items);
    }

    @Override
    public void enableLoadMore() {

    }

    @Override
    public void notifyChanges() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showStubText() {
        tvNoItems.setText(R.string.no_deliveries);
        tvNoItems.setVisibility(VISIBLE);
    }

    @Override
    public void hideStubText() {
        tvNoItems.setVisibility(GONE);
    }

    @OnClick(R.id.fabRemoveFilter_AL)
    void onFabClick() {
        mPresenter.onFabClick();
    }

    @Override
    public void showFab() {
//        Animation alpha = new AlphaAnimation(0, 1);
//        Animation scale = new ScaleAnimation(0, 1, 0, 1);
//        AnimationSet set = new AnimationSet(true);
//        set.addAnimation(alpha);
//        set.addAnimation(scale);

//        fabRemoveFilter.startAnimation(set);
        fabRemoveFilter.setVisibility(VISIBLE);
    }

    @Override
    public void hideFab() {
//        Animation alpha = new AlphaAnimation(1, 0);
//        Animation scale = new ScaleAnimation(1, 0, 1, 0);
//        AnimationSet set = new AnimationSet(true);
//        set.addAnimation(alpha);
//        set.addAnimation(scale);
//
//        set.setAnimationListener(new AnimationListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animation animation) {
//            }
//        });
//        fabRemoveFilter.startAnimation(set);
        fabRemoveFilter.setVisibility(GONE);
    }

    @Override
    public void showListProgress() {
        pbList.setVisibility(VISIBLE);
        pbList.progressiveStart();
    }

    @Override
    public void hideListProgress() {
        pbList.progressiveStop();
        pbList.setVisibility(INVISIBLE);
    }
}