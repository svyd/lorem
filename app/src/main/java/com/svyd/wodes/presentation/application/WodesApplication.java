package com.svyd.wodes.presentation.application;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.StringRes;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Svyd on 02.07.2016.
 */
public class WodesApplication extends MultiDexApplication {

    private static final String TWITTER_KEY = "HHJQjRpDyvYKrKzhylfg1ZlRZ";
    private static final String TWITTER_SECRET = "08VoeIOPNHEiR2owSR78LcQ9OSwfpdtnqIbgvnFbm9axvtQct3";

    private static WodesApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        printHashKey();

        sInstance = this;

        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
    }

    public static Context getContext() {
        return sInstance;
    }

    public static String getStringResource(@StringRes int resId) {
        return sInstance.getString(resId);
    }
    public static String getStringResource(@StringRes int resId, Object... formatArgs) {
        return sInstance.getString(resId, formatArgs);
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.svyd.wodes",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("HASH KEY:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }

}
