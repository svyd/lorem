package com.svyd.wodes.presentation.project.filtering.presenter;

import android.os.Bundle;

import com.svyd.wodes.data.places.GooglePlacesRepository;
import com.svyd.wodes.data.places.GooglePlacesService;
import com.svyd.wodes.data.places.IPlacesRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.places.PlaceDetailsInteractor;
import com.svyd.wodes.domain.places.PredictionsInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.project.filtering.contract.IFilteringPresenter;
import com.svyd.wodes.presentation.project.filtering.contract.IFilteringView;

import java.util.List;

/**
 * Created by Svyd on 21.09.2016.
 */
public class FilteringPresenterFactory implements PresenterFactory<IFilteringPresenter, IFilteringView> {
    private IPlacesRepository mPlacesRepository;

    @Override
    public IFilteringPresenter providePresenter(Bundle args, IFilteringView view) {
        return new FilteringPresenter(view, providePlacesInteractor(), provideDetailsInteractor());
    }

    private PostInteractor<List<String>> provideDetailsInteractor() {
        return new PlaceDetailsInteractor(providePlacesRepository());
    }

    private PostInteractor<String> providePlacesInteractor() {
        return new PredictionsInteractor(providePlacesRepository());
    }

    private IPlacesRepository providePlacesRepository() {
        if (mPlacesRepository == null) {
            mPlacesRepository = new GooglePlacesRepository(providePlacesService());
        }
        return mPlacesRepository;
    }

    private GooglePlacesService providePlacesService() {
        return new ServiceProvider().provideGooglePlacesService(WodesApplication.getContext(), GooglePlacesService.class);
    }
}
