package com.svyd.wodes.presentation.project.delivery.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Svyd on 08.09.2016.
 */
public class SimpleLocationModel implements Serializable{

    @SerializedName("city")
    private String city;

    @SerializedName("country")
    private String country;

    @SerializedName("location")
    private double[] location;

    public double[] getLocation() {
        return location;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }

    public double getLat() {
        return location[0];
    }

    public double getLon() {
        return location[1];
    }

    public static class Builder {
        private String city;

        private String country;

        private double[] location;

        public Builder setCity(String city) {
            this.city = city;
            return this;
        }

        public Builder setCountry(String country) {
            this.country = country;
            return this;
        }

        public Builder setLocation(double[] location) {
            this.location = location;
            return this;
        }

        public SimpleLocationModel build() {
            SimpleLocationModel model = new SimpleLocationModel();
            model.setCity(city);
            model.setCountry(country);
            model.setLocation(location);
            return model;
        }

    }
}
