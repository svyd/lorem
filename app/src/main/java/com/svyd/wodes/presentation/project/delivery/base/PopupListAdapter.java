package com.svyd.wodes.presentation.project.delivery.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.svyd.wodes.R;

import java.util.List;

/**
 * Created by Svyd on 09.09.2016.
 */
public class PopupListAdapter extends BaseAdapter {

    private List<String> mData;
    private Context mContext;
    private AdapterView.OnItemClickListener mListener;

    public PopupListAdapter(Context context, List<String> data) {
        mContext = context;
        mData = data;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(R.layout.list_item_popup, parent, false);
            holder = new TextViewHolder((TextView) convertView.findViewById(R.id.tvItem_LIP));
            convertView.setTag(holder);
        } else {
            holder = (TextViewHolder) convertView.getTag();
        }
        holder.setPosition(position);
        holder.setText(getItem(position));
        return convertView;
    }

    private class TextViewHolder implements View.OnClickListener {
        private TextView textView;
        private int position;

        public TextViewHolder(TextView view) {
            textView = view;
            textView.setOnClickListener(this);
        }

        public void setText(String text) {
            textView.setText(text);
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public int getPosition() {
            return position;
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(null, v, position, -1);
            }
        }
    }
}
