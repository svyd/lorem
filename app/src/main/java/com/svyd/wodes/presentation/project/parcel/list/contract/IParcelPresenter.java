package com.svyd.wodes.presentation.project.parcel.list.contract;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;

import com.svyd.wodes.presentation.base.OnItemClickListener;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import static com.svyd.wodes.presentation.base.tool.LazyLoadListener.*;

/**
 * Created by Svyd on 05.07.2016.
 */
public interface IParcelPresenter extends BasePresenter, OnItemClickListener<ParcelItem>, LoadMoreListener {
    void onAdd();
    void onFabClick();
    void handleResult(int requestCode, int resultCode, Intent data);
    void onCreateOptionsMenu(MenuInflater layoutInflater, Menu menu);
    void onAccept();
}
