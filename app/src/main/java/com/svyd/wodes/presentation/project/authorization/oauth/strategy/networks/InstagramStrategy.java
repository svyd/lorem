package com.svyd.wodes.presentation.project.authorization.oauth.strategy.networks;

import android.app.Activity;
import android.content.Intent;

import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.model.OauthRequest;
import com.svyd.wodes.presentation.project.authorization.oauth.instagram.InstaAuthModel;
import com.svyd.wodes.presentation.project.authorization.oauth.instagram.InstagramActivity;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.AuthorizationStrategy;

import java.lang.ref.WeakReference;

/**
 * Created by Svyd on 18.07.2016.
 */
public class InstagramStrategy implements AuthorizationStrategy {

    private WeakReference<OauthCallback> mCallback;

    private String getTokenByUri(String _uri) {
        String token = _uri.split("=")[1];
        return token;
    }

    @Override
    public void logIn(BaseFragment _fragment, OauthCallback _callback) {
        mCallback = new WeakReference<>(_callback);

        Intent intent = new Intent(_fragment.getActivity(), InstagramActivity.class);
        InstaAuthModel model = new InstaAuthModel();
        model.setAuthUrl(Constants.Credentials.INSTAGRAM_AUTH_URL);
        model.setClientId(Constants.Credentials.INSTAGRAM_CLIENT_ID);
        model.setRedirectUri(Constants.Credentials.REDIRECT_URI);
        intent.putExtra(Constants.BUNDLE_INSTAGRAM_MODEL, model);
        _fragment.startActivityForResult(intent, Constants.INSTA_AUTH_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int _requestCode, int _resultCode, Intent _intent) {
        if (_requestCode == Constants.INSTA_AUTH_ACTIVITY_REQUEST_CODE &&
                _resultCode == Activity.RESULT_OK) {
            if (mCallback.get() == null) {
                return;
            }
            String redirectedUri = _intent.getExtras().getString(Constants.BUNDLE_REDIRECTED_URI);
            String accessToken = getTokenByUri(redirectedUri);

            OauthRequest.Builder model = new OauthRequest.Builder()
                    .setNetwork(Constants.AuthType.INSTAGRAM)
                    .setToken(accessToken);

            mCallback.get().onSuccess(model.build());
        }
    }
}
