package com.svyd.wodes.presentation.project.authorization.presenter.impl;

import android.os.Bundle;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.contract.CodeVerificationView;
import com.svyd.wodes.presentation.project.authorization.model.CodeWrapper;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observer;

/**
 * Created by Svyd on 24.08.2016.
 */
public class CodeAddPresenter extends AbstractCodePresenter {

    private PostInteractor<CodeWrapper> mCodeInteractor;
    private ProfileInfo mProfile;

    public CodeAddPresenter(CodeVerificationView view,
                            PostInteractor<PhoneWrapper> phoneInteractor,
                            PostInteractor<CodeWrapper> codeInteractor) {
        super(view, phoneInteractor);

        mCodeInteractor = codeInteractor;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null) {
            mProfile = (ProfileInfo) args.getSerializable(Constants.Extra.EXTRA_PROFILE);
        }
    }

    @Override
    protected void execute() {
        mCodeInteractor.execute(getRequest().getCode(), new CodeObserver());
    }

    @Override
    public void onStop() {
        super.onStop();
        mCodeInteractor.unSubscribe();
    }

    protected void doOnNext() {
        getView().hideProgress();
        mProfile.setPhone(getRequest().getNumber());

        getView().navigateNext(mProfile);
    }

    private class CodeObserver implements Observer<ResponseModel> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            getView().showToast(WodesApplication.getStringResource(R.string.txt_network_error));
        }

        @Override
        public void onNext(ResponseModel info) {
            doOnNext();
        }
    }
}
