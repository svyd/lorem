package com.svyd.wodes.presentation.project.authorization.presenter.factory;

import android.os.Bundle;

import com.svyd.wodes.domain.authorization.ForgotPasswordInteractor;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.project.authorization.contract.IForgotPasswordPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.ForgotPasswordView;
import com.svyd.wodes.presentation.project.authorization.presenter.impl.ForgotPasswordPresenterImpl;

/**
 * Created by Svyd on 24.08.2016.
 */
public class ForgotPasswordPresenterFactory
        implements PresenterFactory<IForgotPasswordPresenter, ForgotPasswordView> {
    @Override
    public IForgotPasswordPresenter providePresenter(Bundle args, ForgotPasswordView view) {
        return new ForgotPasswordPresenterImpl(view, new ForgotPasswordInteractor(view.getScopedRepository()));
    }
}
