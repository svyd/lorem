package com.svyd.wodes.presentation.project.authorization.model;

import java.io.Serializable;

/**
 * Created by Svyd on 23.08.2016.
 */
public class SignInRequest implements Serializable{

    private String email;
    private String password;
    private String deviceId = System.currentTimeMillis() + "";
    private String deviceToken = System.currentTimeMillis() + "";
    private String provider = "GOOGLE";

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public String getProvider() {
        return provider;
    }
}
