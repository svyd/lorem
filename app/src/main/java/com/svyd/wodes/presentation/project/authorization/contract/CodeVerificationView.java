package com.svyd.wodes.presentation.project.authorization.contract;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

/**
 * Created by Svyd on 13.08.2016.
 */
public interface CodeVerificationView extends BaseView {
    void clearCode();
    void onEditedPhoneValidated();
    void navigateNext(ProfileInfo info);
    void setProgressTitle(String title);
    void showToast(String message);
    void dismissKeyboard();
    void setDarkTextColor();
    AuthRepository provideScopedRepository();

}
