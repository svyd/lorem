package com.svyd.wodes.presentation.project.authorization.oauth.strategy.networks;

import android.content.Intent;

import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.model.OauthRequest;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.AuthorizationStrategy;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.lang.ref.WeakReference;

/**
 * Created by Svyd on 18.07.2016.
 */
public class TwitterStrategy implements AuthorizationStrategy {
    protected TwitterAuthClient mClient;
    protected WeakReference<OauthCallback> mCallbackReference;

    public TwitterStrategy(TwitterAuthClient _client) {
        mClient = _client;
    }

    @Override
    public void logIn(BaseFragment _fragment, OauthCallback _callback) {
        mCallbackReference = new WeakReference<>(_callback);
        mClient.authorize(_fragment.getActivity(), mCallback);
    }

    @Override
    public void onActivityResult(int _requestCode, int _resultCode, Intent _intent) {
        mClient.onActivityResult(_requestCode, _resultCode, _intent);
    }

    private Callback<TwitterSession> mCallback = new Callback<TwitterSession>() {
        @Override
        public void success(Result<TwitterSession> result) {
            if (mCallbackReference.get() == null)
                return;

            OauthRequest.Builder model = new OauthRequest.Builder()
                    .setOathToken(result.data.getAuthToken().token)
                    .setOauthTokenSecret(result.data.getAuthToken().secret)
                    .setUserId(String.valueOf(result.data.getUserId()))
                    .setToken(result.data.getAuthToken().token)
                    .setId(String.valueOf(result.data.getUserId()))
                    .setNetwork(Constants.AuthType.TWITTER);

            mCallbackReference.get().onSuccess(model.build());
        }

        @Override
        public void failure(TwitterException e) {
            if (mCallbackReference.get() == null)
                return;

//            mCallbackReference.get().onError(e);
        }
    };
}
