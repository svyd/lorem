package com.svyd.wodes.presentation.project.filtering.contract;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;

/**
 * Created by Svyd on 20.09.2016.
 */
public interface IFilteringPresenter extends BasePresenter {
    void onDepartureTextChanged(String text);
    void onArrivalTextChanged(String text);
    void onDepartureItemSelected(int position);
    void onArrivalItemSelected(int position);
    void onDoneClick();
    void onDateClick();
    void onDateSet(int year, int month, int day);
}
