package com.svyd.wodes.presentation.project.profile.edit.presenter;

import android.os.Bundle;

import com.svyd.wodes.data.profile.edit.EditProfileRepository;
import com.svyd.wodes.data.profile.edit.UpdateProfileService;
import com.svyd.wodes.domain.profile.edit.EditProfileInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.project.profile.edit.contract.IEditProfilePresenter;
import com.svyd.wodes.presentation.project.profile.edit.contract.IEditProfileView;

/**
 * Created by Svyd on 04.09.2016.
 */
public class EditProfilePresenterFactory implements PresenterFactory<IEditProfilePresenter, IEditProfileView> {
    @Override
    public IEditProfilePresenter providePresenter(Bundle args, IEditProfileView view) {
        return new EditProfilePresenter(view, args, provideInteractor());

    }
    private EditProfileInteractor provideInteractor() {
        UpdateProfileService service = new ServiceProvider().provideWodesService(WodesApplication.getContext(), UpdateProfileService.class);
        return new EditProfileInteractor(new EditProfileRepository(service));
    }
}
