package com.svyd.wodes.presentation.project.delivery.model;

/**
 * Created by Svyd on 24.08.2016.
 */
public class ListRequest {

    private String type;
    private int limit;
    private int offset;

    public ListRequest() {

    }

    public ListRequest(String type, int limit, int offset) {
        this.type = type;
        this.limit = limit;
        this.offset = offset;
    }

    public void incrementOffset() {
        offset += limit;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getType() {
        return type;
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    public static class Builder {
        private String type;
        private int limit;
        private int offset;

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public Builder setLimit(int limit) {
            this.limit = limit;
            return this;
        }

        public Builder setOffset(int offset) {
            this.offset = offset;
            return this;
        }

        public ListRequest build() {
            ListRequest request = new ListRequest();
            request.setLimit(limit);
            request.setOffset(offset);
            request.setType(type);

            return request;
        }

    }
}
