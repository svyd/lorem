package com.svyd.wodes.presentation.base.mvp;

import android.os.Bundle;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface PresenterFactory<P extends BasePresenter, V extends BaseView> {
    P providePresenter(Bundle args, V view);
}
