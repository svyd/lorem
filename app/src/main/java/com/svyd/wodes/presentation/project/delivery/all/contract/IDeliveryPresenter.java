package com.svyd.wodes.presentation.project.delivery.all.contract;

import android.content.Intent;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;

import static com.svyd.wodes.presentation.base.tool.LazyLoadListener.*;

/**
 * Created by Svyd on 24.08.2016.
 */
public interface IDeliveryPresenter extends BasePresenter, LoadMoreListener {
    void onItemClick(DeliveryItem item);
    void handleResult(int requestCode, int resultCode, Intent data);
    void onFabClick();
}