package com.svyd.wodes.presentation.project.delivery.details.presenter;

import android.os.Bundle;

import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.delivery.DeliveriesMapper;
import com.svyd.wodes.data.delivery.DeliveryMapper;
import com.svyd.wodes.data.delivery.DeliveryRepository;
import com.svyd.wodes.data.delivery.DeliveryService;
import com.svyd.wodes.data.size.PreferenceSizeCache;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.delivery.UpdateDeliveryViewsInteractor;
import com.svyd.wodes.domain.parcel.UpdateParcelViewsInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.details.contract.IDeliveryDetailsPresenter;
import com.svyd.wodes.presentation.project.delivery.details.contract.IDeliveryDetailsView;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryEntity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;

import java.util.List;

/**
 * Created by Svyd on 11.09.2016.
 */
public class DeliveryDetailsPresenterFactory implements PresenterFactory<IDeliveryDetailsPresenter, IDeliveryDetailsView>{
    @Override
    public IDeliveryDetailsPresenter providePresenter(Bundle args, IDeliveryDetailsView view) {
        int flow = args.getInt(Constants.Flow.FLOW_KEY);

        switch (flow) {
            case Constants.Flow.DETAILS_MY:
                return new MyDeliveryDetailsPresenter(view, args);
            case Constants.Flow.DETAILS_ALL:
                return new AllDeliveryDetailsPresenter(provideUpdateViewsInteractor(), view, args);
            default:
                throw new IllegalArgumentException("Unsupported flow: " + flow);
        }
    }

    private PostInteractor<String> provideUpdateViewsInteractor() {
        return new UpdateDeliveryViewsInteractor(new DeliveryRepository(provideDeliveryService(), null, null, null));
    }

    private DeliveryService provideDeliveryService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), DeliveryService.class);
    }
}
