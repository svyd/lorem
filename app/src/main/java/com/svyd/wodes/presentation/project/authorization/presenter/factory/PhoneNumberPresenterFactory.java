package com.svyd.wodes.presentation.project.authorization.presenter.factory;

import android.os.Bundle;

import com.svyd.wodes.domain.authorization.PhoneNumberInteractor;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.project.authorization.contract.IPhoneNumberPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.PhoneNumberView;
import com.svyd.wodes.presentation.project.authorization.presenter.impl.PhoneNumberPresenterImpl;

/**
 * Created by Svyd on 08.08.2016.
 */
public class PhoneNumberPresenterFactory implements PresenterFactory<IPhoneNumberPresenter, PhoneNumberView> {

    @Override
    public IPhoneNumberPresenter providePresenter(Bundle args, PhoneNumberView view) {
        return provideSignUpPhonePresenter(view);
    }

    private PhoneNumberPresenterImpl provideSignUpPhonePresenter(PhoneNumberView view) {
        return new PhoneNumberPresenterImpl(view, new PhoneNumberInteractor(view.provideScopedRepository()));
    }
}
