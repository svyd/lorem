package com.svyd.wodes.presentation.project.authorization.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 24.08.2016.
 */
public class EmailWrapper {

    @SerializedName("email")
    private String email;

    public EmailWrapper(String email) {
        this.email = email;
    }
}
