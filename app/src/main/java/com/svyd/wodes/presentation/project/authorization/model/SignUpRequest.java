package com.svyd.wodes.presentation.project.authorization.model;

import java.io.Serializable;

/**
 * Created by Svyd on 18.07.2016.
 */
public class SignUpRequest implements Serializable {

    private String email;
    private String fullName;
    private String password;
    private String confirm;
    private String number;
    private CodeWrapper code;

    public void setCode(String code) {
        this.code = new CodeWrapper(code);
    }

    public CodeWrapper getCode() {
        return code;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public String getConfirm() {
        return confirm;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.fullName = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
