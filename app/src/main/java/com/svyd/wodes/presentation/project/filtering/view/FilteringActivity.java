package com.svyd.wodes.presentation.project.filtering.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.tool.RevealUtil;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.base.SimpleTextWatcher;
import com.svyd.wodes.presentation.project.filtering.contract.IFilteringPresenter;
import com.svyd.wodes.presentation.project.filtering.contract.IFilteringView;
import com.svyd.wodes.presentation.project.filtering.presenter.FilteringPresenter;
import com.svyd.wodes.presentation.project.filtering.presenter.FilteringPresenterFactory;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

/**
 * Created by Svyd on 27.08.2016.
 */
public class FilteringActivity extends BaseActivity implements IFilteringView, DatePickerDialog.OnDateSetListener {

    public static final int REQUEST_CODE_FILTER = 6543;

    private Unbinder mUnbinder;

    @BindView(R.id.flContainer_AF)
    FrameLayout flContainer;

    @BindView(R.id.etDeparture_AF)
    EditText etDeparture;

    @BindView(R.id.etArrival_AF)
    EditText etArrival;

    @BindView(R.id.pbArrival_AF)
    ProgressBar pbArrival;

    @BindView(R.id.pbDeparture_AF)
    ProgressBar pbDeparture;

    @BindView(R.id.etDate_AF)
    EditText etDate;

    private IFilteringPresenter mPresenter;
    private TextWatcher mDepartureTextWatcher;
    private TextWatcher mArrivalTextWatcher;
    private AdapterView.OnItemClickListener mDepartureListener;
    private AdapterView.OnItemClickListener mArrivalListener;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filtering);
        mUnbinder = ButterKnife.bind(this);
        enableBackButton(true);
        setTitle(getString(R.string.title_filtering));
        initListeners();
        initUi();
        initPresenter();
        if (savedInstanceState == null) {
            if (checkLollipop()) {
                makeReveal();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_action, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.miDone_MA) {
            mPresenter.onDoneClick();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initPresenter() {
        mPresenter = new FilteringPresenterFactory().providePresenter(getIntent().getExtras(), this);
        mPresenter.initialize();
    }

    private void initUi() {
        mProgress = new ProgressDialog(this);
        mProgress.setTitle(getString(R.string.txt_wait));
        mProgress.setMessage(getString(R.string.filter));
        mProgress.setCancelable(false);
        etArrival.addTextChangedListener(mArrivalTextWatcher);
        etDeparture.addTextChangedListener(mDepartureTextWatcher);
    }

    private void initListeners() {
        mDepartureListener = (parent, view, position, id) ->
                mPresenter.onDepartureItemSelected(position);

        mArrivalListener = (parent, view, position, id) ->
                mPresenter.onArrivalItemSelected(position);

        mDepartureTextWatcher = new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPresenter.onDepartureTextChanged(s.toString());
            }
        };
        mArrivalTextWatcher = new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPresenter.onArrivalTextChanged(s.toString());
            }
        };
    }

    private void makeReveal() {

        if (!getIntent().hasExtra("x"))
            return;

        int x = getIntent().getIntExtra("x", 0);
        int y = getIntent().getIntExtra("y", 0);
        if (x == 0) {
            return;
        }
        RevealUtil.makeReveal(flContainer, x, y);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    public int getContainerId() {
        return 0;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AF;
    }

    @Override
    public void navigateBackWithResult(SearchRequest request) {
        Intent intent = new Intent();
        intent.putExtra(Constants.Extra.EXTRA_REQUEST, request);
        setResult(RESULT_OK, intent);

        finish();
    }

    @Override
    public void setArrival(String text) {
        dismissListPopup();
        etArrival.removeTextChangedListener(mArrivalTextWatcher);
        etArrival.setText(text);
        etArrival.addTextChangedListener(mArrivalTextWatcher);
    }

    @OnClick(R.id.etDate_AF)
    void onDateClick() {
        mPresenter.onDateClick();
    }

    @Override
    public void setDeparture(String text) {
        dismissListPopup();
        etDeparture.removeTextChangedListener(mDepartureTextWatcher);
        etDeparture.setText(text);
        etDeparture.addTextChangedListener(mDepartureTextWatcher);
    }

    @Override
    public void showArrivalPopup(List<String> places) {
        showListMenu(etArrival, places, mArrivalListener, false);
    }

    @Override
    public void showDeparturePopup(List<String> places) {
        showListMenu(etDeparture, places, mDepartureListener, false);
    }

    @Override
    public void showArrivalProgress(boolean show) {
        pbArrival.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showDepartureProgress(boolean show) {
        pbDeparture.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showDatePicker(int year, int month, int day) {
        DatePickerDialog.newInstance(this, year, month, day)
                .show(getFragmentManager(), DatePickerDialog.class.getSimpleName());
    }

    @Override
    public void setDate(String date) {
        etDate.setText(date);
    }

    @Override
    public void showProgress() {
        mProgress.show();
    }

    @Override
    public void hideProgress() {
        mProgress.cancel();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int day) {
        mPresenter.onDateSet(year, month, day);
    }
}
