package com.svyd.wodes.presentation.base.tool;

import android.support.v7.widget.Toolbar;
import android.view.View;

import com.svyd.wodes.presentation.base.ui.BaseActivity;

/**
 * Created by Svyd on 02.07.2016.
 */
public class ToolbarManager {
    private Toolbar mToolbar;

    public ToolbarManager(BaseActivity _activity) {
        mToolbar = (Toolbar) _activity.findViewById(_activity.getToolbarId());
    }

    public void hideToolbar() {
        mToolbar.setVisibility(View.GONE);
    }

    public void showToolbar() {
        mToolbar.setVisibility(View.VISIBLE);
    }
}
