package com.svyd.wodes.presentation.project.authorization.presenter.factory;

import android.os.Bundle;

import com.svyd.wodes.data.profile.cache.PreferenceProfileCache;
import com.svyd.wodes.domain.profile.ProfileCacheInteractor;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.project.authorization.contract.ISplashPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.ISplashView;
import com.svyd.wodes.presentation.project.authorization.presenter.impl.SplashPresenter;

/**
 * Created by Svyd on 05.09.2016.
 */
public class SplashPresenterFactory implements PresenterFactory<ISplashPresenter, ISplashView> {
    @Override
    public ISplashPresenter providePresenter(Bundle args, ISplashView view) {
        return new SplashPresenter(view, new ProfileCacheInteractor(new PreferenceProfileCache()));
    }
}
