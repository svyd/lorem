package com.svyd.wodes.presentation.project.profile.presenter;

import android.os.Bundle;

import com.svyd.wodes.data.profile.repository.IProfileRepository;
import com.svyd.wodes.data.profile.repository.ProfileRepository;
import com.svyd.wodes.data.profile.repository.ProfileService;
import com.svyd.wodes.domain.base.Interactor;
import com.svyd.wodes.domain.profile.LogoutInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.profile.contract.IProfilePresenter;
import com.svyd.wodes.presentation.project.profile.contract.ProfileView;
import com.svyd.wodes.presentation.project.profile.presenter.implementation.CurrentProfilePresenter;
import com.svyd.wodes.presentation.project.profile.presenter.implementation.ProfilePresenterImpl;
import com.svyd.wodes.domain.profile.CurrentProfileInteractor;
import com.svyd.wodes.domain.profile.ProfileInteractor;
import com.svyd.wodes.data.profile.cache.PreferenceProfileCache;
import com.svyd.wodes.data.profile.repository.StubProfileRepositoryImpl;

/**
 * Created by Svyd on 04.07.2016.
 */
public class ProfilePresenterFactory implements PresenterFactory<IProfilePresenter, ProfileView> {

    private IProfileRepository mRepository;

    @Override
    public IProfilePresenter providePresenter(Bundle args, ProfileView view) {

        int flow = args.getInt(Constants.Flow.FLOW_KEY, -1);
        if (flow < 0) {
            throw new IllegalArgumentException("Presenter cannot be provided without flow");
        }
        switch (flow) {
            case Constants.Flow.PROFILE_CURRENT:
                return new CurrentProfilePresenter(
                        new CurrentProfileInteractor(
                                provideProfileRepository()),
                        provideLogoutInteractor(),
                        view);
            case Constants.Flow.PROFILE_NOT_CURRENT:
                return new ProfilePresenterImpl(
                        new ProfileInteractor(
                                new StubProfileRepositoryImpl(
                                        new PreferenceProfileCache())), view);
            default:
                throw new IllegalArgumentException("Unsupported flow: " + flow);
        }
    }

    private Interactor provideLogoutInteractor() {
        return new LogoutInteractor(provideProfileRepository());
    }

    private IProfileRepository provideProfileRepository() {
        if (mRepository == null) {
            mRepository = new ProfileRepository(provideProfileService(), new PreferenceProfileCache());
        }
        return mRepository;
    }

    private ProfileService provideProfileService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), ProfileService.class);
    }
}
