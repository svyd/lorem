package com.svyd.wodes.presentation.project.authorization.presenter.factory;

import android.os.Bundle;

import com.svyd.wodes.domain.authorization.OauthInteractor;
import com.svyd.wodes.domain.authorization.SignInInteractor;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.project.authorization.contract.ISignUpPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.SignUpView;
import com.svyd.wodes.presentation.project.authorization.presenter.impl.SignUpPresenterImpl;

/**
 * Created by Svyd on 17.07.2016.
 */
public class SignUpPresenterFactory implements PresenterFactory<ISignUpPresenter, SignUpView> {
    @Override
    public ISignUpPresenter providePresenter(Bundle args, SignUpView view) {

        return new SignUpPresenterImpl(view,
                new SignInInteractor(view.provideScopedRepository()),
                new OauthInteractor(view.provideScopedRepository()));
    }
}
