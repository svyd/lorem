package com.svyd.wodes.presentation.base.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.svyd.wodes.R;

/**
 * Created by Svyd on 06.04.2016.
 */
@SuppressWarnings("FieldCanBeLocal")
public class CenteredRadioButton extends RadioButton implements CompoundButton.OnCheckedChangeListener {

    private final float SCALE_NORMAL = 1;
    private final float SCALE_INCREASED = 1.3F;
    private final float ALPHA_ZERO = 0;
    private final float ALPHA_FULL = 255;
    private final int RED_HIGHLIGHTED = 189;
    private final int GREEN_HIGHLIGHTED = 237;
    private final int BLUE_HIGHLIGHTED = 249;
    private final int ANIMATION_DURATION = 100;

    private Drawable mDrawable;
    private ValueAnimator mScaleAnimator;
    private ValueAnimator mTintAnimator;
    private float mScaleX = 1, mScaleY = 1;
    private float mHalfHeight;
    private float mHalfWidth;

    public CenteredRadioButton(Context context) {
        super(context);
    }

    public CenteredRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CenteredRadioButton, 0, 0);
        mDrawable = a.getDrawable(R.styleable.CenteredRadioButton_drawable);
        setButtonDrawable(android.R.color.transparent);
        a.recycle();
        setOnCheckedChangeListener(this);
    }

    private void onCheck() {
        scale(SCALE_NORMAL, SCALE_INCREASED);
        addTint(ALPHA_ZERO, ALPHA_FULL);
    }

    private void onUnCheck() {
        scale(SCALE_INCREASED, SCALE_NORMAL);
        addTint(ALPHA_FULL, ALPHA_ZERO);
    }

    void addTint(float fromAlpha, float toAlpha) {
        mTintAnimator = ValueAnimator.ofFloat(fromAlpha, toAlpha);
        mTintAnimator.setDuration(ANIMATION_DURATION);
        mTintAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        mTintAnimator.addUpdateListener(animation -> {
            float alpha = (float) animation.getAnimatedValue();
            mDrawable.setColorFilter(
                    Color.argb(((int)alpha),
                    RED_HIGHLIGHTED,
                    GREEN_HIGHLIGHTED,
                    BLUE_HIGHLIGHTED),
                    PorterDuff.Mode.SRC_ATOP);
        });
        mTintAnimator.start();
    }

    void scale(float from, float to) {
        mScaleAnimator = ValueAnimator.ofFloat(from, to);
        mScaleAnimator.setDuration(ANIMATION_DURATION);
        mScaleAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        mScaleAnimator.addUpdateListener(animation -> {
            mScaleX = (float) animation.getAnimatedValue();
            mScaleY = (float) animation.getAnimatedValue();
            invalidate();
        });
        mScaleAnimator.start();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mHalfHeight = h / 2;
        mHalfWidth = w / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mDrawable != null) {
            mDrawable.setState(getDrawableState());
            final int verticalGravity = getGravity() & Gravity.VERTICAL_GRAVITY_MASK;
            final int height = mDrawable.getIntrinsicHeight();

            int y = 0;

            switch (verticalGravity) {
                case Gravity.BOTTOM:
                    y = getHeight() - height;
                    break;
                case Gravity.CENTER_VERTICAL:
                    y = (getHeight() - height) / 2;
                    break;
            }

            canvas.scale(mScaleX, mScaleY, mHalfWidth, mHalfHeight);

            int buttonWidth = mDrawable.getIntrinsicWidth();
            int buttonLeft = (getWidth() - buttonWidth) / 2;

            mDrawable.setBounds(buttonLeft, y, buttonLeft + buttonWidth, y + height);
            mDrawable.draw(canvas);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked()) {
            onUnCheck();
        } else {
            onCheck();
        }
    }
}
