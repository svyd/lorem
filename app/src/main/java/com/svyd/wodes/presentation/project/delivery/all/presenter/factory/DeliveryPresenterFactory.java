package com.svyd.wodes.presentation.project.delivery.all.presenter.factory;

import android.os.Bundle;

import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.delivery.DeliveriesMapper;
import com.svyd.wodes.data.delivery.DeliveryMapper;
import com.svyd.wodes.data.delivery.DeliveryRepository;
import com.svyd.wodes.data.delivery.DeliveryService;
import com.svyd.wodes.data.delivery.IDeliveryRepository;
import com.svyd.wodes.data.profile.mapper.ProfileMapper;
import com.svyd.wodes.data.size.PreferenceSizeCache;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.delivery.DeliveryInteractor;
import com.svyd.wodes.domain.delivery.FindDeliveryInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.all.contract.IDeliveryPresenter;
import com.svyd.wodes.presentation.project.delivery.all.contract.IDeliveryView;
import com.svyd.wodes.presentation.project.delivery.all.presenter.impl.DeliveryPresenterImpl;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryEntity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import java.util.List;

/**
 * Created by Svyd on 24.08.2016.
 */
public class DeliveryPresenterFactory implements PresenterFactory<IDeliveryPresenter,IDeliveryView> {

    private IDeliveryRepository mRepository;

    @Override
    public IDeliveryPresenter providePresenter(Bundle args, IDeliveryView view) {
        return new DeliveryPresenterImpl(view, provideInteractor(), provideSearchInteractor());
    }

    private PostInteractor<SearchRequest> provideSearchInteractor() {
        return new FindDeliveryInteractor(provideDeliveryRepository());
    }

    private IDeliveryRepository provideDeliveryRepository() {
        if (mRepository == null) {
            mRepository = new DeliveryRepository(provideDeliveryService(), new PreferenceSizeCache(), provideDeliveryMapper(), provideDeliveriesMapper());
        }
        return mRepository;
    }

    private DeliveryService provideDeliveryService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), DeliveryService.class);
    }

    PostInteractor<ListRequest> provideInteractor() {
        return new DeliveryInteractor(provideDeliveryRepository());
    }

    private TypeMapper<DataWrapper<List<DeliveryEntity>>, List<DeliveryItem>> provideDeliveriesMapper() {
        return new DeliveriesMapper(provideDeliveryMapper());
    }

    private TypeMapper<DeliveryEntity, DeliveryItem> provideDeliveryMapper() {
        return new DeliveryMapper(provideProfileMapper());
    }

    private TypeMapper<ProfileInfo, ProfileInfo> provideProfileMapper() {
        return new ProfileMapper();
    }
}
