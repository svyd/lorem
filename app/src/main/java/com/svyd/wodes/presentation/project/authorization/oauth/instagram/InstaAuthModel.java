package com.svyd.wodes.presentation.project.authorization.oauth.instagram;

import java.io.Serializable;

/**
 * Created by Svyd on 18.07.2016.
 */
public class InstaAuthModel implements Serializable {
    private String authUrl;
    private String clientId;
    private String redirectUri;
    private String responseType = "token";

    public void setAuthUrl(String _url) {
        authUrl = _url;
    }

    public void setClientId(String _id) {
        clientId = _id;
    }

    public void setRedirectUri(String _uri) {
        redirectUri = _uri;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public String getResponseType() {
        return responseType;
    }
}