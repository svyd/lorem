package com.svyd.wodes.presentation.project.authorization.presenter.impl;

import android.util.Log;

import com.svyd.wodes.domain.base.Interactor;
import com.svyd.wodes.presentation.project.authorization.contract.ISplashPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.ISplashView;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observer;

/**
 * Created by Svyd on 05.09.2016.
 */
public class SplashPresenter implements ISplashPresenter {

    private static final String TAG = SplashPresenter.class.getSimpleName();
    private Interactor mCacheInteractor;
    private ISplashView mView;

    public SplashPresenter(ISplashView view, Interactor interactor) {
        mCacheInteractor = interactor;
        mView = view;
    }

    @Override
    public void initialize() {
        mCacheInteractor.execute(new CacheObserver());
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    protected void doOnNext(ProfileInfo info) {
        if (info != null) {
            mView.startProfile(info);
        } else {
            mView.startAuthorization();
        }
    }

    private class CacheObserver implements Observer<ProfileInfo> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "onError() called with: " + "e = [" + e + "]");
            mView.startAuthorization();
        }

        @Override
        public void onNext(ProfileInfo info) {
            doOnNext(info);
        }
    }
}
