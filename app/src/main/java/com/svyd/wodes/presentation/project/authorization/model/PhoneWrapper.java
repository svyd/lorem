package com.svyd.wodes.presentation.project.authorization.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 18.08.2016.
 */
public class PhoneWrapper {

    @SerializedName("phone")
    private String phone;

    public PhoneWrapper(String phone) {
        this.phone = phone;
    }
}
