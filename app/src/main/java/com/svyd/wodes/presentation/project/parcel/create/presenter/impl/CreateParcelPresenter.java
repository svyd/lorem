package com.svyd.wodes.presentation.project.parcel.create.presenter.impl;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.Interactor;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.project.parcel.create.contract.ICreateParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelRequest;

/**
 * Created by Svyd on 14.09.2016.
 */
public class CreateParcelPresenter extends AbstractCreateParcelPresenter {

    public CreateParcelPresenter(ICreateParcelView view, PostInteractor<String> placesInteractor, PostInteractor<ParcelRequest> requestInteractor, Interactor sizeInteractor) {
        super(view, placesInteractor, requestInteractor, sizeInteractor);
    }

    @Override
    public void initialize() {
        super.initialize();
        getView().setToolbarTitle(WodesApplication.getStringResource(R.string.create_parcel));
    }
}
