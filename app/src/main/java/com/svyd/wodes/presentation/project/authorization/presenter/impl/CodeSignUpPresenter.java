package com.svyd.wodes.presentation.project.authorization.presenter.impl;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.project.authorization.contract.CodeVerificationView;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observer;

/**
 * Created by Svyd on 13.08.2016.
 */
public class CodeSignUpPresenter extends AbstractCodePresenter {

    private PostInteractor<SignUpRequest> mCodeInteractor;

    public CodeSignUpPresenter(CodeVerificationView view, PostInteractor<PhoneWrapper> phoneInteractor, PostInteractor<SignUpRequest> codeInteractor) {
        super(view, phoneInteractor);
        mCodeInteractor = codeInteractor;
    }

    @Override
    protected void execute() {
        mCodeInteractor.execute(getRequest(), new CodeObserver());
    }

    @Override
    public void onStop() {
        super.onStop();
        mCodeInteractor.unSubscribe();
    }

    protected class CodeObserver implements Observer<ProfileInfo> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            getView().showToast(WodesApplication.getStringResource(R.string.txt_network_error));
        }

        @Override
        public void onNext(ProfileInfo info) {
            getView().hideProgress();
            getView().navigateNext(info);
        }
    }
}
