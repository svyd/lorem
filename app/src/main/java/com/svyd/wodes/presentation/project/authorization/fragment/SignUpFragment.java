package com.svyd.wodes.presentation.project.authorization.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.svyd.wodes.R;
import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.ScopedInstance;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.project.authorization.activity.AuthListener;
import com.svyd.wodes.presentation.project.authorization.contract.ISignUpPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.SignUpView;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.AuthController;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.AuthorizationStrategy;
import com.svyd.wodes.presentation.project.authorization.presenter.factory.SignUpPresenterFactory;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Svyd on 17.07.2016.
 */
public class SignUpFragment extends BaseFragment implements SignUpView {

    @BindView(R.id.etEmail_FS)
    EditText etEmail;

    @BindView(R.id.etFullName_FS)
    EditText etFullName;

    @BindView(R.id.etPassword_FS)
    EditText etPassword;

    @BindView(R.id.etConfirmPassword_FS)
    EditText etConfirmPassword;

    @BindView(R.id.ivCheckConfirm_FS)
    ImageView ivCheckConfirm;

    @BindView(R.id.ivCheckEmail_FS)
    ImageView ivCheckEmail;

    @BindView(R.id.ivCheckName_FS)
    ImageView ivCheckName;

    @BindView(R.id.ivCheckPassword_FS)
    ImageView ivCheckPassword;

    @BindView(R.id.pbConfirm_FS)
    ProgressBar pbConfirm;

    @BindView(R.id.pbEmail_FS)
    ProgressBar pbEmail;

    @BindView(R.id.pbName_FS)
    ProgressBar pbName;

    @BindView(R.id.pbPassword_FS)
    ProgressBar pbPassword;

    @BindView(R.id.rlNameContainer_FS)
    RelativeLayout rlNameContainer;

    @BindView(R.id.rlConfirmContainer_FS)
    RelativeLayout rlConfirmContainer;

    @BindView(R.id.btnSignUp_FS)
    Button btnSignUp;

    @BindView(R.id.tvAlreadyRegistered_FS)
    TextView tvAlreadyRegistered;

    @BindView(R.id.rlRoot_FS)
    RelativeLayout rlRoot;

    @BindView(R.id.tvForgotPassword_FS)
    TextView tvForgotPassword;

    @BindView(R.id.tvSignUp_FS)
    TextView tvSignUp;

    @BindView(R.id.llFieldsContainer_FS)
    LinearLayout llFieldsContainer;

    @BindView(R.id.rlControlsContainer_FS)
    RelativeLayout rlControlsContainer;

    private ISignUpPresenter mPresenter;
    private AuthController mAuthController;
    private ObjectAnimator mTextFadeOutAnimator;
    private ObjectAnimator mTextFadeInAnimator;
    private ProgressDialog mProgress;

    public static SignUpFragment newInstance() {

        Bundle args = new Bundle();

        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_signup);
    }

    private void iniUi() {
        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle(WodesApplication.getStringResource(R.string.txt_wait));
        mProgress.setMessage(WodesApplication.getStringResource(R.string.txt_signing_in));
    }

    private void initAnimators() {
        mTextFadeOutAnimator = ObjectAnimator.ofInt(btnSignUp, "textColor", ContextCompat.getColor(getActivity(), R.color.textColorBlue), Color.TRANSPARENT);
        mTextFadeInAnimator = ObjectAnimator.ofInt(btnSignUp, "textColor", Color.TRANSPARENT, ContextCompat.getColor(getActivity(), R.color.textColorBlue));
        mTextFadeOutAnimator.setEvaluator(new ArgbEvaluator());
        mTextFadeInAnimator.setEvaluator(new ArgbEvaluator());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initAnimators();
        iniUi();

        initPresenter();
        initController();
        mPresenter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mAuthController.onActivityResult(requestCode, resultCode, data);
    }

    private void initController() {
        mAuthController = new AuthController();
    }

    private void initPresenter() {
        if (mPresenter == null) {
            PresenterFactory<ISignUpPresenter, SignUpView> mFactory = new SignUpPresenterFactory();
            mPresenter = mFactory.providePresenter(getArguments(), this);
            mPresenter.initialize();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPresenter.onSaveInstanceState(outState);
    }

    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showProgress() {
        mProgress.show();
    }

    @Override
    public void hideProgress() {
        mProgress.cancel();
    }

    @Override
    public void setEmailError(String error) {
        etEmail.setError(error);
    }

    @Override
    public void setPasswordError(String error) {
        etPassword.setError(error);
    }

    @Override
    public void setConfirmError(String error) {
        etConfirmPassword.setError(error);
    }

    @Override
    public void setNameError(String error) {
        etFullName.setError(error);
    }

    @Override
    public void showEmailProgress(boolean show) {
        pbEmail.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNameProgress(boolean show) {
        pbName.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPasswordProgress(boolean show) {
        pbPassword.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showConfirmProgress(boolean show) {
        pbConfirm.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setEmailChecked(boolean checked) {
        ivCheckEmail.setVisibility(checked ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setNameChecked(boolean checked) {
        ivCheckName.setVisibility(checked ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setPasswordChecked(boolean checked) {
        ivCheckPassword.setVisibility(checked ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setConfirmChecked(boolean checked) {
        ivCheckConfirm.setVisibility(checked ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setConfirmEnabled(boolean enabled) {
        etConfirmPassword.setEnabled(enabled);
    }

    @Override
    public void passActivitySignUp(SignUpRequest request) {
        ((AuthListener) getActivity()).onSignUpClick(request);
    }

    @Override
    public void setDelegateStrategy(AuthorizationStrategy strategy) {
        mAuthController.setDelegateStrategy(strategy);
    }

    @Override
    public void logIn(AuthorizationStrategy.OauthCallback callback) {
        mAuthController.logIn(this, callback);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void dismissKeyboard() {
        hideKeyboard();
    }

    @Override
    public void showSignUpFields(boolean show) {
        etFullName.setEnabled(show);
        etConfirmPassword.setEnabled(show);

        etFullName.setFocusableInTouchMode(show);
        etConfirmPassword.setFocusableInTouchMode(show);

        etFullName.setVisibility(show ? View.VISIBLE : View.GONE);
        etConfirmPassword.setVisibility(show ? View.VISIBLE : View.GONE);

        rlNameContainer.setVisibility(show ? View.VISIBLE : View.GONE);
        rlConfirmContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setButtonTitle(final String title) {
        mTextFadeOutAnimator.start();
        mTextFadeOutAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                btnSignUp.setText(title);
                mTextFadeInAnimator.start();
            }
        });
    }

    @Override
    public void showSignUpControls(boolean show) {
        tvAlreadyRegistered.setVisibility(show ? View.VISIBLE : View.GONE);
        tvSignUp.setVisibility(show ? View.GONE : View.VISIBLE);
        tvForgotPassword.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void navigateToForgotPassword() {
        ((AuthListener) getActivity()).onForgotPassword();
    }

    @Override
    public void animateLayoutChanges() {
        rlControlsContainer.setLayoutTransition(new LayoutTransition());
        llFieldsContainer.setLayoutTransition(new LayoutTransition());
    }

    @Override
    public void SignUp(ProfileInfo info) {
        ((AuthListener) getActivity()).navigateToProfile(info);
    }

    @Override
    public void addPhone(ProfileInfo info) {
        ((AuthListener) getActivity()).onAddNumber(info);
    }

    @Override
    @SuppressWarnings("unchecked")
    public AuthRepository provideScopedRepository() {
        return ((ScopedInstance<AuthRepository>) getActivity()).provideScopedInstance();
    }

    @OnTextChanged(R.id.etEmail_FS)
    void onEmailChanged(CharSequence email) {
        mPresenter.onEmailChanged(email.toString());
    }

    @OnTextChanged(R.id.etFullName_FS)
    void onNameChanged(CharSequence name) {
        mPresenter.onFullNameChanged(name.toString());
    }

    @OnTextChanged(R.id.etPassword_FS)
    void onPasswordChanged(CharSequence password) {
        mPresenter.onPasswordChanged(password.toString());
    }

    @OnTextChanged(R.id.etConfirmPassword_FS)
    void onConfirmChanged(CharSequence confirm) {
        mPresenter.onConfirmPasswordChanged(confirm.toString());
    }

    @OnClick({R.id.tvAlreadyRegistered_FS, R.id.tvSignUp_FS})
    void onAlreadyRegisteredClick() {
        mPresenter.onAlreadyRegisteredClick();
    }

    @OnClick(R.id.btnSignUp_FS)
    void onSignUpClick() {
        mPresenter.onSignUpClick();
    }

    @OnClick(R.id.tvForgotPassword_FS)
    void onForgotPasswordClick() {
        mPresenter.onForgotPasswordClick();
    }

        @OnClick(R.id.ibFacebook_FS)
    void onFacebookClick() {
        mPresenter.onFacebookClick();
    }

    @OnClick(R.id.ibTwitter_FS)
    void onTwitterClick() {
        mPresenter.onTwitterClick();
    }

    @OnClick(R.id.ibInstagram_FS)
    void onInstagramClick() {
        mPresenter.onInstagramClick();
    }
}
