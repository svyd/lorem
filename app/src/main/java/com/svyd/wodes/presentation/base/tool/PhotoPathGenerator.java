package com.svyd.wodes.presentation.base.tool;

import android.content.Context;

import java.io.File;

/**
 * Created by Svyd on 30.08.2016.
 */
public class PhotoPathGenerator {
    private Context mContext;

    private String mPhotoPath;

    public PhotoPathGenerator(Context _context) {
        mContext = _context;
        generatePhotoPath();
    }

    private void generatePhotoPath() {
        File externalDir = mContext.getExternalCacheDir();

        File photoPath = new File(externalDir, "avatar.jpg");
        mPhotoPath = photoPath.getAbsolutePath();
    }

    public String getAvatarPath() {
        return mPhotoPath;
    }
}
