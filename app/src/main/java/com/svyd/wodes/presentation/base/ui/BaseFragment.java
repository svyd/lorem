package com.svyd.wodes.presentation.base.ui;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.tool.FragmentNavigator;
import com.svyd.wodes.presentation.base.tool.ToolbarManager;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Svyd on 02.07.2016.
 */
public abstract class BaseFragment extends Fragment {

    protected @LayoutRes
    int mContentId = -1;

    private Unbinder mUnbinder;

    protected void setContentView(@LayoutRes int _contentId) {
        mContentId = _contentId;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getPresenter() != null) {
            getPresenter().onStop();
        }
    }

    protected TextView getActionBarTitleView() {
        return getBaseActivity().getActionBarTitleView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        onBind(ButterKnife.bind(this, view));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    public void onBind(Unbinder unbinder) {
        this.mUnbinder = unbinder;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mContentId == -1)
            throw new RuntimeException("You need to call setContentView before onCreateView");
        return inflater.inflate(mContentId, container, false);
    }

    protected abstract BasePresenter getPresenter();

    protected void hideKeyboard() {
        getBaseActivity().hideKeyboard();
    }

    protected BaseActivity getBaseActivity() {
        return ((BaseActivity) getActivity());
    }

    protected FragmentNavigator getFragmentNavigator() {
        return ((BaseActivity) getActivity()).getFragmentNavigator();
    }

    protected ToolbarManager getToolbarManager() {
        return ((BaseActivity) getActivity()).getToolbarManager();
    }

}
