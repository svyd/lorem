package com.svyd.wodes.presentation.project.delivery.base;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Svyd on 09.09.2016.
 */
public abstract class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }
}
