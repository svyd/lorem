package com.svyd.wodes.presentation.project.parcel.model;

import com.google.gson.annotations.SerializedName;
import com.svyd.wodes.presentation.project.delivery.model.SimpleLocationModel;
import com.svyd.wodes.presentation.project.delivery.model.SizeModel;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import java.io.Serializable;

/**
 * Created by Svyd on 13.09.2016.
 */
public class ParcelEntity implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("visits")
    private int visits;

    @SerializedName("price")
    private String price;

    @SerializedName("startDate")
    private String startDate;

    @SerializedName("from")
    private SimpleLocationModel fromLocation;

    @SerializedName("to")
    private SimpleLocationModel toLocation;

    @SerializedName("size")
    private SizeModel size;

    @SerializedName("owner")
    private ProfileInfo owner;

    @SerializedName("image")
    private String  image;

    public String getImage() {
        return image;
    }

    public String getPrice() {
        return price;
    }

    public String getId() {
        return id;
    }

    public int getVisits() {
        return visits;
    }

    public SimpleLocationModel getFromLocation() {
        return fromLocation;
    }

    public SimpleLocationModel getToLocation() {
        return toLocation;
    }

    public ProfileInfo getOwner() {
        return owner;
    }

    public SizeModel getSize() {
        return size;
    }

    public String getDescription() {
        return description;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getTitle() {
        return title;
    }

}
