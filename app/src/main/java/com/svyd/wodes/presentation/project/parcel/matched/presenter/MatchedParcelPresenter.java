package com.svyd.wodes.presentation.project.parcel.matched.presenter;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.svyd.wodes.R;
import com.svyd.wodes.data.dialog.model.DialogEntity;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.parcel.matched.contract.IMatchedParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.matched.contract.IMatchedParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import java.util.List;

import rx.Observer;

/**
 * Created by Svyd on 20.09.2016.
 */
public class MatchedParcelPresenter implements IMatchedParcelPresenter {

    private static final String TAG = MatchedParcelPresenter.class.getSimpleName();
    private PostInteractor<SearchRequest> mInteractor;
    private PostInteractor<InviteRequest> mInviteInteractor;
    private IMatchedParcelView mView;
    private SearchRequest mRequest;
    private InviteRequest mInviteRequest;
    private List<ParcelItem> mItems;

    public MatchedParcelPresenter(Bundle args,
                                  IMatchedParcelView view,
                                  PostInteractor<SearchRequest> interactor,
                                  PostInteractor<InviteRequest> inviteInteractor) {
        initRequest(args);
        mView = view;
        mInteractor = interactor;
        mInviteInteractor = inviteInteractor;
    }

    private void initRequest(Bundle args) {
        mRequest = (SearchRequest) args.getSerializable(Constants.Extra.EXTRA_REQUEST);
        mInviteRequest = (InviteRequest) args.getSerializable(Constants.Extra.EXTRA_INVITE_REQUEST);
    }

    @Override
    public void initialize() {
        mView.setToolbarTitle(WodesApplication.getStringResource(R.string.title_matched_parcels));
        mView.showProgress();
        mInteractor.execute(mRequest, new ParcelObserver());
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
    @Override
    public void onItemClick(int index, ParcelItem item, View view) {
        mInviteRequest.setParcel(item.getEntity().getId());
        mView.showAcceptDialog();
    }

    @Override
    public void onLoadMore() {
        mRequest.incrementOffset();
        mInteractor.execute(mRequest, new ParcelObserver());
    }

    void onSetItems(List<ParcelItem> items) {
        mItems = items;
        mView.setItems(mItems);
    }

    void onAddItems(List<ParcelItem> items) {
        mItems.addAll(items);
        mView.notifyChanges();
    }

    boolean toAdd() {
        return mRequest.getOffset() > 0;
    }

    void doOnNext(List<ParcelItem> items) {
        if (items.size() > 0) {
            mView.enableScrollHandling();
            if (toAdd()) {
                onAddItems(items);
            } else {
                onSetItems(items);
            }
        } else {
            if (mRequest.getOffset() == 0) {
                mView.showStubText();
            }
        }
    }

    @Override
    public void onAccept() {
        mInviteInteractor.execute(mInviteRequest, new InviteObserver());
    }

    private class InviteObserver implements Observer<DialogEntity> {

        @Override
        public void onCompleted() {
            Log.d(TAG, "onCompleted() called with: " + "");
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "onError() called with: " + "e = [" + e + "]");
        }

        @Override
        public void onNext(DialogEntity dialog) {
            mView.navigateHome();
            Log.d(TAG, "onNext() called with: " + "dialog = [" + dialog + "]");
        }
    }

    private class ParcelObserver implements Observer<List<ParcelItem>> {

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            mView.hideProgress();
        }

        @Override
        public void onNext(List<ParcelItem> deliveryItems) {
            doOnNext(deliveryItems);
        }
    }
}
