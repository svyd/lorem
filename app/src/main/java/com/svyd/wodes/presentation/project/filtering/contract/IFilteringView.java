package com.svyd.wodes.presentation.project.filtering.contract;

import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.global.model.SearchRequest;

import java.util.List;

/**
 * Created by Svyd on 20.09.2016.
 */
public interface IFilteringView extends BaseView {
    void navigateBackWithResult(SearchRequest request);
    void setArrival(String text);
    void setDeparture(String text);
    void showArrivalPopup(List<String> places);
    void showDeparturePopup(List<String> places);
    void showArrivalProgress(boolean show);
    void showDepartureProgress(boolean show);
    void showDatePicker(int year, int month, int day);
    void setDate(String date);
}
