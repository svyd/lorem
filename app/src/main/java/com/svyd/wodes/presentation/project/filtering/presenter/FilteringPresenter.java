package com.svyd.wodes.presentation.project.filtering.presenter;

import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.base.tool.DateTimeUtility;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.PredictionsModel;
import com.svyd.wodes.presentation.project.delivery.model.SimpleLocationModel;
import com.svyd.wodes.presentation.project.filtering.contract.IFilteringPresenter;
import com.svyd.wodes.presentation.project.filtering.contract.IFilteringView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observer;
import rx.subjects.PublishSubject;

/**
 * Created by Svyd on 20.09.2016.
 */
public class FilteringPresenter implements IFilteringPresenter {

    private PostInteractor<String> mPlacesInteractor;
    private PostInteractor<List<String>> mPlacesDetailsInteractor;
    private PredictionsModel mDeparturePredictions;
    private PredictionsModel mArrivalPredictions;
    private List<String> mDeparturePredictionNames;
    private List<String> mArrivalPredictionNames;
    private SearchRequest mRequest;

    private PublishSubject<String> mDepartureSubject;
    private PublishSubject<String> mArrivalSubject;

    private IFilteringView mView;

    public FilteringPresenter(IFilteringView view,
                              PostInteractor<String> placesInteractor,
                              PostInteractor<List<String>> detailsInteractor) {
        mView = view;
        mPlacesInteractor = placesInteractor;
        mPlacesDetailsInteractor = detailsInteractor;
    }

    private void initSubjects() {
        mDepartureSubject = PublishSubject.create();
        mArrivalSubject = PublishSubject.create();

        mDepartureSubject.debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(this::completeDeparture);

        mArrivalSubject.debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(this::completeArrival);
    }

    private void completeDeparture(String string) {
        mPlacesInteractor.execute(string, new DepartureLocationObserver());
    }

    private void completeArrival(String string) {
        mPlacesInteractor.execute(string, new ArrivalLocationObserver());
    }

    @Override
    public void onDepartureTextChanged(String text) {
        if (text.length() == 0) {
            return;
        }
        mDepartureSubject.onNext(text);
        mView.showDepartureProgress(true);
    }

    @Override
    public void onArrivalTextChanged(String text) {
        if (text.length() == 0) {
            return;
        }
        mArrivalSubject.onNext(text);
        mView.showArrivalProgress(true);
    }

    @Override
    public void onDepartureItemSelected(int position) {
        mRequest.setReferenceFrom(mDeparturePredictions.predictions[position].reference);
        mView.setDeparture(mDeparturePredictionNames.get(position));
    }

    @Override
    public void onArrivalItemSelected(int position) {
        mRequest.setReferenceTo(mArrivalPredictions.predictions[position].reference);
        mView.setArrival(mArrivalPredictionNames.get(position));
    }

    @Override
    public void onDoneClick() {
        if (validate()) {
            List<String> references = Arrays.asList(mRequest.getReferenceFrom(), mRequest.getReferenceTo());
            mView.showProgress();
            mPlacesDetailsInteractor.execute(references, new PlacesDetailsObserver());
        }
    }

    private boolean validate() {
        return mRequest.getReferenceFrom() != null &&
                mRequest.getReferenceTo() != null;
    }

    @Override
    public void onDateClick() {
        DateTime dateTime = DateTime.now().withTimeAtStartOfDay();
        mView.showDatePicker(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth());
    }

    @Override
    public void onDateSet(int year, int month, int day) {
        Calendar calendar = new GregorianCalendar(year, month, day);
        mRequest.setDate(DateTimeUtility.toIso(calendar));
        mView.setDate(DateTimeUtility.formatDate(mRequest.getDate(), false));
    }

    @Override
    public void initialize() {
        initSubjects();
        initRequest();
    }

    private void initRequest() {
        mRequest = new SearchRequest.Builder()
                .setLimit(10)
                .setOffset(0)
                .build();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    private void handleDeparturePredictions(PredictionsModel predictions) {
        mDeparturePredictions = predictions;
        mDeparturePredictionNames = new ArrayList<>();
        for (PredictionsModel.Prediction prediction : predictions.predictions) {
            mDeparturePredictionNames.add(prediction.description);
        }
        mView.showDeparturePopup(mDeparturePredictionNames);
    }

    private void handleArrivalPredictions(PredictionsModel predictions) {
        mArrivalPredictions = predictions;
        mArrivalPredictionNames = new ArrayList<>();
        for (PredictionsModel.Prediction prediction : predictions.predictions) {
            mArrivalPredictionNames.add(prediction.description);
        }
        mView.showArrivalPopup(mArrivalPredictionNames);
    }

    private class PlacesDetailsObserver implements Observer<List<SimpleLocationModel>> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(List<SimpleLocationModel> locations) {
            mView.hideProgress();
            handlePlacesDetails(locations);
        }
    }

    private void handlePlacesDetails(List<SimpleLocationModel> locations) {
        mRequest.setDeparture(locations.get(0));
        mRequest.setArrival(locations.get(1));
        mView.navigateBackWithResult(mRequest);
    }

    private class DepartureLocationObserver implements Observer<PredictionsModel> {

        @Override
        public void onCompleted() {
            mView.showDepartureProgress(false);
        }

        @Override
        public void onError(Throwable e) {
            mView.showDepartureProgress(false);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            handleDeparturePredictions(predictionsModel);
        }
    }

    private class ArrivalLocationObserver implements Observer<PredictionsModel> {

        @Override
        public void onCompleted() {
            mView.showArrivalProgress(false);
        }

        @Override
        public void onError(Throwable e) {
            mView.showArrivalProgress(false);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            handleArrivalPredictions(predictionsModel);
        }
    }
}
