package com.svyd.wodes.presentation.project.delivery.my.presenter;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.create.CreateDeliveryActivity;
import com.svyd.wodes.presentation.project.delivery.details.DeliveryDetailsActivity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.delivery.my.contract.IMyDeliveryPresenter;
import com.svyd.wodes.presentation.project.delivery.my.contract.IMyDeliveryView;

import java.util.List;

import rx.Observer;

/**
 * Created by Svyd on 07.09.2016.
 */
public class MyDeliveryPresenter implements IMyDeliveryPresenter {

    private ListRequest mRequest;
    private PostInteractor<ListRequest> mInteractor;
    private List<DeliveryItem> mItems;
    private IMyDeliveryView mView;

    public MyDeliveryPresenter(IMyDeliveryView view, PostInteractor<ListRequest> interactor) {
        mInteractor = interactor;
        mView = view;
        initRequest();
    }

    private void initRequest() {
        mRequest = new ListRequest.Builder()
                .setLimit(10)
                .setOffset(0)
                .setType(Constants.ITEM_TYPE_MY)
                .build();
    }

    @Override
    public void initialize() {
        mView.setToolbarTitle(WodesApplication.getStringResource(R.string.title_my_deliveries));
        mView.showProgress();
        mInteractor.execute(mRequest, new DeliveryObserver());
    }

    protected IMyDeliveryView getView() {
        return mView;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void onLoadMore() {
        mRequest.incrementOffset();
        mInteractor.execute(mRequest, new DeliveryObserver());
    }

    void onSetItems(List<DeliveryItem> items) {
        mItems = items;
        mView.setItems(mItems);
    }

    void onAddItems(List<DeliveryItem> items) {
        mItems.addAll(items);
        mView.notifyChanges();
    }

    boolean toAdd() {
        return mRequest.getOffset() > 0;
    }

    void doOnNext(List<DeliveryItem> items) {
        if (items.size() > 0) {
            mView.enableScrollHandling();
            if (toAdd()) {
                onAddItems(items);
            } else {
                onSetItems(items);
            }
        } else {
            if (mRequest.getOffset() == 0) {
                mView.showStubText();
            }
        }
    }

    @Override
    public void onCreateItem() {
        mView.navigateToAddDelivery();
    }

    @Override
    public void handleResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case DeliveryDetailsActivity.REQUEST_CODE_EDIT:
                    onEditItem(data);
                    break;
                case CreateDeliveryActivity.REQUEST_CREATE_DELIVERY:
                    onAddItem(data);
                    break;
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(MenuInflater menuInflater, Menu menu) {
        menuInflater.inflate(R.menu.add_item, menu);
    }

    @Override
    public void onAccept() {

    }

    private void onEditItem(Intent data) {
        int position = data.getIntExtra(Constants.Extra.EXTRA_POSITION, -1);
        if (position != -1) {
            DeliveryItem item = (DeliveryItem) data.getSerializableExtra(Constants.Extra.EXTRA_DELIVERY);
            mView.editItem(item, position);
        }
    }

    private void onAddItem (Intent data) {
        DeliveryItem item = (DeliveryItem) data.getSerializableExtra(Constants.Extra.EXTRA_DELIVERY);
        mView.addItem(item);
        mView.hideStubText();
    }

    @Override
    public void onItemClick(int index, DeliveryItem item, View view) {
        mView.navigateToDetails(index, item, view);
    }

    private class DeliveryObserver implements Observer<List<DeliveryItem>> {

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            mView.hideProgress();
        }

        @Override
        public void onNext(List<DeliveryItem> deliveryItems) {
            doOnNext(deliveryItems);
        }
    }
}
