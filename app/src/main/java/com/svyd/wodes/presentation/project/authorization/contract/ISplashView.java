package com.svyd.wodes.presentation.project.authorization.contract;

import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

/**
 * Created by Svyd on 05.09.2016.
 */
public interface ISplashView extends BaseView {
    void startAuthorization();
    void startProfile(ProfileInfo info);
}
