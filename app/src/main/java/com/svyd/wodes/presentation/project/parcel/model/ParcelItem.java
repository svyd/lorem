package com.svyd.wodes.presentation.project.parcel.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Svyd on 05.07.2016.
 */
public class ParcelItem implements Serializable {
    private String title;
    private String from;
    private String to;
    private String deliveryTime;
    private String size;
    private String time;
    private String views;

    private ParcelEntity mEntity;

    public void setEntity(ParcelEntity entity) {
        mEntity = entity;
    }

    public ParcelEntity getEntity() {
        return mEntity;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public String getFrom() {
        return from;
    }

    public String getSize() {
        return size;
    }

    public String getTime() {
        return time;
    }

    public String getTitle() {
        return title;
    }

    public String getTo() {
        return to;
    }



    public String getViews() {
        return views;
    }

    public static class Builder {
        private String title;
        private String from;
        private String to;
        private String deliveryTime;
        private String size;
        private String time;
        private String views;

        private ParcelEntity mEntity;

        public Builder setEntity(ParcelEntity entity) {
            mEntity = entity;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setDeliveryTime(String deliveryTime) {
            this.deliveryTime = deliveryTime;
            return this;
        }

        public Builder setFrom(String from) {
            this.from = from;
            return this;
        }

        public Builder setSize(String size) {
            this.size = size;
            return this;
        }

        public Builder setTime(String time) {
            this.time = time;
            return this;
        }

        public Builder setTo(String to) {
            this.to = to;
            return this;
        }

        public Builder setViews(String views) {
            this.views = views;
            return this;
        }

        public ParcelItem build() {
            ParcelItem item = new ParcelItem();

            item.setDeliveryTime(deliveryTime);
            item.setFrom(from);
            item.setTo(to);
            item.setSize(size);
            item.setTitle(title);
            item.setTime(time);
            item.setViews(views);
            item.setEntity(mEntity);

            return item;
        }
    }
}
