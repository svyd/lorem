package com.svyd.wodes.presentation.project.authorization.contract;

import android.os.Bundle;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;

/**
 * Created by Svyd on 24.08.2016.
 */
public interface IForgotPasswordPresenter extends BasePresenter {
    void onEmailChanged(String email);
    void onRequestClick();
    void onSaveInstanceState(Bundle state);
    void onRestoreInstanceState(Bundle state);
}
