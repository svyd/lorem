package com.svyd.wodes.presentation.project.authorization.contract;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.presentation.base.mvp.BaseView;

/**
 * Created by Svyd on 24.08.2016.
 */
public interface ForgotPasswordView extends BaseView {
    AuthRepository getScopedRepository();
    void navigateBack();
    void showToast(String message);
    void setEmailError(String error);
    void showEmailProgress(boolean show);
    void setEmailChecked(boolean checked);
}
