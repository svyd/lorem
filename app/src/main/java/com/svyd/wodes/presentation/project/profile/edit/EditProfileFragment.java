package com.svyd.wodes.presentation.project.profile.edit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.svyd.wodes.R;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.tool.CircularTransform;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.profile.edit.contract.IEditProfilePresenter;
import com.svyd.wodes.presentation.project.profile.edit.contract.IEditProfileView;
import com.svyd.wodes.presentation.project.home.image_picker.ImagePickerFragment;
import com.svyd.wodes.presentation.project.profile.edit.presenter.EditProfilePresenterFactory;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Svyd on 30.08.2016.
 */
public class EditProfileFragment extends BaseFragment implements IEditProfileView {

    @BindView(R.id.etName_FEP)
    EditText etName;

    @BindView(R.id.etPhone_FEP)
    EditText etPhone;

    @BindView(R.id.etMail_FEP)
    EditText etMail;

    @BindView(R.id.etBirthday_FEP)
    EditText etBirthday;

    @BindView(R.id.rbFemale_FEP)
    RadioButton rbFemale;

    @BindView(R.id.rbMale_FEP)
    RadioButton rbMale;

    @BindView(R.id.rgGender_FSU)
    RadioGroup rgGender;

    @BindView(R.id.ivAvatar_FEP)
    ImageView ivAvatar;

    private ProgressDialog mProgress;
    private BitmapTransformation mTransformation;
    private IEditProfilePresenter mPresenter;

    public static EditProfileFragment newInstance(Bundle args) {


        EditProfileFragment fragment = new EditProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_edit_profile);
        setHasOptionsMenu(true);
        initPresenter();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initUi();
        mPresenter.initialize();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_action, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miDone_MA:
                mPresenter.onDone();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private RadioGroup.OnCheckedChangeListener mGenderListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.rbFemale_FEP:
                    mPresenter.onGenderFemaleSelected();
                    break;
                case R.id.rbMale_FEP:
                    mPresenter.onGenderMaleSelected();
                    break;
            }
        }
    };

    private void initPresenter() {
        mPresenter = new EditProfilePresenterFactory().providePresenter(getArguments(), this);
    }

    private void initUi() {
        mProgress = new ProgressDialog(getActivity());
        mTransformation = new CircularTransform(getActivity());
        mProgress.setTitle(WodesApplication.getStringResource(R.string.txt_wait));
        mProgress.setMessage(WodesApplication.getStringResource(R.string.user_update));
        rgGender.setOnCheckedChangeListener(mGenderListener);
    }

    @OnTextChanged(R.id.etName_FEP)
    void onNameChanged(CharSequence name) {
        mPresenter.onNameChanged(name.toString());
    }

    @OnTextChanged(R.id.etPhone_FEP)
    void onPhoneChanged(CharSequence phone) {
        mPresenter.onNumberChanged(phone.toString());
    }

    @OnTextChanged(R.id.etMail_FEP)
    void onMailChanged(CharSequence mail) {
        mPresenter.onEmailChanged(mail.toString());
    }

    @OnClick(R.id.ivAvatar_FEP)
    void onAvatarClick() {
        mPresenter.onAvatarClick();
    }

    @OnClick(R.id.etBirthday_FEP)
    void onBirthdayClick() {
        mPresenter.onBirthdayClick();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onProfileUpdated(ProfileInfo info) {
        ((IEditProfileController) getActivity()).onProfileUpdated(info);
    }

    @Override
    public void setName(String name) {
        etName.setText(name);
    }

    @Override
    public void setNumber(String number) {
        etPhone.setText(number);
    }

    @Override
    public void setEmail(String email) {
        etMail.setText(email);
    }

    @Override
    public void setAvatar(String path) {
        Glide.with(getActivity())
                .load(path)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .centerCrop()
                .animate(android.R.anim.fade_in)
                .transform(mTransformation)
                .error(R.drawable.ic_arrow)
                .into(ivAvatar);
    }

    @Override
    public void setBirthday(String birthday) {
        etBirthday.setText(birthday);
    }

    @Override
    public void setGenderFemale() {
        rbFemale.setChecked(true);
    }

    @Override
    public void setGenderMale() {
        rbMale.setChecked(true);
    }

    @Override
    public void showImagePicker() {
        hideKeyboard();
        new Handler().postDelayed(() -> ImagePickerFragment.showWithTarget(this, ImagePickerFragment.PICTURE_REQUEST_CODE), 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == ImagePickerFragment.PICTURE_REQUEST_CODE) {
            setUrlToPresenter(data.getStringExtra(Constants.Extra.EXTRA_STRING));
        }
    }

    private void setUrlToPresenter(String _localPath) {
        if (mPresenter == null)
            return;

        mPresenter.setAvatarPath(_localPath);
    }

    @Override
    public void showDatePicker(int _year, int _month, int _day) {
        DatePickerDialog.newInstance(mBirthdayListener, _year, _month, _day)
                .show(getActivity().getFragmentManager(), DatePickerDialog.class.getSimpleName());
    }

    private DatePickerDialog.OnDateSetListener mBirthdayListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            mPresenter.onBirthdayChanged(year, monthOfYear, dayOfMonth);
        }
    };

    @Override
    public void showProgress() {
        mProgress.show();
    }

    @Override
    public void hideProgress() {
        mProgress.cancel();
    }
}
