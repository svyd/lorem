package com.svyd.wodes.presentation.project.authorization.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.tool.AnimationListenerAdapter;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.project.authorization.activity.AuthListener;
import com.svyd.wodes.presentation.project.authorization.contract.ISplashPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.ISplashView;
import com.svyd.wodes.presentation.project.authorization.presenter.factory.SplashPresenterFactory;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import butterknife.BindView;

/**
 * Created by Svyd on 11.08.2016.
 */
public class SplashFragment extends BaseFragment implements ISplashView {

    @BindView(R.id.ivLogo_FS)
    ImageView ivLogo;

    @BindView(R.id.flContainer_FS)
    FrameLayout flContainer;

    private Handler mHandler;
    private Runnable mRunnable;

    private ISplashPresenter mPresenter;

    public static SplashFragment newInstance() {

        Bundle args = new Bundle();

        SplashFragment fragment = new SplashFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_splash);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mHandler = new Handler();
        mRunnable = () -> mPresenter.initialize();

        initPresenter();
    }

    private void initPresenter() {
        mPresenter = new SplashPresenterFactory().providePresenter(getArguments(), this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler.postDelayed(mRunnable, 1000);
    }

    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void startAuthorization() {
        ((AuthListener) getActivity()).fadeInSignUp();
    }

    @Override
    public void startProfile(final ProfileInfo info) {
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out_scale_down);
        animation.setAnimationListener(new AnimationListenerAdapter() {

            @Override
            public void onAnimationEnd(Animation animation) {
                flContainer.setVisibility(View.INVISIBLE);
                ((AuthListener) getActivity()).navigateToProfile(info);
            }
        });
        flContainer.startAnimation(animation);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {
    }

}
