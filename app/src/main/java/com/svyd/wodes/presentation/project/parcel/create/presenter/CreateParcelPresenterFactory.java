package com.svyd.wodes.presentation.project.parcel.create.presenter;

import android.os.Bundle;

import com.google.gson.Gson;
import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.parcel.mapper.ParcelMapper;
import com.svyd.wodes.data.parcel.mapper.ParcelsMapper;
import com.svyd.wodes.data.parcel.repository.IParcelRepository;
import com.svyd.wodes.data.parcel.repository.ParcelRepository;
import com.svyd.wodes.data.parcel.repository.ParcelService;
import com.svyd.wodes.data.places.GooglePlacesRepository;
import com.svyd.wodes.data.places.GooglePlacesService;
import com.svyd.wodes.data.places.IPlacesRepository;
import com.svyd.wodes.data.size.ISizeRepository;
import com.svyd.wodes.data.size.ISizesCache;
import com.svyd.wodes.data.size.PreferenceSizeCache;
import com.svyd.wodes.data.size.SizeRepository;
import com.svyd.wodes.data.size.SizeService;
import com.svyd.wodes.domain.base.Interactor;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.parcel.EditParcelInteractor;
import com.svyd.wodes.domain.size.SizeInteractor;
import com.svyd.wodes.domain.parcel.CreateParcelInteractor;
import com.svyd.wodes.domain.places.PredictionsInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.parcel.create.contract.ICreateParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.create.contract.ICreateParcelView;
import com.svyd.wodes.presentation.project.parcel.create.presenter.impl.CreateParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.create.presenter.impl.EditParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.model.ParcelEntity;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelRequest;

import java.util.List;

/**
 * Created by Svyd on 14.09.2016.
 */
public class CreateParcelPresenterFactory implements PresenterFactory<ICreateParcelPresenter, ICreateParcelView> {

    private IParcelRepository mDeliveryRepository;
    private IPlacesRepository mPlacesRepository;

    @Override
    public ICreateParcelPresenter providePresenter(Bundle args, ICreateParcelView view) {
        int flow = args.getInt(Constants.Flow.FLOW_KEY);

        switch (flow) {
            case Constants.Flow.EDIT_ITEM:
                return provideEditPresenter(args, view);
            case Constants.Flow.CREATE_ITEM:
                return provideCreatePresenter(args, view);
            default:
                throw new IllegalArgumentException("Unsupported flow: " + flow);
        }
    }

    private ICreateParcelPresenter provideEditPresenter(Bundle args, ICreateParcelView view) {
        return new EditParcelPresenter(args, provideEditInteractor(), view, providePlacesInteractor(), provideCreateInteractor(), provideSizeInteractor());
    }

    private PostInteractor<ParcelRequest> provideEditInteractor() {
        return new EditParcelInteractor(provideParcelRepository());
    }

    private ICreateParcelPresenter provideCreatePresenter(Bundle args, ICreateParcelView view) {
        return new CreateParcelPresenter(view, providePlacesInteractor(), provideCreateInteractor(), provideSizeInteractor());
    }


    private PostInteractor<ParcelRequest> provideCreateInteractor() {
        return new CreateParcelInteractor(provideParcelRepository(), providePlacesRepository());
    }

    private Interactor provideSizeInteractor() {
        return new SizeInteractor(provideSizeRepository());
    }

    private ISizeRepository provideSizeRepository() {
        return new SizeRepository(provideSizeService(), provideSizeCache());
    }

    private ISizesCache provideSizeCache() {
        return new PreferenceSizeCache();
    }

    private SizeService provideSizeService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), SizeService.class);
    }

    private IParcelRepository provideParcelRepository() {
        if (mDeliveryRepository == null) {
            mDeliveryRepository =  new ParcelRepository(new Gson(),
                    provideParcelService(),
                    provideParcelsMapper(),
                    provideParcelMapper());
//            mDeliveryRepository =  new StubParcelRepository();
        }
        return mDeliveryRepository;
    }

    private ParcelService provideParcelService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), ParcelService.class);
    }

    private PostInteractor<String> providePlacesInteractor() {
        return new PredictionsInteractor(providePlacesRepository());
    }

    private IPlacesRepository providePlacesRepository() {
        if (mPlacesRepository == null) {
            mPlacesRepository = new GooglePlacesRepository(providePlacesService());
        }
        return mPlacesRepository;
    }

    TypeMapper<ParcelEntity, ParcelItem> provideParcelMapper() {
        return new ParcelMapper();
    }

    TypeMapper<DataWrapper<List<ParcelEntity>>, List<ParcelItem>> provideParcelsMapper() {
        return new ParcelsMapper(provideParcelMapper());
    }

    private GooglePlacesService providePlacesService() {
        return new ServiceProvider().provideGooglePlacesService(WodesApplication.getContext(), GooglePlacesService.class);
    }
}
