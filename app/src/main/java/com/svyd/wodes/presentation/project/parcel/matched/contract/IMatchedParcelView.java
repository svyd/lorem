package com.svyd.wodes.presentation.project.parcel.matched.contract;

import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import java.util.List;

/**
 * Created by Svyd on 20.09.2016.
 */
public interface IMatchedParcelView extends BaseView {
    void setItems(List<ParcelItem> items);
    void notifyChanges();
    void enableScrollHandling();
    void showStubText();
    void setToolbarTitle(String title);
    void showAcceptDialog();
    void navigateHome();
}
