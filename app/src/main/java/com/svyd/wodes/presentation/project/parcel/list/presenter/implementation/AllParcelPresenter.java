package com.svyd.wodes.presentation.project.parcel.list.presenter.implementation;

import android.app.Activity;
import android.content.Intent;

import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.filtering.view.FilteringActivity;
import com.svyd.wodes.presentation.project.parcel.details.view.ParcelDetailsActivity;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

/**
 * Created by Svyd on 14.09.2016.
 */
public class AllParcelPresenter extends AbstractParcelPresenter {


    public AllParcelPresenter(IParcelView view, PostInteractor<ListRequest> interactor, PostInteractor<SearchRequest> searchInteractor) {
        super(view, interactor, searchInteractor);
    }

    @Override
    public void initialize() {
        ListRequest request = new ListRequest();
        request.setType(Constants.ITEM_TYPE_ALL);
        request.setLimit(10);
        request.setOffset(0);
        setRequest(request);
        requestItems();
        getView().setToolbarTitle("All parcels");
    }

    @Override
    public void handleResult(int requestCode, int resultCode, Intent data) {
        super.handleResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ParcelDetailsActivity.REQUEST_CODE_VIEW_PARCEL:
                    onIncrementView(data);
                    break;

            }
        }
    }

    private void onIncrementView(Intent data) {
        int position = data.getIntExtra(Constants.Extra.EXTRA_POSITION, -1);
        ParcelItem item = getItems().get(position);
        item.setViews(String.valueOf(Integer.parseInt(item.getViews()) + 1));
        getView().notifyChanges();
    }
}
