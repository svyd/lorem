package com.svyd.wodes.presentation.project.authorization.contract;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;

/**
 * Created by Svyd on 08.08.2016.
 */
public interface PhoneNumberView extends BaseView {
    String getNumber();
    void showInputError(String error);
    void showToast(String message);
    void navigateNext(SignUpRequest request);

    void setPhoneChecked(boolean checked);
    void showPhoneProgress(boolean show);
    void setNextEnabled(boolean enabled);

    void dismissKeyboard();

    AuthRepository provideScopedRepository();
}
