package com.svyd.wodes.presentation.project.delivery.all.contract;

import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;

import java.util.List;

/**
 * Created by Svyd on 24.08.2016.
 */
public interface IDeliveryView extends BaseView {
    void setTitle(String title);
    void setItems(List<DeliveryItem> items);
    void enableLoadMore();
    void notifyChanges();
    void showStubText();
    void hideStubText();
    void showFab();
    void hideFab();
    void showListProgress();
    void hideListProgress();
}