package com.svyd.wodes.presentation.global.model;

import com.svyd.wodes.presentation.project.delivery.model.SimpleLocationModel;

import java.io.Serializable;

/**
 * Created by Svyd on 20.09.2016.
 */
public class SearchRequest implements Serializable {

    private SimpleLocationModel departure;
    private SimpleLocationModel arrival;

    private int limit;
    private int offset;

    private String referenceFrom;
    private String referenceTo;
    private String date;

    public SearchRequest() {

    }

    public SearchRequest(int limit, int offset) {
        this.limit = limit;
        this.offset = offset;
    }

    public String getReferenceTo() {
        return referenceTo;
    }

    public String getReferenceFrom() {
        return referenceFrom;
    }

    public void setArrival(SimpleLocationModel arrival) {
        this.arrival = arrival;
    }

    public void setDeparture(SimpleLocationModel departure) {
        this.departure = departure;
    }

    public SimpleLocationModel getDeparture() {
        return departure;
    }

    public SimpleLocationModel getArrival() {
        return arrival;
    }

    public void incrementOffset() {
        offset += limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setReferenceFrom(String referenceFrom) {
        this.referenceFrom = referenceFrom;
    }

    public void setReferenceTo(String referenceTo) {
        this.referenceTo = referenceTo;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public static class Builder {

        private int limit;
        private int offset;
        private SimpleLocationModel departure;
        private SimpleLocationModel arrival;

        public Builder setArrival(SimpleLocationModel arrival) {
            this.arrival = arrival;
            return this;
        }

        public Builder setDeparture(SimpleLocationModel departure) {
            this.departure = departure;
            return this;
        }

        public Builder setLimit(int limit) {
            this.limit = limit;
            return this;
        }

        public Builder setOffset(int offset) {
            this.offset = offset;
            return this;
        }

        public SearchRequest build() {
            SearchRequest request = new SearchRequest();
            request.setLimit(limit);
            request.setOffset(offset);
            request.setArrival(arrival);
            request.setDeparture(departure);

            return request;
        }

    }
}
