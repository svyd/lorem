package com.svyd.wodes.presentation.project.authorization.oauth.strategy.networks;

import android.content.Intent;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.model.OauthRequest;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.AuthorizationStrategy;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 18.07.2016.
 */
public class FacebookStrategy implements AuthorizationStrategy {
    protected CallbackManager mCallbackManager;
    protected WeakReference<OauthCallback> mLogInCallback;

    public FacebookStrategy(CallbackManager _manager) {
        mCallbackManager = _manager;
        LoginManager.getInstance().registerCallback(mCallbackManager, mCallback);
    }

    @Override
    public void logIn(BaseFragment _fragment, OauthCallback _callback) {
        LoginManager.getInstance().logOut();
        mLogInCallback = new WeakReference<>(_callback);
        List<String> permissions = new ArrayList<>();
        LoginManager.getInstance().logInWithPublishPermissions(_fragment, permissions);
    }

    @Override
    public void onActivityResult(int _requestCode, int _resultCode, Intent _intent) {
        mCallbackManager.onActivityResult(_requestCode, _resultCode, _intent);
    }

    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            if (mLogInCallback.get() == null)
                return;

            OauthRequest model = new OauthRequest.Builder()
                    .setToken(loginResult.getAccessToken().getToken())
                    .setNetwork(Constants.AuthType.FACEBOOK).build();

            mLogInCallback.get().onSuccess(model);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {
            if (error instanceof FacebookAuthorizationException) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                }
            }

            mLogInCallback.get().onError(error);
        }
    };
}
