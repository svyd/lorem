package com.svyd.wodes.presentation.project.parcel.matched.contract;

import com.svyd.wodes.presentation.base.OnItemClickListener;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

/**
 * Created by Svyd on 20.09.2016.
 */
public interface IMatchedParcelPresenter extends BasePresenter, OnItemClickListener<ParcelItem>, LazyLoadListener.LoadMoreListener {
    void onAccept();
}
