package com.svyd.wodes.presentation.project.authorization.presenter.factory;

import android.os.Bundle;

import com.svyd.wodes.domain.authorization.CodeInteractor;
import com.svyd.wodes.domain.authorization.PhoneNumberInteractor;
import com.svyd.wodes.domain.authorization.SignUpInteractor;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.contract.ICodeVerificationPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.CodeVerificationView;
import com.svyd.wodes.presentation.project.authorization.presenter.impl.CodeAddPresenter;
import com.svyd.wodes.presentation.project.authorization.presenter.impl.CodeEditPresenter;
import com.svyd.wodes.presentation.project.authorization.presenter.impl.CodeSignUpPresenter;

/**
 * Created by Svyd on 13.08.2016.
 */
public class CodeVerificationPresenterFactory
        implements PresenterFactory<ICodeVerificationPresenter, CodeVerificationView> {

    @Override
    public ICodeVerificationPresenter providePresenter(Bundle args, CodeVerificationView view) {

        if (args != null && args.containsKey(Constants.Flow.FLOW_KEY)) {
            int flow = args.getInt(Constants.Flow.FLOW_KEY);
            switch (flow) {
                case Constants.Flow.AUTH_SIGN_UP:
                    return provideCodeSignUpPresenter(view);
                case Constants.Flow.AUTH_ADD_NUMBER:
                    return provideCodeAddPresenter(view);
                case Constants.Flow.EDIT_VERIFY_NUMBER:
                    return provideCodeEditPresenter(view);
                default:
                    throw new IllegalArgumentException("Unsupported flow: " + flow);
            }
        } else {
            throw new IllegalArgumentException("No specified flow");
        }
    }

    private ICodeVerificationPresenter provideCodeEditPresenter(CodeVerificationView view) {
        return new CodeEditPresenter(
                view,
                new PhoneNumberInteractor(view.provideScopedRepository()),
                provideCodeInteractor(view));
    }

    private ICodeVerificationPresenter provideCodeAddPresenter(CodeVerificationView view) {
        return new CodeAddPresenter(
                view,
                new PhoneNumberInteractor(view.provideScopedRepository()),
                provideCodeInteractor(view));
    }

    private CodeInteractor provideCodeInteractor(CodeVerificationView view) {
        return new CodeInteractor(view.provideScopedRepository());
    }

    private ICodeVerificationPresenter provideCodeSignUpPresenter(CodeVerificationView view) {
        SignUpInteractor signUpInteractor =
                new SignUpInteractor(view.provideScopedRepository());

        return new CodeSignUpPresenter(
                view,
                new PhoneNumberInteractor(view.provideScopedRepository()),
                signUpInteractor);
    }
}
