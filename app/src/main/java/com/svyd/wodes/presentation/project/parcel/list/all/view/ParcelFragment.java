package com.svyd.wodes.presentation.project.parcel.list.all.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.filtering.view.FilteringActivity;
import com.svyd.wodes.presentation.project.parcel.details.view.ParcelDetailsActivity;
import com.svyd.wodes.presentation.project.parcel.list.adapter.ParcelAdapter;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.presentation.project.parcel.list.presenter.ParcelPresenterFactory;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

import static android.view.View.INVISIBLE;

/**
 * Created by Svyd on 05.07.2016.
 */
public class ParcelFragment extends BaseFragment implements IParcelView {

    @BindView(R.id.rvItems_FL)
    RecyclerView rvParcels;

    @BindView(R.id.tvTitle_TL)
    TextView tvTitle;

    @BindView(R.id.tvNoItems_FL)
    TextView tvNoItems;

    @BindView(R.id.pbList_FL)
    SmoothProgressBar pbList;

    @BindView(R.id.fabRemoveFilter_AL)
    FloatingActionButton fabRemoveFilter;

    private IParcelPresenter mPresenter;
    private PresenterFactory<IParcelPresenter, IParcelView> mFactory;
    private ParcelAdapter mAdapter;
    private LazyLoadListener mScrollListener;

    public static ParcelFragment newInstance(Bundle args) {

        ParcelFragment fragment = new ParcelFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_list);
    }

    @OnClick(R.id.btnFilter_TL)
    void onFilterClick(View view) {
        startFilteringActivity(view);
    }

    void startFilteringActivity(View view) {
        int[] position = new int[2];
        view.getLocationOnScreen(position);
        Intent intent = new Intent(getActivity(), FilteringActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("x", position[0] + view.getWidth() / 2);
        intent.putExtra("y", position[1] + view.getHeight() / 2);
        startActivityForResult(intent, FilteringActivity.REQUEST_CODE_FILTER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.handleResult(requestCode, resultCode, data);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        providePresenter();
        initPresenter();
        initList();
    }

    private void initList() {
        mScrollListener = new LazyLoadListener(mPresenter);
        mAdapter = new ParcelAdapter(getActivity());
        mAdapter.setOnItemClickListener(mPresenter);
        rvParcels.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvParcels.setAdapter(mAdapter);
        rvParcels.addOnScrollListener(mScrollListener);
        mAdapter.notifyDataSetChanged();
    }

    private void providePresenter() {
        mFactory = new ParcelPresenterFactory();
        mPresenter = mFactory.providePresenter(getArguments(), this);
    }

    private void initPresenter() {
        mPresenter.initialize();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setToolbarTitle(String title) {
        tvTitle.setText(title);
    }

    @Override
    public void setItems(List<ParcelItem> items) {
        mAdapter.setItems(items);
    }

    @Override
    public void navigateToDetails(int index, View view, ParcelItem item) {
        Intent intent = new Intent(getActivity(), ParcelDetailsActivity.class);

        intent.putExtra(Constants.Flow.FLOW_KEY, Constants.Flow.DETAILS_ALL);
        intent.putExtra(Constants.Extra.EXTRA_PARCEL, item);
        intent.putExtra(Constants.Extra.EXTRA_POSITION, index);

        startActivityForResult(intent, ParcelDetailsActivity.REQUEST_CODE_VIEW_PARCEL);
    }

    @Override
    public void navigateToCreate() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void notifyChanges() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void enableLoadMore() {
        mScrollListener.enable();
    }

    @Override
    public void showStubText() {
        tvNoItems.setText(R.string.no_parcels);
        tvNoItems.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideStubText() {
        tvNoItems.setVisibility(View.GONE);
    }

    @Override
    public void addItem(ParcelItem item) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void editItem(int position, ParcelItem item) {
        throw new UnsupportedOperationException();
    }

    @OnClick(R.id.fabRemoveFilter_AL)
    void OnFabClick() {
        mPresenter.onFabClick();
    }

    @Override
    public void showFab() {
        fabRemoveFilter.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFab() {
        fabRemoveFilter.setVisibility(View.GONE);
    }

    @Override
    public void showListProgress() {
        pbList.setVisibility(View.VISIBLE);
        pbList.progressiveStart();
    }

    @Override
    public void hideListProgress() {
        pbList.progressiveStop();
        pbList.setVisibility(INVISIBLE);
    }

    @Override
    public void showAcceptDialog() {

    }

    @Override
    public void navigateHome() {

    }
}
