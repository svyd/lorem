package com.svyd.wodes.presentation.project.authorization.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.svyd.wodes.R;
import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.ScopedInstance;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.activity.AuthListener;
import com.svyd.wodes.presentation.project.authorization.contract.IPhoneNumberPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.PhoneNumberView;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.authorization.presenter.factory.PhoneNumberPresenterFactory;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Svyd on 04.08.2016.
 */
public class PhoneNumberFragment extends BaseFragment implements PhoneNumberView {

    private IPhoneNumberPresenter mPresenter;
    private ProgressDialog mProgress;

    @BindView(R.id.etPhoneNumber_FPN)
    EditText etPhoneNumber;

    @BindView(R.id.ivCheckPhone_FPN)
    ImageView ivCheckPhone;

    @BindView(R.id.pbPhone_FPN)
    ProgressBar pbPhone;

    @BindView(R.id.btnNext_FPN)
    Button btnNext_FPN;

    public static PhoneNumberFragment newInstance(Bundle args) {

        PhoneNumberFragment fragment = new PhoneNumberFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_phone_number);
    }

    private void initUi() {
        etPhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle(WodesApplication.getStringResource(R.string.txt_wait));
        mProgress.setMessage(WodesApplication.getStringResource(R.string.txt_sending_phone));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUi();

        providePresenter();
        if (savedInstanceState == null) {
            mPresenter.setArguments(getArguments());
        } else {
            mPresenter.onRestoreInstanceState(savedInstanceState);
        }
    }

    private void providePresenter() {
        PresenterFactory<IPhoneNumberPresenter, PhoneNumberView> mFactory = new PhoneNumberPresenterFactory();
        mPresenter = mFactory.providePresenter(getArguments(), this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mPresenter.onSaveInstanceState(outState);
    }

    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showProgress() {
        mProgress.show();
    }

    @Override
    public void hideProgress() {
        mProgress.cancel();
    }

    @Override
    public String getNumber() {
        return etPhoneNumber.getText().toString();
    }

    @Override
    public void showInputError(String error) {
        if (etPhoneNumber != null) {
            etPhoneNumber.setError(error);
        }
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateNext(SignUpRequest request) {
        getArguments().putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, request);
        ((AuthListener) getActivity()).onNumberPassed(getArguments());
    }

    @Override
    public void dismissKeyboard() {
        hideKeyboard();
    }

    @Override
    @SuppressWarnings("unchecked")
    public AuthRepository provideScopedRepository() {
        return ((ScopedInstance<AuthRepository>) getActivity()).provideScopedInstance();
    }

    @Override
    public void setPhoneChecked(boolean checked) {
        if (ivCheckPhone != null){
            ivCheckPhone.setVisibility(checked ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void showPhoneProgress(boolean show) {
        pbPhone.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setNextEnabled(boolean enabled) {
        btnNext_FPN.setEnabled(enabled);
    }

    @OnTextChanged(R.id.etPhoneNumber_FPN)
    void onTextChanged(CharSequence text) {
        mPresenter.onTextChanged(text.toString());
    }

    @OnClick(R.id.btnNext_FPN)
    void onNextClick() {
        mPresenter.onNextClick();
    }
}
