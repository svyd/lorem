package com.svyd.wodes.presentation.base.tool;

import android.util.Patterns;

/**
 * Created by Svyd on 18.07.2016.
 */
public class PatternValidator {
    public static boolean isEmailValid(String _email) {
        return _email != null && Patterns.EMAIL_ADDRESS.matcher(_email).matches();
    }

    public static boolean isPhoneNumberValid(String _email) {
        return _email != null && Patterns.PHONE.matcher(_email).matches();
    }

    public static boolean isPasswordValid(String password) {
        return password != null && password.length() > 7;
    }
}
