package com.svyd.wodes.presentation.project.authorization.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 18.07.2016.
 */
public class OauthRequest {

    private String network;

    @SerializedName("access_token")
    private String token;

    @SerializedName("deviceId")
    private String id;

    @SerializedName("deviceToken")
    private String deviceToken;

    @SerializedName("provider")
    private String provider;

    @SerializedName("oauth_token")
    private String oathToken;

    @SerializedName("oauth_token_secret")
    private String oauthTokenSecret;

    @SerializedName("user_id")
    private String userId;

    public void setOathToken(String oathToken) {
        this.oathToken = oathToken;
    }

    public void setOauthTokenSecret(String oauthTokenSecret) {
        this.oauthTokenSecret = oauthTokenSecret;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNetwork() {
        return network;
    }

    public static class Builder {
        private String network;
        private String token;
        private String id;
        private String oathToken;
        private String oauthTokenSecret;
        private String userId;

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Builder setNetwork(String network) {
            this.network = network;
            return this;
        }

        public Builder setToken(String token) {
            this.token = token;
            return this;
        }

        public Builder setOathToken(String oathToken) {
            this.oathToken = oathToken;
            return this;
        }

        public Builder setOauthTokenSecret(String oauthTokenSecret) {
            this.oauthTokenSecret = oauthTokenSecret;
            return this;
        }

        public Builder setUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public OauthRequest build() {
            OauthRequest request = new OauthRequest();
            request.setNetwork(network);
            request.setToken(token);
            request.setId(id);
            request.setOathToken(oathToken);
            request.setOauthTokenSecret(oauthTokenSecret);
            request.setUserId(userId);

            return request;
        }
    }
}
