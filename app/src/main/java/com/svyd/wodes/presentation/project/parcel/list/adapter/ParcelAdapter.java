package com.svyd.wodes.presentation.project.parcel.list.adapter;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.OnItemClickListener;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 05.07.2016.
 */
public class ParcelAdapter extends RecyclerView.Adapter<ParcelAdapter.ParcelHolder> {

    private Context mContext;
    private List<ParcelItem> mItems;
    private OnItemClickListener<ParcelItem> mListener;

    public ParcelAdapter(Context context) {
        mContext = context;
        mItems = new ArrayList<>();
    }

    @Override
    public ParcelHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(mContext)
                .inflate(R.layout.list_item_parcel, parent, false);

        return new ParcelHolder(view);
    }

    @Override
    public void onBindViewHolder(ParcelHolder holder, int position) {
        ParcelItem item = mItems.get(position);

        holder.setTitle(item.getTitle());
        holder.setDeliveryTime(item.getDeliveryTime());
        holder.setFrom(item.getFrom());
        holder.setTo(item.getTo());
        holder.setSize(item.getSize());
        holder.setTime(item.getTime());
        holder.setViews(item.getViews());
    }

    public void setOnItemClickListener(OnItemClickListener<ParcelItem> listener) {
        mListener = listener;
    }

    public void setItems(List<ParcelItem> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void addItem(ParcelItem item) {
        mItems.add(0, item);
        notifyItemInserted(0);
        notifyItemRangeChanged(0, mItems.size());
    }

    public void editItem(int position, ParcelItem item) {
        mItems.remove(position);
        mItems.add(position, item);
        notifyDataSetChanged();
    }

    public class ParcelHolder extends RecyclerView.ViewHolder implements View.OnTouchListener {

        @BindView(R.id.tvTitle_LIP)
        TextView tvTitle;

        @BindView(R.id.tvFrom_LIP)
        TextView tvFrom;

        @BindView(R.id.tvTo_LIP)
        TextView tvTo;

        @BindView(R.id.tvDeliveryTime_LIP)
        TextView tvDeliveryTime;

        @BindView(R.id.tvSize_LIP)
        TextView tvSize;

        @BindView(R.id.tvTime_LIP)
        TextView tvTime;

        @BindView(R.id.tvViews_LIP)
        TextView tvViews;

        View container;

        public ParcelHolder(View itemView) {
            super(itemView);

            container = itemView;
            container.setOnTouchListener(this);
            ButterKnife.bind(this, itemView);
        }

        public void setTitle(String title) {
            tvTitle.setText(title);
        }

        public void setFrom(String from) {
            tvFrom.setText(from);
        }

        public void setTo(String to) {
            tvTo.setText(to);
        }

        public void setDeliveryTime(String deliveryTime) {
            tvDeliveryTime.setText(deliveryTime);
        }

        public void setSize(String size) {
            tvSize.setText(size);
        }

        public void setTime(String time) {
            tvTime.setText(time);
        }

        public void setViews(String views) {
            tvViews.setText(views);
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP
                    && mListener != null) {
                Point point = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
                view.setTag(point);
                mListener.onItemClick(getLayoutPosition(), mItems.get(getLayoutPosition()), view);
            }
            return false;
        }
    }
}
