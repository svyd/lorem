package com.svyd.wodes.presentation.base.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.svyd.wodes.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 04.07.2016.
 */
@SuppressWarnings("deprecation")
public class RatingView extends LinearLayout {

    private int mMaxRating = 5;
    private int mRating;
    private List<ImageView> mImages;
    private LinearLayout.LayoutParams mParams;


    public RatingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RatingView(Context context) {
        this(context, null, 0);
    }

    public RatingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mImages = new ArrayList<>();
        setOrientation(HORIZONTAL);

        mParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mParams.setMargins(
                0,
                0,
                (int) getResources().getDimension(R.dimen.margin_half),
                0);
        initViews();
    }

    private void initViews() {
        mImages.clear();
        removeAllViews();
        for (int i = 0; i < mMaxRating; i++) {
            ImageView view = new ImageView(getContext());
            view.setLayoutParams(mParams);
            view.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_grey));
            mImages.add(view);
            addView(view);
        }
    }

    private void clearSelection() {
        for (ImageView view: mImages) {
            view.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_grey));
        }
    }

    public void setMaxRating(int max) {
        mMaxRating = max;

        init();
    }

    public void setRating(int rating) {
        if (mRating == rating) {
            return;
        }

        mRating = rating;

        clearSelection();

        for(int i = 0; i < mRating && i < mImages.size(); i++) {
            mImages.get(i).setImageDrawable(getResources().getDrawable(R.drawable.ic_star_pink));
        }
    }
}
