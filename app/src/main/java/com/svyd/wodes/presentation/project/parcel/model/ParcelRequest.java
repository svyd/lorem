package com.svyd.wodes.presentation.project.parcel.model;

import com.svyd.wodes.presentation.project.delivery.model.SimpleLocationModel;

/**
 * Created by Svyd on 06.07.2016.
 */
public class ParcelRequest {

    private SimpleLocationModel from;
    private SimpleLocationModel to;

    private String referenceFrom;
    private String referenceTo;

    private String title;
    private String description;
    private String startDate;
    private String size;
    private String price;
    private String photo;
    private String id;

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setReferenceFrom(String referenceFrom) {
        this.referenceFrom = referenceFrom;
    }

    public void setReferenceTo(String referenceTo) {
        this.referenceTo = referenceTo;
    }

    public String getReferenceFrom() {
        return referenceFrom;
    }

    public String getReferenceTo() {
        return referenceTo;
    }

    public SimpleLocationModel getFrom() {
        return from;
    }

    public SimpleLocationModel getTo() {
        return to;
    }

    public String getDescription() {
        return description;
    }

    public String getSize() {
        return size;
    }

    public String getTitle() {
        return title;
    }

    public String getPhoto() {
        return photo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFrom(SimpleLocationModel from) {
        this.from = from;
    }

    public void setTo(SimpleLocationModel to) {
        this.to = to;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
