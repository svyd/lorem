package com.svyd.wodes.presentation.project.delivery.base;

/**
 * Created by Svyd on 07.09.2016.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.OnItemClickListener;
import com.svyd.wodes.presentation.base.Utils;
import com.svyd.wodes.presentation.base.tool.CircularTransform;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseDeliveryAdapter extends RecyclerView.Adapter<BaseDeliveryAdapter.DeliveryHolder> {

    private Activity mContext;
    private List<DeliveryItem> mItems;
    private OnItemClickListener<DeliveryItem> mListener;

    public BaseDeliveryAdapter(Activity context) {
        mContext = context;
        mItems = new ArrayList<>();
    }

    @Override
    public DeliveryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(mContext)
                .inflate(getLayoutResource(), parent, false);
        return new DeliveryHolder(view, mContext);
    }

    protected abstract int getLayoutResource();

    @Override
    public void onBindViewHolder(DeliveryHolder holder, int position) {
        DeliveryItem item = mItems.get(position);

        holder.setStart(item.getStart());
        holder.setFrom(item.getFrom());
        holder.setTo(item.getTo());
        holder.setAvailable(item.getAvailable());
        holder.setCarrier(item.getCarrier());
        holder.setTime(item.getTime());
        holder.setViews(item.getViews());
        holder.setAvatar(item.getAvatar());
    }

    public void setOnItemClickListener(OnItemClickListener<DeliveryItem> listener) {
        mListener = listener;
    }

    public void setItems(List<DeliveryItem> items) {
        mItems = items;
        notifyItemRangeInserted(0, mItems.size());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void addItem(DeliveryItem item) {
        mItems.add(0, item);
        notifyItemInserted(0);
        notifyItemRangeChanged(0, mItems.size());
    }

    public void swapItem(DeliveryItem item, int position) {
        mItems.remove(position);
        mItems.add(position, item);
        notifyDataSetChanged();
    }

    public class DeliveryHolder extends RecyclerView.ViewHolder implements View.OnTouchListener {

        @Nullable
        @BindView(R.id.ivAvatar_LID)
        ImageView ivAvatar;

        @BindView(R.id.tvFrom_LID)
        TextView tvFrom;

        @BindView(R.id.tvTo_LID)
        TextView tvTo;

        @BindView(R.id.tvStart_LID)
        TextView tvStart;

        @BindView(R.id.tvAvailable_LID)
        TextView tvAvailable;

        @BindView(R.id.tvCarrier_LID)
        TextView tvCarrier;

        @BindView(R.id.tvTime_LID)
        TextView tvTime;

        @BindView(R.id.tvViews_LID)
        TextView tvViews;

        View container;
        private BitmapTransformation mTransformation;

        private Context mContext;

        public DeliveryHolder(View itemView, Context context) {
            super(itemView);

            mContext = context;
            container = itemView;
            container.setOnTouchListener(this);
            mTransformation = new CircularTransform(mContext);
            ButterKnife.bind(this, itemView);
        }

        public void setStart(String start) {
            tvStart.setText(start);
        }

        public void setFrom(String from) {
            tvFrom.setText(from);
        }

        public void setTo(String to) {
            tvTo.setText(to);
        }

        public void setAvailable(String deliveryTime) {
            tvAvailable.setText(deliveryTime);
        }

        public void setCarrier(String size) {
            tvCarrier.setText(size);
        }

        public void setTime(String time) {
            tvTime.setText(time);
        }

        public void setViews(String views) {
            tvViews.setText(views);
        }

        public void setAvatar(String url) {
            if (ivAvatar != null) {
                Glide.with(mContext)
                        .load(url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter()
                        .centerCrop()
                        .placeholder(R.drawable.stub_avatar)
                        .error(R.drawable.stub_avatar)
                        .transform(mTransformation)
                        .animate(android.R.anim.fade_in)
                        .into(ivAvatar);
            }
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP
                    && mListener != null) {
                Point point = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
                view.setTag(point);
                mListener.onItemClick(getLayoutPosition(), mItems.get(getLayoutPosition()), view);
            }
            return false;
        }
    }
}
