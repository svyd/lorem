package com.svyd.wodes.presentation.project.parcel.list.my.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.home.activity.HomeActivity;
import com.svyd.wodes.presentation.project.parcel.create.view.CreateParcelActivity;
import com.svyd.wodes.presentation.project.parcel.details.view.ParcelDetailsActivity;
import com.svyd.wodes.presentation.project.parcel.list.adapter.ParcelAdapter;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelView;
import com.svyd.wodes.presentation.project.parcel.list.presenter.ParcelPresenterFactory;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 14.09.2016.
 */
public class MyParcelActivity extends BaseActivity implements IParcelView {

    @BindView(R.id.rvItems_AMP)
    RecyclerView rvParcels;

    @BindView(R.id.pbParcel_AMP)
    ProgressBar pbParcel;

    @BindView(R.id.tvNoItems_AMP)
    TextView tvNoItems;

    private IParcelPresenter mPresenter;
    private ParcelAdapter mAdapter;
    private LazyLoadListener mScrollListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_parcel);
        enableBackButton(true);

        ButterKnife.bind(this);
        providePresenter();
        initPresenter();
        initList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mPresenter.onCreateOptionsMenu(getMenuInflater(), menu);
        return true;
    }

    private void initList() {
        mScrollListener = new LazyLoadListener(mPresenter);
        mAdapter = new ParcelAdapter(this);
        mAdapter.setOnItemClickListener(mPresenter);
        rvParcels.setLayoutManager(new LinearLayoutManager(this));
        rvParcels.setAdapter(mAdapter);
        rvParcels.addOnScrollListener(mScrollListener);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.handleResult(requestCode, resultCode, data);
    }

    private void providePresenter() {
        mPresenter = new ParcelPresenterFactory().providePresenter(getIntent().getExtras(), this);
    }

    private void initPresenter() {
        mPresenter.initialize();
    }

    @Override
    public int getContainerId() {
        return 0;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AMP;
    }

    @Override
    public void showProgress() {
        pbParcel.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbParcel.setVisibility(View.GONE);
    }

    @Override
    public void setToolbarTitle(String title) {
        setTitle(title);
    }

    @Override
    public void setItems(List<ParcelItem> items) {
        mAdapter.setItems(items);
    }

    @Override
    public void navigateToDetails(int index, View view, ParcelItem item) {
        Intent intent = new Intent(MyParcelActivity.this, ParcelDetailsActivity.class);

        intent.putExtra(Constants.Flow.FLOW_KEY, Constants.Flow.DETAILS_MY);
        intent.putExtra(Constants.Extra.EXTRA_PARCEL, item);
        intent.putExtra(Constants.Extra.EXTRA_POSITION, index);

        startActivityForResult(intent, CreateParcelActivity.REQUEST_EDIT_PARCEL);
    }

    @Override
    public void navigateToCreate() {
        Intent intent = new Intent(MyParcelActivity.this, CreateParcelActivity.class);

        if (checkLollipop()) {

            TypedValue tv = new TypedValue();
            int toolbarHeight = 0;
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                toolbarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
            }
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra("x", width - 36);
            intent.putExtra("y", toolbarHeight);
        }
        intent.putExtra(Constants.Flow.FLOW_KEY, Constants.Flow.CREATE_ITEM);

        startActivityForResult(intent, CreateParcelActivity.REQUEST_CREATE_PARCEL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miAdd_AI:
                mPresenter.onAdd();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void notifyChanges() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void enableLoadMore() {
        mScrollListener.enable();
    }

    @Override
    public void showStubText() {
        tvNoItems.setText(R.string.text_no_parcels);
        tvNoItems.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideStubText() {
        tvNoItems.setVisibility(View.GONE);
    }

    @Override
    public void addItem(ParcelItem item) {
        rvParcels.scrollToPosition(0);
        mAdapter.addItem(item);
    }

    @Override
    public void editItem(int position, ParcelItem item) {
        mAdapter.editItem(position, item);
    }

    @Override
    public void showFab() {

    }

    @Override
    public void hideFab() {

    }

    @Override
    public void showListProgress() {

    }

    @Override
    public void hideListProgress() {

    }

    @Override
    public void showAcceptDialog() {
        showDialog();
    }

    @Override
    public void navigateHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.Extra.EXTRA_OPTIONS, Constants.Extra.EXTRA_OPTION_UPDATE);
        startActivity(intent);
    }

    void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.alert_title_contact_carrier)
                .setMessage(R.string.alert_message_contact_carrier)
                .setCancelable(false)
                .setPositiveButton(R.string.confirm, (dialog1, which) -> {
                    mPresenter.onAccept();
                    dialog1.dismiss();
                })
                .setNegativeButton(R.string.cancel,
                        (dialog, id) -> {
                            dialog.cancel();
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
