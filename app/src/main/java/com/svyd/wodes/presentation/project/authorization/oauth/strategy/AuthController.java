package com.svyd.wodes.presentation.project.authorization.oauth.strategy;

import android.content.Intent;

import com.svyd.wodes.presentation.base.ui.BaseFragment;

/**
 * Created by Svyd on 18.07.2016.
 */
public class AuthController implements AuthorizationStrategy {

    private AuthorizationStrategy mDelegateStrategy;

    public void setDelegateStrategy(AuthorizationStrategy _delegate) {
        mDelegateStrategy = _delegate;
    }

    @Override
    public void logIn(BaseFragment _fragment, OauthCallback _callback) {
        mDelegateStrategy.logIn(_fragment, _callback);
    }

    @Override
    public void onActivityResult(int _requestCode, int _resultCode, Intent _intent) {
        mDelegateStrategy.onActivityResult(_requestCode, _resultCode, _intent);
    }
}
