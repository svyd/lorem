package com.svyd.wodes.presentation.project.parcel.list.presenter;

import android.os.Bundle;

import com.google.gson.Gson;
import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.ListDataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.dialog.DialogMapper;
import com.svyd.wodes.data.dialog.DialogRepository;
import com.svyd.wodes.data.dialog.DialogService;
import com.svyd.wodes.data.dialog.IDialogRepository;
import com.svyd.wodes.data.dialog.model.DialogEntity;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.data.parcel.mapper.ParcelMapper;
import com.svyd.wodes.data.parcel.mapper.ParcelsMapper;
import com.svyd.wodes.data.parcel.repository.IParcelRepository;
import com.svyd.wodes.data.parcel.repository.ParcelRepository;
import com.svyd.wodes.data.parcel.repository.ParcelService;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.dialog.InviteInteractor;
import com.svyd.wodes.domain.parcel.FindParcelInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.dialog.list.model.DialogItem;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelView;
import com.svyd.wodes.presentation.project.parcel.list.presenter.implementation.AllParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.list.presenter.implementation.ChooseParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.list.presenter.implementation.MyParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.list.all.view.ParcelFragment;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.domain.parcel.ParcelInteractor;
import com.svyd.wodes.presentation.project.parcel.model.ParcelEntity;

import java.util.List;

/**
 * Created by Svyd on 05.07.2016.
 */
public class ParcelPresenterFactory implements PresenterFactory<IParcelPresenter, IParcelView> {

    private IParcelRepository mRepository;

    @Override
    public IParcelPresenter providePresenter(Bundle args, IParcelView view) {
        if (args != null && args.containsKey(Constants.Flow.FLOW_KEY)) {
            int flow = args.getInt(Constants.Flow.FLOW_KEY);
            switch (flow) {
                case Constants.Flow.CHOOSE_ITEM:
                    return provideChoosePresenter(view, args);
                default:
                    throw new IllegalArgumentException("Unsupported flow: " + flow);
            }
        } else {
            if (view instanceof ParcelFragment) {
                return provideAllParcelPresenter(view);
            } else {
                return provideMyParcelPresenter(view);
            }
        }
    }

    private IParcelPresenter provideChoosePresenter(IParcelView view, Bundle args) {
        return new ChooseParcelPresenter(view, args, provideParcelInteractor(), provideSearchInteractor(), provideInviteInteractor());
    }

    private PostInteractor<InviteRequest> provideInviteInteractor() {
        return new InviteInteractor(provideDialogRepository());
    }

    private IDialogRepository provideDialogRepository() {
        return new DialogRepository(provideDialogService(), provideDialogMapper());
    }


    private TypeMapper<ListDataWrapper<DialogEntity>, List<DialogItem>> provideDialogMapper() {
        return new DialogMapper();
    }


    private DialogService provideDialogService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), DialogService.class);
    }

    private IParcelPresenter provideMyParcelPresenter(IParcelView view) {
        return new MyParcelPresenter(view, provideParcelInteractor(), provideSearchInteractor());
    }

    private IParcelPresenter provideAllParcelPresenter(IParcelView view) {
        return new AllParcelPresenter(view, provideParcelInteractor(), provideSearchInteractor());
    }

    private PostInteractor<SearchRequest> provideSearchInteractor() {
        return new FindParcelInteractor(provideParcelRepository());
    }

    private ParcelInteractor provideParcelInteractor() {
        return new ParcelInteractor(provideParcelRepository());
    }

    private IParcelRepository provideParcelRepository() {
        if (mRepository == null) {
            mRepository = new ParcelRepository(new Gson(),
                    provideParcelService(),
                    provideParcelsMapper(),
                    provideParcelMapper());
        }
        return mRepository;
    }

    private TypeMapper<ParcelEntity, ParcelItem> provideParcelMapper() {
        return new ParcelMapper();
    }

    TypeMapper<DataWrapper<List<ParcelEntity>>, List<ParcelItem>> provideParcelsMapper() {
        return new ParcelsMapper(new ParcelMapper());
    }

    private ParcelService provideParcelService() {
        return new ServiceProvider().provideWodesService(WodesApplication.getContext(), ParcelService.class);
    }

}
