package com.svyd.wodes.presentation.project.delivery.my.presenter;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.svyd.wodes.R;
import com.svyd.wodes.data.dialog.model.DialogEntity;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.delivery.my.contract.IMyDeliveryView;

import rx.Observer;

/**
 * Created by Svyd on 23.09.2016.
 */
public class ChooseDeliveryPresenter extends MyDeliveryPresenter {

    private static final String TAG = ChooseDeliveryPresenter.class.getSimpleName();
    private PostInteractor<InviteRequest> mInviteInteractor;
    private InviteRequest mRequest;

    public ChooseDeliveryPresenter(IMyDeliveryView view,
                                   Bundle args,
                                   PostInteractor<ListRequest> interactor,
                                   PostInteractor<InviteRequest> inviteInteractor) {
        super(view, interactor);
        mInviteInteractor = inviteInteractor;
        getRequest(args);
    }

    private void getRequest(Bundle args) {
        mRequest = (InviteRequest) args.getSerializable(Constants.Extra.EXTRA_INVITE_REQUEST);
    }

    @Override
    public void onAccept() {
        mInviteInteractor.execute(mRequest, new InviteObserver());
    }

    @Override
    public void onCreateOptionsMenu(MenuInflater menuInflater, Menu menu) {

    }

    @Override
    public void onItemClick(int index, DeliveryItem item, View view) {
        mRequest.setDelivery(item.getEntity().getId());
        getView().showAcceptDialog();
    }

    @Override
    public void initialize() {
        super.initialize();
        getView().setToolbarTitle(WodesApplication.getStringResource(R.string.title_choose_delivery));
    }

    private class InviteObserver implements Observer<DialogEntity> {

        @Override
        public void onCompleted() {
            Log.d(TAG, "onCompleted() called with: " + "");
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "onError() called with: " + "e = [" + e + "]");
        }

        @Override
        public void onNext(DialogEntity dialog) {
            getView().navigateHome();
            Log.d(TAG, "onNext() called with: " + "dialog = [" + dialog + "]");
        }
    }

}
