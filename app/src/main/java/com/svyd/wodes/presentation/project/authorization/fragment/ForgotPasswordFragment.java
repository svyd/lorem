package com.svyd.wodes.presentation.project.authorization.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.svyd.wodes.R;
import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.ScopedInstance;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.project.authorization.contract.IForgotPasswordPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.ForgotPasswordView;
import com.svyd.wodes.presentation.project.authorization.presenter.factory.ForgotPasswordPresenterFactory;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by Svyd on 14.08.2016.
 */
public class ForgotPasswordFragment extends BaseFragment implements ForgotPasswordView {

    @BindView(R.id.rlRoot_FFP)
    RelativeLayout rlRoot;

    @BindView(R.id.etEmail_FFP)
    EditText etEmail;

    @BindView(R.id.pbEmail_FFP)
    ProgressBar pbEmail;

    @BindView(R.id.ivCheckEmail_FFP)
    ImageView ivCheckEmail;

    @OnTextChanged(R.id.etEmail_FFP)
    void onEmailChanged(CharSequence email) {
        mPresenter.onEmailChanged(email.toString());
    }

    @OnClick(R.id.btnRequest_FFP)
    void onRequestClick() {
        mPresenter.onRequestClick();
    }

    private ProgressDialog mProgress;

    private IForgotPasswordPresenter mPresenter;

    public static ForgotPasswordFragment newInstance() {

        Bundle args = new Bundle();

        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_forgot_password);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUi();

        initPresenter();
        mPresenter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPresenter.onSaveInstanceState(outState);
    }

    private void initPresenter() {
        mPresenter = new ForgotPasswordPresenterFactory()
                .providePresenter(getArguments(), this);
        mPresenter.initialize();
    }

    private void initUi() {
        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle(WodesApplication.getStringResource(R.string.txt_wait));
        mProgress.setMessage(WodesApplication.getStringResource(R.string.txt_forgot_password_progress));
    }

    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    @SuppressWarnings("unchecked")
    public AuthRepository getScopedRepository() {
        return ((ScopedInstance<AuthRepository>) getActivity()).provideScopedInstance();
    }

    @Override
    public void navigateBack() {
        getFragmentNavigator().popBackStack();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setEmailError(String error) {
        etEmail.setError(error);
    }

    @Override
    public void showEmailProgress(boolean show) {
        pbEmail.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setEmailChecked(boolean checked) {
        ivCheckEmail.setVisibility(checked ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showProgress() {
        mProgress.show();
    }

    @Override
    public void hideProgress() {
        mProgress.cancel();
    }
}
