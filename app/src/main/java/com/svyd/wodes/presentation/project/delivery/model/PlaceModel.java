package com.svyd.wodes.presentation.project.delivery.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 09.09.2016.
 */
public class PlaceModel {
    @Expose
    @SerializedName("formatted_address")
    public String address;

    @Expose
    public Geometry geometry;

    class Geometry {
        @Expose
        public  Location location;

        class Location {
            @Expose
            String lat;
            @Expose
            String lng;
        }
    }
}
