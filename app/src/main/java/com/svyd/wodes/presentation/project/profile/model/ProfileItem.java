package com.svyd.wodes.presentation.project.profile.model;

import android.support.annotation.IdRes;

import java.io.Serializable;

/**
 * Created by Svyd on 04.07.2016.
 */
public class ProfileItem implements Serializable {
    private String title;
    private String subtitle;
    private int icon;
    private boolean isClickable;
    private boolean hasDivider;
    private boolean hasOlyTitle;

    private String meta;

    public void setHasOlyTitle(boolean hasOlyTitle) {
        this.hasOlyTitle = hasOlyTitle;
    }

    public boolean hasOnlyTitle() {
        return hasOlyTitle;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public void setHasDivider(boolean hasDivider) {
        this.hasDivider = hasDivider;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }

    public void setIcon(@IdRes int icon) {
        this.icon = icon;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getTitle() {
        return title;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public boolean hasDivider() {
        return hasDivider;
    }

    public static class Builder {
        private String title;
        private String subtitle;
        private int icon;
        private boolean isClickable;
        private boolean hasDivider;
        private boolean hasOnlyTitle;

        private String meta;

        public Builder setHasOnlyTitle(boolean hasOnlyTitle) {
            this.hasOnlyTitle = hasOnlyTitle;
            return this;
        }

        public String getMeta() {
            return meta;
        }

        public Builder setMeta(String meta) {
            this.meta = meta;
            return this;
        }

        public Builder setHasDivider(boolean hasDivider) {
            this.hasDivider = hasDivider;
            return this;
        }

        public Builder setClickable(boolean clickable) {
            isClickable = clickable;
            return this;
        }

        public Builder setIcon(int icon) {
            this.icon = icon;
            return this;
        }

        public Builder setSubtitle(String subtitle) {
            this.subtitle = subtitle;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public ProfileItem build() {
            ProfileItem item = new ProfileItem();
            item.setClickable(isClickable);
            item.setIcon(icon);
            item.setSubtitle(subtitle);
            item.setTitle(title);
            item.setHasDivider(hasDivider);
            item.setMeta(meta);
            item.setHasOlyTitle(hasOnlyTitle);

            return item;
        }
    }
}
