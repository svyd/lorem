package com.svyd.wodes.presentation.project.delivery.details.presenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.svyd.wodes.R;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.tool.DateTimeUtility;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.create.CreateDeliveryActivity;
import com.svyd.wodes.presentation.project.delivery.details.contract.IDeliveryDetailsPresenter;
import com.svyd.wodes.presentation.project.delivery.details.contract.IDeliveryDetailsView;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 18.09.2016.
 */
@SuppressWarnings("Convert2streamapi")
public abstract class AbstractDeliveryDetailsPresenter implements IDeliveryDetailsPresenter {

    private IDeliveryDetailsView mView;
    private DeliveryItem mItem;
    private List<LatLng> mCoordinates;

    public AbstractDeliveryDetailsPresenter(IDeliveryDetailsView view, Bundle args) {
        mView = view;
        initArgs(args);
    }

    private void initArgs(Bundle args) {
        if (args.containsKey(Constants.Extra.EXTRA_DELIVERY)) {
            mItem = (DeliveryItem) args.getSerializable(Constants.Extra.EXTRA_DELIVERY);
        } else {
            throw new IllegalArgumentException("Bundle must contain " + DeliveryItem.class);
        }
    }

    @Override
    public void onCreateOptionsMenu(MenuInflater menuInflater, Menu menu) {

    }

    @Override
    public void onAccept() {

    }

    protected IDeliveryDetailsView getView() {
        return mView;
    }

    protected DeliveryItem getItem() {
        return mItem;
    }

    @Override
    public void initialize() {
        initCoordinates();
        initData();
    }

    @Override
    public void onOwnerClick() {
        throw new UnsupportedOperationException();
    }

    private void initData() {
        mView.setDepartureDate(DateTimeUtility.formatDate(mItem.getEntity().getStartDate(), true));
        mView.setStart(DateTimeUtility.formatDate(mItem.getEntity().getStartDate(), true));
        mView.setDepartureCity(mItem.getFrom());
        mView.setArrivalDate(DateTimeUtility.formatDate(mItem.getEntity().getStartDate(), true));
        mView.setArrivalCity(mItem.getTo());
        mView.setAvailable(mItem.getEntity().getSize().getName());
        mView.setDescription(mItem.getEntity().getDescription());
        mView.setOwnerName(mItem.getEntity().getOwner().getName());
        mView.setAvatar(mItem.getEntity().getOwner().getAvatar());
        mView.setUserSince(DateTimeUtility.formatDate(mItem.getEntity().getOwner().getCreatedAt(), false));
        mView.setDeliveredParcels(WodesApplication.getStringResource(R.string.delivered_parcels, mItem.getEntity().getOwner().getMyParcels()));
        int visits = Integer.parseInt(mItem.getViews());
        mView.setVisits(WodesApplication.getStringResource(R.string.visits, visits));
    }

    private void initCoordinates() {
        mCoordinates = new ArrayList<>();
        for (float[] points: mItem.getEntity().getPoints()) {
            mCoordinates.add(new LatLng(points[0], points[1]));
        }
    }

    private void drawRoute() {
        mView.drawRoute(mCoordinates);
    }

    private void setBounds() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng point : mCoordinates) {
            builder.include(point);
        }
        mView.setBounds(builder.build());
    }

    private void addMarker(LatLng point) {
        MarkerOptions marker = new MarkerOptions()
                .position(point)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin));
        mView.addMarker(marker);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onMapLoaded() {
        mView.showMap();
        setBounds();
        addMarker(mCoordinates.get(0));
    }

    @Override
    public void onRouteDrawn() {
        addMarker(mCoordinates.get(mCoordinates.size() - 1));
    }

    @Override
    public void onCameraIdle() {
        drawRoute();
    }

    @Override
    public void onEdit() {
        mView.navigateToEdit(mItem);
    }

    @Override
    public void handleResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CreateDeliveryActivity.REQUEST_EDIT_DELIVERY &&
                resultCode == Activity.RESULT_OK) {
            mItem = (DeliveryItem) data.getSerializableExtra(Constants.Extra.EXTRA_DELIVERY);
            initData();
            mView.setResult(mItem);
        }
    }
}
