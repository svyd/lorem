package com.svyd.wodes.presentation.project.profile.edit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.svyd.wodes.R;
import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.data.authorization.AuthRepositoryImpl;
import com.svyd.wodes.data.authorization.AuthService;
import com.svyd.wodes.data.profile.cache.PreferenceProfileCache;
import com.svyd.wodes.presentation.base.ScopedInstance;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.fragment.CodeVerificationFragment;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

/**
 * Created by Svyd on 27.08.2016.
 */
public class EditProfileActivity extends BaseActivity implements IEditProfileController, ScopedInstance<AuthRepository> {

    public static final int REQUEST_CODE_EDIT_PROFILE = 321;

    private String mOldPhone;
    private String mNewPhone;
    private ProfileInfo mOldProfile;
    private ProfileInfo mNewProfile;
    private AuthRepository mAuthRepository;

    public static void startEditProfile(Activity activity, ProfileInfo info) {
        Intent intent = new Intent(activity, EditProfileActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_PROFILE, info);
        activity.startActivityForResult(intent, REQUEST_CODE_EDIT_PROFILE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setTitle(getString(R.string.title_edit_profile));
        enableBackButton(true);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        initProfile();
        initFragment();
    }

    private void initFragment() {
        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_PROFILE, mOldProfile);
        getFragmentNavigator().addFragmentWithoutBackStack(EditProfileFragment.newInstance(args));
    }

    void initProfile() {
        mOldProfile = (ProfileInfo) getIntent().getSerializableExtra(Constants.Extra.EXTRA_PROFILE);
        mOldPhone = mOldProfile.getPhone();
    }



    @Override
    public int getContainerId() {
        return R.id.flContainer_AEP;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AEP;
    }

    @Override
    public void done() {
        finish();
    }

    private void setResultOk() {
        Intent intent = new Intent();
        intent.putExtra(Constants.Extra.EXTRA_PROFILE, mNewProfile);
        setResult(RESULT_OK, intent);
    }

    private void checkPhone() {
        if (mNewPhone != null) {
            startCodeValidation();
        } else {
            done();
        }
    }

    private void startCodeValidation() {
        Bundle args = new Bundle();
        args.putInt(Constants.Flow.FLOW_KEY, Constants.Flow.EDIT_VERIFY_NUMBER);
        getFragmentNavigator().replaceFragmentWithBackStack(CodeVerificationFragment.newInstance(args));
    }

    //Until the phone number is not validated set new profile's number to the old one

    @Override
    public void onProfileUpdated(ProfileInfo info) {
        mNewProfile = info;
        mNewPhone = mNewProfile.getPhone();
        mNewProfile.setPhone(mOldPhone);
        setResultOk();
        checkPhone();
    }

    @Override
    public void setValidatedPhone(boolean validated) {
        mNewProfile.setPhone(validated ? mNewPhone : mOldPhone);
        setResultOk();
    }

    @Override
    public AuthRepository provideScopedInstance() {
        synchronized (EditProfileActivity.this) {

            if (mAuthRepository == null) {
                mAuthRepository = new AuthRepositoryImpl(new ServiceProvider()
                        .provideWodesService(getApplicationContext(), AuthService.class), new PreferenceProfileCache());
            }

            return mAuthRepository;
        }
    }
}
