package com.svyd.wodes.presentation.base.tool;

import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.base.ui.BaseFragment;

/**
 * Created by Svyd on 02.07.2016.
 */
public class FragmentNavigator {

    protected FragmentManager mFragmentManager;
    private BaseActivity mActivity;

    protected
    @IdRes
    int mContainerId = -1;

    public FragmentNavigator(BaseActivity _activity, FragmentManager _manager, @IdRes int _containerId) {
        mFragmentManager = _manager;
        mContainerId = _containerId;
        mActivity = _activity;
    }

    protected FragmentTransaction getTransaction() {
        return mFragmentManager.beginTransaction();
    }

    public void clearBackStack() {
        int entryCount = mFragmentManager.getBackStackEntryCount();
        if (entryCount <= 0)
            return;

        FragmentManager.BackStackEntry entry = mFragmentManager.getBackStackEntryAt(0);
        int id = entry.getId();
        mActivity.runOnUiThread(() ->
                mFragmentManager.popBackStackImmediate(id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        );

    }

    public void replaceFragmentWithoutBackStack(BaseFragment _fragment) {
        clearBackStack();
        getTransaction()
                .replace(mContainerId, _fragment)
                .commit();
    }

    public void addFragmentWithoutBackStack(BaseFragment _fragment) {
        clearBackStack();
        getTransaction()
                .add(mContainerId, _fragment)
                .commit();
    }

    public void addFragmentWithContainer(BaseFragment _fragment, @IdRes int _containerId) {
        getTransaction()
                .replace(_containerId, _fragment)
                .commit();
    }

    public void replaceFragment(BaseFragment _fragment) {
        getTransaction()
                .replace(mContainerId, _fragment)
                .commit();
    }

    public void replaceFragmentWithBackStack(BaseFragment _fragment) {
        getTransaction()
                .setCustomAnimations(R.anim.enter_slide_left, R.anim.exit_slide_left, R.anim.pop_enter_slide_left, R.anim.pop_exit_slide_left)
                .replace(mContainerId, _fragment)
                .addToBackStack(_fragment.getClass().getSimpleName())
                .commit();
    }

    public void replaceFragmentWithoutBackStackFade(BaseFragment _fragment) {
        getTransaction()
                .setCustomAnimations(R.anim.enter_add, R.anim.fade_out_scale_down, R.anim.pop_exit_slide_up, android.R.anim.fade_in)
                .replace(mContainerId, _fragment)
                .commit();
    }

    public void replaceFragmentWithoutBackStackSlideUp(BaseFragment _fragment) {
        getTransaction()
                .setCustomAnimations(R.anim.enter_slide_up, R.anim.exit_slide_up, R.anim.pop_enter_slide_up, R.anim.pop_exit_slide_up)
                .replace(mContainerId, _fragment)
                .addToBackStack(_fragment.getClass().getSimpleName())
                .commit();
    }

    public void addFragmentWithBackStack(BaseFragment _fragment) {
        getTransaction()
                .add(mContainerId, _fragment)
                .addToBackStack(_fragment.getClass().getSimpleName())
                .commit();
    }

    public void popBackStack() {
        mFragmentManager.popBackStack();
    }

    public BaseFragment getTopFragment() {
        return (BaseFragment) mFragmentManager.findFragmentById(mContainerId);
    }

    public BaseFragment getTopFragment(@IdRes int _containerId) {
        return (BaseFragment) mFragmentManager.findFragmentById(_containerId);
    }
}
