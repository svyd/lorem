package com.svyd.wodes.presentation.project.authorization.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;
import android.widget.Toast;

import com.svyd.wodes.R;
import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.ScopedInstance;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.base.widget.VerificationEditText;
import com.svyd.wodes.presentation.project.authorization.activity.AuthListener;
import com.svyd.wodes.presentation.project.authorization.contract.ICodeVerificationPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.CodeVerificationView;
import com.svyd.wodes.presentation.project.authorization.presenter.factory.CodeVerificationPresenterFactory;
import com.svyd.wodes.presentation.project.profile.edit.IEditProfileController;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Svyd on 10.08.2016.
 */
public class CodeVerificationFragment extends BaseFragment
        implements VerificationEditText.FilledListener, CodeVerificationView {

    @BindView(R.id.vetCode_FCV)
    VerificationEditText vetCode;

    @BindView(R.id.tvDescription_FCV)
    TextView tvDescription;

    @BindView(R.id.tvTitle_FCV)
    TextView tvTitle;

    private ProgressDialog mProgress;
    private ICodeVerificationPresenter mPresenter;

    public static CodeVerificationFragment newInstance(Bundle args) {

        CodeVerificationFragment fragment = new CodeVerificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_code_verification);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUi();
        initPresenter();

        if (savedInstanceState == null) {
            mPresenter.setArguments(getArguments());
        } else {
            mPresenter.onRestoreInstanceState(savedInstanceState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mPresenter.onSaveInstanceState(outState);
    }

    private void initPresenter() {
        PresenterFactory<ICodeVerificationPresenter, CodeVerificationView> factory =
                new CodeVerificationPresenterFactory();
        mPresenter = factory.providePresenter(getArguments(), this);
        mPresenter.initialize();
    }

    private void initUi() {
        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle(WodesApplication.getStringResource(R.string.txt_wait));
        mProgress.setMessage(WodesApplication.getStringResource(R.string.txt_sending_code));

        vetCode.setFilledListener(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onFilled(String code) {
        mPresenter.onCodeFilled(code);
    }

    @Override
    public void onEmpty() {
        mPresenter.onCodeEmpty();
    }

    @OnClick(R.id.tvSendAgain_FCV)
    void onClick() {
        mPresenter.onSendAgain();
    }

    @Override
    public void showProgress() {
        mProgress.show();
    }

    @Override
    public void hideProgress() {
        mProgress.hide();
    }

    @Override
    public void clearCode() {
        vetCode.clear();
    }

    @Override
    public void onEditedPhoneValidated() {
        ((IEditProfileController) getActivity()).setValidatedPhone(true);
        ((IEditProfileController) getActivity()).done();
    }

    @Override
    public void navigateNext(ProfileInfo info) {
        ((AuthListener) getBaseActivity()).navigateToProfile(info);
    }

    @Override
    public void setProgressTitle(String title) {
        mProgress.setTitle(title);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void dismissKeyboard() {
        hideKeyboard();
    }

    @Override
    public void setDarkTextColor() {
        tvTitle.setTextColor(getResources().getColor(R.color.textColorBlack));
        tvDescription.setTextColor(getResources().getColor(R.color.textColorDark));
        vetCode.setTextColor(getResources().getColor(R.color.textColorDark));
    }

    @Override
    @SuppressWarnings("unchecked")
    public AuthRepository provideScopedRepository() {
        if (getActivity() instanceof ScopedInstance) {
            return ((ScopedInstance<AuthRepository>) getActivity()).provideScopedInstance();
        } else {
            return null;
        }
    }
}
