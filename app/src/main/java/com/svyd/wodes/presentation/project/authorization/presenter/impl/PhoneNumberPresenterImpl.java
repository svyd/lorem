package com.svyd.wodes.presentation.project.authorization.presenter.impl;

import android.os.Bundle;
import android.util.Log;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.tool.PatternValidator;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.contract.IPhoneNumberPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.PhoneNumberView;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;

import java.util.concurrent.TimeUnit;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;

/**
 * Created by Svyd on 23.08.2016.
 */
public class PhoneNumberPresenterImpl implements IPhoneNumberPresenter {

    private static final int DEBOUNCE = 500;
    private static final String TAG = PhoneNumberPresenterImpl.class.getSimpleName();
    private static final String REQUEST_STATE = "request_state";

    private PhoneNumberView mView;
    private PostInteractor<PhoneWrapper> mPhoneInteractor;
    private PublishSubject<String> mPhoneSubject;
    private SignUpRequest mRequest;

    public PhoneNumberPresenterImpl(PhoneNumberView view, PostInteractor<PhoneWrapper> interactor) {
        mPhoneInteractor = interactor;
        mView = view;
        mRequest = new SignUpRequest();
        initSubject();
    }

    private void initSubject() {
        mPhoneSubject = PublishSubject.create();
        mPhoneSubject.debounce(DEBOUNCE, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::validatePhoneNumber);
    }

    void validatePhoneNumber(String number) {
        if (!PatternValidator.isPhoneNumberValid(number)) {
            mView.showInputError(WodesApplication.getContext().getString(R.string.txt_invalid_phone));
            mView.setPhoneChecked(false);
            mView.setNextEnabled(false);
        } else {
            mView.showInputError(null);
            mView.setPhoneChecked(true);
            mView.setNextEnabled(true);
        }
        mView.showPhoneProgress(false);
    }

    protected PhoneNumberView getView() {
        return mView;
    }

    protected SignUpRequest getSignUpRequest() {
        return  mRequest;
    }

    @Override
    public void onNextClick() {
        mView.showProgress();
        mView.dismissKeyboard();
        mRequest.setNumber(mView.getNumber());
        mPhoneInteractor.execute(new PhoneWrapper(mView.getNumber()), new PhoneObserver());
    }

    @Override
    public void onTextChanged(String text) {
        mView.setPhoneChecked(false);
        mView.showPhoneProgress(true);
        mPhoneSubject.onNext(text);
    }

    @Override
    public void initialize() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void setArguments(Bundle args) {
        if (args != null && args.containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            mRequest = (SignUpRequest) args.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle state) {
        if (state != null) {
            mRequest = (SignUpRequest) state.getSerializable(REQUEST_STATE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        state.putSerializable(REQUEST_STATE, mRequest);
    }

    @Override
    public void onStop() {
        mPhoneInteractor.unSubscribe();
    }

    private class PhoneObserver implements Observer<ResponseModel> {

        @Override
        public void onCompleted() {
            getView().navigateNext(getSignUpRequest());
        }

        @Override
        public void onError(Throwable e) {
            mView.showToast(WodesApplication.getStringResource(R.string.txt_network_error));
        }

        @Override
        public void onNext(ResponseModel responseModel) {
            Log.i(TAG, "onNext: " + responseModel.response);
            mView.hideProgress();
        }
    }
}
