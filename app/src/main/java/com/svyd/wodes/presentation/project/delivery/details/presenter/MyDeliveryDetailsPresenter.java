package com.svyd.wodes.presentation.project.delivery.details.presenter;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;

import com.svyd.wodes.R;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.data.size.SizeRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.details.contract.IDeliveryDetailsView;

import rx.Observer;

/**
 * Created by Svyd on 18.09.2016.
 */
public class MyDeliveryDetailsPresenter extends AbstractDeliveryDetailsPresenter {


    public MyDeliveryDetailsPresenter(IDeliveryDetailsView view, Bundle args) {
        super(view, args);
    }

    @Override
    public void initialize() {
        super.initialize();
        getView().showOwner(false);
        getView().setButtonText(WodesApplication.getStringResource(R.string.brn_matched));
    }

    @Override
    public void onCreateOptionsMenu(MenuInflater menuInflater, Menu menu) {
        menuInflater.inflate(R.menu.details_edit, menu);
    }

    @Override
    public void onButtonClick() {
        SearchRequest request = new SearchRequest.Builder()
                .setLimit(10)
                .setOffset(0)
                .setDeparture(getItem().getEntity().getFromLocation())
                .setArrival(getItem().getEntity().getToLocation())
                .build();
        InviteRequest inviteRequest = new InviteRequest(Constants.Role.SENDER);
        inviteRequest.setDelivery(getItem().getEntity().getId());
        getView().navigateToMatchedParcels(request, inviteRequest);
    }

}
