package com.svyd.wodes.presentation.project.authorization.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.svyd.wodes.R;
import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.data.authorization.AuthRepositoryImpl;
import com.svyd.wodes.data.authorization.AuthService;
import com.svyd.wodes.data.profile.cache.PreferenceProfileCache;
import com.svyd.wodes.presentation.base.ScopedInstance;
import com.svyd.wodes.presentation.base.tool.ServiceProvider;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.fragment.CodeVerificationFragment;
import com.svyd.wodes.presentation.project.authorization.fragment.ForgotPasswordFragment;
import com.svyd.wodes.presentation.project.authorization.fragment.PhoneNumberFragment;
import com.svyd.wodes.presentation.project.authorization.fragment.SignUpFragment;
import com.svyd.wodes.presentation.project.authorization.fragment.SplashFragment;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.home.activity.HomeActivity;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.blurry.Blurry;

/**
 * Created by Svyd on 17.07.2016.
 */
public class AuthorizationActivity extends BaseActivity implements AuthListener, ScopedInstance<AuthRepository> {

    @BindView(R.id.flBackground_AS)
    FrameLayout flBackground;

    @BindView(R.id.flRoot_AS)
    FrameLayout flRoot;

    private AuthRepository mAuthRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        flRoot.setBackground(null);
        if (checkLogout()) {
            initSignUpFragmentImmediately();
        } else {
            initSplashFragment();
        }
    }

    private void initSignUpFragmentImmediately() {
        blur();
        getFragmentNavigator().replaceFragment(SignUpFragment.newInstance());
    }

    private boolean checkLogout() {
        return getIntent().getBooleanExtra(Constants.AUTH_LOGOUT, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        BaseFragment fragment = getFragmentNavigator().getTopFragment();
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initSplashFragment() {
        getFragmentNavigator().addFragmentWithoutBackStack(SplashFragment.newInstance());
    }

    @Override
    public int getContainerId() {
        return R.id.flContainer_AS;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }

    @Override
    public void fadeInSignUp() {
        blur();
        getFragmentNavigator().replaceFragmentWithoutBackStackFade(SignUpFragment.newInstance());
    }

    @Override
    public void blur() {
        Blurry.with(this)
                .radius(5)
                .sampling(8)
                .async()
                .animate(400)
                .onto(flBackground);
    }

    @Override
    public void onSignUpClick(SignUpRequest request) {
        Bundle args = new Bundle();
        args.putInt(Constants.Flow.FLOW_KEY, Constants.Flow.AUTH_SIGN_UP);
        args.putSerializable(Constants.Extra.EXTRA_SERIALIZABLE, request);
        getFragmentNavigator().replaceFragmentWithBackStack(PhoneNumberFragment.newInstance(args));
    }

    @Override
    public void onNumberPassed(Bundle args) {
        getFragmentNavigator().replaceFragmentWithBackStack(CodeVerificationFragment.newInstance(args));
    }

    @Override
    public void navigateToProfile(ProfileInfo info) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_PROFILE, info);
        startActivity(intent);
        finish();
    }

    @Override
    public void onForgotPassword() {
        getFragmentNavigator().replaceFragmentWithoutBackStackSlideUp(ForgotPasswordFragment.newInstance());
    }

    @Override
    public void onAddNumber(ProfileInfo info) {
        Bundle args = new Bundle();
        args.putSerializable(Constants.Extra.EXTRA_PROFILE, info);
        args.putInt(Constants.Flow.FLOW_KEY, Constants.Flow.AUTH_ADD_NUMBER);
        getFragmentNavigator().replaceFragmentWithBackStack(PhoneNumberFragment.newInstance(args));
    }

    @Override
    public AuthRepository provideScopedInstance() {
        synchronized (AuthorizationActivity.this) {

            if (mAuthRepository == null) {
                mAuthRepository = new AuthRepositoryImpl(new ServiceProvider()
                        .provideWodesService(getApplicationContext(), AuthService.class), new PreferenceProfileCache());
            }

            return mAuthRepository;
        }
    }
}
