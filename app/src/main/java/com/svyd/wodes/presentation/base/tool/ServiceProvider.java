package com.svyd.wodes.presentation.base.tool;

import android.content.Context;

import com.svyd.wodes.data.net.global.ApiConstants;
import com.svyd.wodes.data.net.retrofit.GoogleApiRetrofitWrapper;
import com.svyd.wodes.data.net.retrofit.HttpClientWrapper;
import com.svyd.wodes.data.net.retrofit.WodesRetrofitWrapper;
import com.svyd.wodes.data.places.GooglePlacesService;

import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Svyd on 18.08.2016.
 */
public class ServiceProvider {

    public  <T> T provideWodesService(Context context, Class<T> clazz) {
        return WodesRetrofitWrapper.getInstance(
                HttpClientWrapper.getInstance(context).getClient(),
                GsonConverterFactory.create())
                .getRetrofit()
                .create(clazz);
    }

    public <T> T provideGooglePlacesService(Context context, Class<T> clazz) {
        return GoogleApiRetrofitWrapper.getInstance(
                HttpClientWrapper.getInstance(context).getClient(),
                GsonConverterFactory.create())
                .getRetrofit()
                .create(clazz);
    }
}
