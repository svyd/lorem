package com.svyd.wodes.presentation.project.profile.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.OnItemClickListener;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.mvp.PresenterFactory;
import com.svyd.wodes.presentation.base.ui.BaseFragment;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.activity.AuthorizationActivity;
import com.svyd.wodes.presentation.project.delivery.my.MyDeliveryActivity;
import com.svyd.wodes.presentation.project.dialog.list.invites.InvitesActivity;
import com.svyd.wodes.presentation.project.dialog.list.pending.PendingActivity;
import com.svyd.wodes.presentation.project.profile.adapter.ProfileAdapter;
import com.svyd.wodes.presentation.project.profile.contract.IProfilePresenter;
import com.svyd.wodes.presentation.project.profile.contract.ProfileView;
import com.svyd.wodes.presentation.project.profile.edit.EditProfileActivity;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;
import com.svyd.wodes.presentation.project.profile.model.ProfileItem;
import com.svyd.wodes.presentation.project.profile.presenter.ProfilePresenterFactory;
import com.svyd.wodes.presentation.base.widget.RatingView;
import com.svyd.wodes.presentation.project.parcel.list.my.view.MyParcelActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Svyd on 02.07.2016.
 */
public class ProfileFragment extends BaseFragment implements
        ProfileView,
        OnItemClickListener<ProfileItem> {

    @BindView(R.id.clContent_FP)
    CoordinatorLayout clContent;

    @BindView(R.id.appbar_FP)
    AppBarLayout appbar;

    @BindView(R.id.collapsing_toolbar_FP)
    CollapsingToolbarLayout collapsingToolbar;

    @BindView(R.id.ivAvatar_FP)
    ImageView ivAvatar;

    @BindView(R.id.toolbar_FP)
    Toolbar toolbar;

    @BindView(R.id.rvProfile_FP)
    RecyclerView rvProfile;

    @BindView(R.id.fabEdit_FP)
    FloatingActionButton fabEdit;

    @BindView(R.id.tvRating_FP)
    TextView tvExperience;

    @BindView(R.id.pbProfile_FP)
    ProgressBar pbProfile;

    @BindView(R.id.ravRating_FP)
    RatingView ravProfile;

    @BindView(R.id.rlHeaderContent_FP)
    RelativeLayout rlHeaderContent;

    @BindView(R.id.border)
    View border;

    private IProfilePresenter mPresenter;
    private PresenterFactory<IProfilePresenter, ProfileView> mFactory;
    private ProfileAdapter mAdapter;


    public static ProfileFragment newInstance(Bundle args) {

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        providePresenter();
        initList();
        mPresenter.setArguments(getArguments());
        mPresenter.initialize();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initHeader();
        getBaseActivity().setSupportActionBar(toolbar);
    }

    private void initHeader() {
        appbar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float percentage = ((float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange());
            float reversedPercentage = Math.abs(percentage - 1);
            rlHeaderContent.setAlpha(reversedPercentage);
//            fabEdit.setScaleX(reversedPercentage);
//            fabEdit.setScaleY(reversedPercentage);
//            fabEdit.setAlpha(reversedPercentage);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.handleResult(requestCode, resultCode, data);
    }

    private void providePresenter() {
        mFactory = new ProfilePresenterFactory();
        mPresenter = mFactory.providePresenter(getArguments(), this);
    }

    void initList() {
        mAdapter = new ProfileAdapter(getActivity());
        mAdapter.setOnItemClickListener(this);
        rvProfile.setLayoutManager(new LinearLayoutManager(rvProfile.getContext()));
        rvProfile.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected BasePresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void setAvatar(String url) {
        Glide.with(getActivity())
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .centerCrop()
                .animate(android.R.anim.fade_in)
                .into(ivAvatar);
    }

    @Override
    public void setName(String name) {
        collapsingToolbar.setTitle(name);
    }

    @Override
    public void setRating(int rating) {
        ravProfile.setRating(rating);
    }

    @Override
    public void setListData(List<ProfileItem> items) {
        rvProfile.setVisibility(View.VISIBLE);
        mAdapter.setItems(items);
    }

    @Override
    public void startEditProfile(ProfileInfo info) {
        EditProfileActivity.startEditProfile(getActivity(), info);
    }

    @Override
    public void hideFab() {
        fabEdit.setVisibility(View.GONE);
    }

    @Override
    public void setToolbar() {
        getBaseActivity().setSupportActionBar(toolbar);
    }

    @Override
    public void setExtraToolbarHeight() {
        CollapsingToolbarLayout.LayoutParams layoutParams = (CollapsingToolbarLayout.LayoutParams) toolbar.getLayoutParams();
        layoutParams.height = (int) getResources().getDimension(R.dimen.extra_toolbar_height);
        toolbar.setLayoutParams(layoutParams);
        toolbar.requestLayout();
    }

    @Override
    public void hideBorder() {
        border.setVisibility(View.GONE);
    }

    @Override
    public void navigateToMyDeliveries() {
        Intent intent = new Intent(getActivity(), MyDeliveryActivity.class);
        intent.putExtra(Constants.Flow.FLOW_KEY, Constants.Flow.SHOW_ITEMS);
        getActivity().startActivity(intent);
    }

    @Override
    public void navigateToMyParcels() {
        getActivity().startActivity(new Intent(getActivity(), MyParcelActivity.class));
    }

    @Override
    public void navigateToInvites() {
        startActivity(new Intent(getActivity(), InvitesActivity.class));
    }

    @Override
    public void navigateToPending() {
        startActivity(new Intent(getActivity(), PendingActivity.class));
    }

    @Override
    public void logout() {
        Intent intent = new Intent(getActivity(), AuthorizationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.AUTH_LOGOUT, true);
        startActivity(intent);
    }

    @Override
    public void showToast(String toast) {
        Toast.makeText(getActivity(), toast, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showBackButton() {
        getBaseActivity().enableBackButton(true);
    }

    @Override
    public void showProgress() {
        pbProfile.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbProfile.setVisibility(View.GONE);
    }

    @OnClick(R.id.fabEdit_FP)
    void onFabClick() {
        mPresenter.onFabClick();
    }

    @Override
    public void onItemClick(int index, ProfileItem item, View view) {
        mPresenter.onItemClick(index, item);
    }
}
