package com.svyd.wodes.presentation.base;

/**
 * Created by Svyd on 18.08.2016.
 */
public interface ScopedInstance<T> {
    T provideScopedInstance();
}
