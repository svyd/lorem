package com.svyd.wodes.presentation.project.delivery.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.create.CreateDeliveryActivity;
import com.svyd.wodes.presentation.project.delivery.details.DeliveryDetailsActivity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.my.contract.IMyDeliveryPresenter;
import com.svyd.wodes.presentation.project.delivery.my.contract.IMyDeliveryView;
import com.svyd.wodes.presentation.project.delivery.my.presenter.MyDeliveryPresenterFactory;
import com.svyd.wodes.presentation.project.home.activity.HomeActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Svyd on 07.09.2016.
 */
public class MyDeliveryActivity extends BaseActivity implements IMyDeliveryView {

    @BindView(R.id.pbDelivery_AMD)
    ProgressBar pbDelivery;

    @BindView(R.id.rvDelivery_AMD)
    RecyclerView rvDelivery;

    @BindView(R.id.tvNoDeliveries_AMD)
    TextView tvNoDeliveries;

    private MyDeliveryAdapter mAdapter;
    private LazyLoadListener mScrollListener;
    private IMyDeliveryPresenter mPresenter;
    private Unbinder mUnbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_delivery);
        enableBackButton(true);
        mUnbinder = ButterKnife.bind(this);
        initPresenter();
        initList();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mPresenter.onCreateOptionsMenu(getMenuInflater(), menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.handleResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.miAdd_AI:
                mPresenter.onCreateItem();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initPresenter() {
        mPresenter = new MyDeliveryPresenterFactory().providePresenter(getIntent().getExtras(), this);
        mPresenter.initialize();
    }

    private void initList() {
        mScrollListener = new LazyLoadListener(mPresenter);
        mAdapter = new MyDeliveryAdapter(this);
        mAdapter.setOnItemClickListener(mPresenter);
        rvDelivery.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvDelivery.addOnScrollListener(mScrollListener);
        rvDelivery.setAdapter(mAdapter);
    }

    @Override
    public int getContainerId() {
        return 0;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AMD;
    }

    @Override
    public void showProgress() {
        pbDelivery.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (pbDelivery != null) {
            pbDelivery.setVisibility(View.GONE);
        }
    }

    @Override
    public void setItems(List<DeliveryItem> items) {
        mAdapter.setItems(items);
    }

    @Override
    public void notifyChanges() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void enableScrollHandling() {
        mScrollListener.enable();
    }

    @Override
    public void setToolbarTitle(String title) {
        setTitle(title);
    }

    @Override
    public void showStubText() {
        tvNoDeliveries.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideStubText() {
        tvNoDeliveries.setVisibility(View.GONE);
    }

    @Override
    public void navigateToAddDelivery() {
        Intent intent = new Intent(MyDeliveryActivity.this, CreateDeliveryActivity.class);
        intent.putExtra(Constants.Flow.FLOW_KEY, Constants.Flow.CREATE_ITEM);
        if (checkLollipop()) {

            TypedValue tv = new TypedValue();
            int toolbarHeight = 0;
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                toolbarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
            }
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra("x", width - 36);
            intent.putExtra("y", toolbarHeight);
        }

        startActivityForResult(intent, CreateDeliveryActivity.REQUEST_CREATE_DELIVERY);
    }

    @Override
    public void navigateToDetails(int index, DeliveryItem item, View view) {

        Intent intent = new Intent(MyDeliveryActivity.this, DeliveryDetailsActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_POSITION, index);
        intent.putExtra(Constants.Flow.FLOW_KEY, Constants.Flow.DETAILS_MY);
        intent.putExtra(Constants.Extra.EXTRA_DELIVERY, item);
        startActivityForResult(intent, DeliveryDetailsActivity.REQUEST_CODE_EDIT);
    }

    @Override
    public void addItem(DeliveryItem item) {
        rvDelivery.scrollToPosition(0);
        mAdapter.addItem(item);
    }

    @Override
    public void editItem(DeliveryItem item, int position) {
        mAdapter.swapItem(item, position);
    }


    @Override
    public void showAcceptDialog() {
        showDialog();
    }

    @Override
    public void navigateHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.Extra.EXTRA_OPTIONS, Constants.Extra.EXTRA_OPTION_UPDATE);
        startActivity(intent);
    }

    void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.alert_title_contact_carrier)
                .setMessage(R.string.alert_message_contact_carrier)
                .setCancelable(false)
                .setPositiveButton(R.string.confirm, (dialog1, which) -> {
                    mPresenter.onAccept();
                    dialog1.dismiss();
                })
                .setNegativeButton(R.string.cancel,
                        (dialog, id) -> {
                            dialog.cancel();
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
