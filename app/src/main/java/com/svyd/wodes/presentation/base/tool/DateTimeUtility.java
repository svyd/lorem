package com.svyd.wodes.presentation.base.tool;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.application.WodesApplication;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;

/**
 * Created by Svyd on 30.08.2016.
 */
public class DateTimeUtility {
    public static String formatDate(String _date, boolean withTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(WodesApplication.getStringResource(R.string.pattern_iso));
        DateTime dt = formatter.parseDateTime(validateString(_date));
        DateTime currentDate = new DateTime(DateTimeZone.UTC);

        String pattern = WodesApplication.getStringResource(R.string.pattern_date);
        if (withTime) {
            pattern = WodesApplication.getStringResource(R.string.pattern_date_time);
        }
        String formattedDate = dt.toString(pattern);
        if ((dt.year() == currentDate.year()) && (dt.monthOfYear() == currentDate.monthOfYear())) {
            if (dt.dayOfMonth() == currentDate.dayOfMonth()) {
                formattedDate = WodesApplication.getStringResource(R.string.today);
            } else if (currentDate.plusDays(1).dayOfMonth() == dt.dayOfMonth()) {
                formattedDate = WodesApplication.getStringResource(R.string.tomorrow);
            }
        }
        return formattedDate;
    }

    public static String toIso(Calendar calendar) {

        DateTime dateTime = new DateTime(calendar);
        DateTimeFormatter formatter = DateTimeFormat.forPattern(WodesApplication.getStringResource(R.string.pattern_iso));

        return dateTime.toString(formatter);
    }

    private static String validateString(String _field) {
        return (_field == null) ? "" : _field;
    }
}
