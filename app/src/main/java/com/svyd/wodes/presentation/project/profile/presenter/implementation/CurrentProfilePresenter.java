package com.svyd.wodes.presentation.project.profile.presenter.implementation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.svyd.wodes.domain.base.Interactor;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.profile.contract.ProfileView;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.profile.edit.EditProfileActivity;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observer;

/**
 * Created by Svyd on 04.07.2016.
 */
public class CurrentProfilePresenter extends AbstractProfilePresenter {

    private static final String TAG = CurrentProfilePresenter.class.getSimpleName();
    private Interactor mLogoutInteractor;

    public CurrentProfilePresenter(PostInteractor<String> interactor, Interactor logoutInteractor, ProfileView view) {
        super(interactor, view);
        mLogoutInteractor = logoutInteractor;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
//        getView().setExtraToolbarHeight();
    }

    @Override
    public void handleResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EditProfileActivity.REQUEST_CODE_EDIT_PROFILE
                && resultCode == Activity.RESULT_OK) {
            onProfileUpdated(data);
        }
    }

    @Override
    public void initialize() {
    }

    void onProfileUpdated(Intent data) {
        ProfileInfo info = (ProfileInfo) data.getSerializableExtra(Constants.Extra.EXTRA_PROFILE);

        setProfile(info);
        if (!info.getName().equals(getProfileInfo().getName())) {
            getView().setName(info.getName());
        }

        if (info.getAvatar() != null) {
            getView().setAvatar(info.getAvatar());
        }

        getView().setListData(info.getItems());
    }

    @Override
    protected void getProfile() {
        getInteractor().execute("", new ProfileObserver());
        getView().showProgress();
    }

    @Override
    protected void onLogoutClick() {
        mLogoutInteractor.execute(new LogoutObserver());
    }

    private class LogoutObserver implements Observer<ResponseModel> {

        @Override
        public void onCompleted() {
            getView().logout();
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "onError() called with: " + "e = [" + e + "]");
        }

        @Override
        public void onNext(ResponseModel responseModel) {
            Log.d(TAG, "onNext: ");
        }
    }
}
