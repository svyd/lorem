package com.svyd.wodes.presentation.project.delivery.my;

import android.app.Activity;
import android.content.Context;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.project.delivery.base.BaseDeliveryAdapter;

/**
 * Created by Svyd on 07.09.2016.
 */
public class MyDeliveryAdapter extends BaseDeliveryAdapter {
    public MyDeliveryAdapter(Activity context) {
        super(context);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.list_item_my_delivery;
    }
}
