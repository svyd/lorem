package com.svyd.wodes.presentation.project.parcel.matched.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.all.adapter.DeliveryAdapter;
import com.svyd.wodes.presentation.project.home.activity.HomeActivity;
import com.svyd.wodes.presentation.project.parcel.list.adapter.ParcelAdapter;
import com.svyd.wodes.presentation.project.parcel.matched.contract.IMatchedParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.matched.contract.IMatchedParcelView;
import com.svyd.wodes.presentation.project.parcel.matched.presenter.MatchedParcelPresenterFactory;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 20.09.2016.
 */
public class MatchedParcelActivity extends BaseActivity implements IMatchedParcelView {

    @BindView(R.id.tvNoItems_AL)
    TextView tvNoItems;

    @BindView(R.id.rvItems_AL)
    RecyclerView rvItems;

    @BindView(R.id.pbProgress_AL)
    ProgressBar pbProgress;


    private LazyLoadListener mScrollListener;
    private ParcelAdapter mAdapter;
    private IMatchedParcelPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        initPresenter();
        initList();
        enableBackButton(true);
    }

    private void initPresenter() {
        mPresenter = new MatchedParcelPresenterFactory().providePresenter(getIntent().getExtras(), this);
        mPresenter.initialize();
    }

    private void initList() {
        mScrollListener = new LazyLoadListener(mPresenter);
        mAdapter = new ParcelAdapter(this);
        mAdapter.setOnItemClickListener(mPresenter);
        rvItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvItems.addOnScrollListener(mScrollListener);
        rvItems.setAdapter(mAdapter);
    }

    @Override
    public int getContainerId() {
        return 0;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_AL;
    }

    @Override
    public void setItems(List<ParcelItem> items) {
        mAdapter.setItems(items);
    }

    @Override
    public void notifyChanges() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void enableScrollHandling() {
        mScrollListener.enable();
    }

    @Override
    public void showStubText() {
        tvNoItems.setText(R.string.no_matched_parcels);
        tvNoItems.setVisibility(View.VISIBLE);
    }

    @Override
    public void setToolbarTitle(String title) {
        setTitle(title);
    }

    @Override
    public void showAcceptDialog() {
        showDialog();
    }

    @Override
    public void navigateHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.Extra.EXTRA_OPTIONS, Constants.Extra.EXTRA_OPTION_UPDATE);
        startActivity(intent);
    }

    void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.alert_title_contact_carrier)
                .setMessage(R.string.alert_message_contact_carrier)
                .setCancelable(false)
                .setPositiveButton(R.string.confirm, (dialog1, which) -> {
                    mPresenter.onAccept();
                    dialog1.dismiss();
                })
                .setNegativeButton(R.string.cancel,
                        (dialog, id) -> {
                            dialog.cancel();
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void showProgress() {
        pbProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbProgress.setVisibility(View.GONE);
    }
}
