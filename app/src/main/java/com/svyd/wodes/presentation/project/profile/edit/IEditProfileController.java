package com.svyd.wodes.presentation.project.profile.edit;

import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

/**
 * Created by Svyd on 30.08.2016.
 */
public interface IEditProfileController {
    void done();
    void onProfileUpdated(ProfileInfo info);
    void setValidatedPhone(boolean validated);
}
