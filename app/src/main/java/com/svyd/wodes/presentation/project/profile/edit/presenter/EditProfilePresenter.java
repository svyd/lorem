package com.svyd.wodes.presentation.project.profile.edit.presenter;

import android.os.Bundle;

import com.svyd.wodes.data.profile.edit.model.UpdateProfileRequest;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.base.tool.DateTimeUtility;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.profile.edit.contract.IEditProfilePresenter;
import com.svyd.wodes.presentation.project.profile.edit.contract.IEditProfileView;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import org.joda.time.DateTime;

import rx.Observer;

/**
 * Created by Svyd on 30.08.2016.
 */
public class EditProfilePresenter implements IEditProfilePresenter {

    private IEditProfileView mView;
    private UpdateProfileRequest mRequest;
    private PostInteractor<UpdateProfileRequest> mInteractor;
    private ProfileInfo mInfo;
    private String mOriginalPhone;

    public EditProfilePresenter(IEditProfileView view,
                                Bundle args,
                                PostInteractor<UpdateProfileRequest> interactor) {
        mView = view;
        mInteractor = interactor;
        setArguments(args);
    }

    public void setArguments(Bundle arguments) {
        if (arguments.containsKey(Constants.Extra.EXTRA_PROFILE)) {
            mInfo = (ProfileInfo) arguments.getSerializable(Constants.Extra.EXTRA_PROFILE);
            mRequest = new UpdateProfileRequest(mInfo);
            mOriginalPhone = mRequest.getPhone();
        }
    }

    void initUser() {
        if (mInfo.getBirthday() != null) {
            mView.setBirthday(DateTimeUtility.formatDate(mInfo.getBirthday(), false));
        }
        mView.setAvatar(mInfo.getAvatar());
        mView.setEmail(mInfo.getEmail());
        mView.setName(mInfo.getName());
        mView.setNumber(mInfo.getPhone());
        if (mInfo.getGender() != null) {
            switch (mInfo.getGender()) {
                case Constants.Gender.GENDER_FEMALE:
                    mView.setGenderFemale();
                    break;
                case Constants.Gender.GENDER_MALE:
                    mView.setGenderMale();
                    break;
            }
        }
    }

    @Override
    public void onNameChanged(String name) {
        mRequest.setFullName(name);
    }

    @Override
    public void onNumberChanged(String number) {
        mRequest.setPhone(number);
    }

    @Override
    public void onEmailChanged(String email) {
        mRequest.setEmail(email);
    }

    @Override
    public void onAvatarClick() {
        mView.showImagePicker();
    }

    @Override
    public void onGenderMaleSelected() {
        mRequest.setGender(Constants.Gender.GENDER_MALE);
    }

    @Override
    public void onGenderFemaleSelected() {
        mRequest.setGender(Constants.Gender.GENDER_FEMALE);
    }

    @Override
    public void setAvatarPath(String path) {
        mRequest.setAvatar(path);
        mView.setAvatar(path);
    }

    @Override
    public void onBirthdayChanged(int _year, int _month, int _day) {
        mRequest.setBirthday(
                String.valueOf(_year)
                + "-"
                + String.valueOf(_month)
                + "-"
                + String.valueOf(_day)
        );
        mView.setBirthday(String.valueOf(_day)
                + "."
                + String.valueOf(_month)
                + "."
                + String.valueOf(_year));
    }

    @Override
    public void onDone() {
        mView.showProgress();
        if (mOriginalPhone.equals(mRequest.getPhone())) {
            mRequest.setPhone(null);
        }
        mInteractor.execute(mRequest, new UpdateObserver());
    }

    @Override
    public void onBirthdayClick() {
        DateTime dateTime = DateTime.now().withTimeAtStartOfDay();
        mView.showDatePicker(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth());
    }

    @Override
    public void initialize() {
        initUser();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    private class UpdateObserver implements Observer<ProfileInfo> {

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            mView.hideProgress();
        }

        @Override
        public void onNext(ProfileInfo info) {
            info.setPhone(mRequest.getPhone());
            mView.onProfileUpdated(info);
        }
    }
}
