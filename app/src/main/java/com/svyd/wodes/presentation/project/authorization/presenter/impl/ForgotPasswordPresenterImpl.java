package com.svyd.wodes.presentation.project.authorization.presenter.impl;

import android.os.Bundle;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.tool.PatternValidator;
import com.svyd.wodes.presentation.project.authorization.contract.IForgotPasswordPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.ForgotPasswordView;
import com.svyd.wodes.presentation.project.authorization.model.EmailWrapper;

import java.util.concurrent.TimeUnit;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;

/**
 * Created by Svyd on 24.08.2016.
 */
public class ForgotPasswordPresenterImpl implements IForgotPasswordPresenter {

    private static final long DEBOUNCE = 500;
    private static final String STATE_EMAIL = "state_email";

    private PostInteractor<EmailWrapper> mInteractor;
    private PublishSubject<String> mEmailSubject;
    private ForgotPasswordView mView;
    private String mEmail;

    public ForgotPasswordPresenterImpl(ForgotPasswordView view, PostInteractor<EmailWrapper> interactor) {
        mInteractor = interactor;
        mView = view;
    }

    @Override
    public void onEmailChanged(String email) {
        mView.showEmailProgress(true);
        mView.setEmailChecked(false);
        mEmailSubject.onNext(email);
    }

    @Override
    public void onRequestClick() {
        if (mEmail != null) {
            mView.showProgress();
            mInteractor.execute(new EmailWrapper(mEmail), new EmailObserver());
        } else {
            mView.showToast(WodesApplication.getStringResource(R.string.error_invalid_email));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        state.putString(STATE_EMAIL, mEmail);
    }

    @Override
    public void onRestoreInstanceState(Bundle state) {
        if (state != null && state.containsKey(STATE_EMAIL)) {
            mEmail = state.getString(STATE_EMAIL);
        }
    }

    @Override
    public void initialize() {
        initSubject();
    }

    private void initSubject() {
        mEmailSubject = PublishSubject.create();
        mEmailSubject.debounce(DEBOUNCE, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::validateEmail);
    }

    private void validateEmail(String email) {
        if (!PatternValidator.isEmailValid(email)) {
            mView.setEmailError(WodesApplication.getContext().getString(R.string.error_invalid_email));
            mView.setEmailChecked(false);
            mEmail = null;
        } else {
            mEmail = email;
            mView.setEmailChecked(true);
        }
        mView.showEmailProgress(false);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    private void handleError(Throwable e) {
        if (e instanceof HttpException) {
            if (((HttpException) e).code() == 400) {
                mView.showToast(WodesApplication.getStringResource(R.string.txt_no_user_error));
            } else {
                mView.showToast(WodesApplication.getStringResource(R.string.txt_unknown_error));
            }
        } else {
            mView.showToast(WodesApplication.getStringResource(R.string.txt_network_error));
        }
    }

    private class EmailObserver implements Observer<ResponseModel> {

        @Override
        public void onCompleted() {
            mView.navigateBack();
        }

        @Override
        public void onError(Throwable e) {
            handleError(e);
            mView.hideProgress();
        }

        @Override
        public void onNext(ResponseModel responseModel) {
            mView.hideProgress();
            mView.showToast(WodesApplication.getStringResource(R.string.password_requested));
        }
    }
}
