package com.svyd.wodes.presentation.base.tool;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Svyd on 07.09.2016.
 */
public class LazyLoadListener extends RecyclerView.OnScrollListener {
    private boolean handleScrolls = true;
    private LoadMoreListener loadMoreListener;
    public static final int ROW_COUNT = 10;

    public LazyLoadListener(LoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void enable() {
        this.handleScrolls = true;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (layoutManager.getItemCount() >= (ROW_COUNT - 3) &&
                layoutManager.findFirstVisibleItemPosition() ==
                        layoutManager.getItemCount() - layoutManager.getChildCount()) {

            if (handleScrolls) {
                if (loadMoreListener != null)
                    loadMoreListener.onLoadMore();
            }
            handleScrolls = false;
        }
    }

    public interface LoadMoreListener {
        void onLoadMore();
    }
}
