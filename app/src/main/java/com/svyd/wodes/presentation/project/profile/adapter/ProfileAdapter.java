package com.svyd.wodes.presentation.project.profile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.OnItemClickListener;
import com.svyd.wodes.presentation.project.profile.model.ProfileItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Svyd on 04.07.2016.
 */
public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ProfileHolder> {

    private Context mContext;
    private List<ProfileItem> mItems;
    private OnItemClickListener<ProfileItem> mListener;

    public ProfileAdapter(Context context) {
        mContext = context;
        mItems = new ArrayList<>();
    }

    public void setItems(List<ProfileItem> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener<ProfileItem> listener) {
        mListener = listener;
    }

    @Override
    public ProfileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProfileHolder(LayoutInflater.from(mContext).inflate(R.layout.item_profile_details, parent, false));
    }

    @Override
    public void onBindViewHolder(ProfileHolder holder, int position) {
        ProfileItem item = mItems.get(position);

        holder.setClickable(item.isClickable());
        holder.setDivider(item.hasDivider());
        holder.setIcon(item.getIcon());
        if (item.hasOnlyTitle()) {
            holder.setBigTitle(item.getTitle());
        } else {
            holder.setTitle(item.getTitle());
            holder.setSubTitle(item.getSubtitle());
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ProfileHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ivIcon_IPD)
        ImageView ivIcon;

        @BindView(R.id.tvTitle_IPD)
        TextView tvTitle;

        @BindView(R.id.tvSubtitle_IPD)
        TextView tvSubtitle;

        @BindView(R.id.ivArrow_IPD)
        ImageView ivArrow;

        @BindView(R.id.vDivider_IPD)
        View divider;

        @BindView(R.id.tvBigTitle_IPD)
        TextView tvBigTitle;

        View rlContainer;

        public ProfileHolder(View itemView) {
            super(itemView);
            rlContainer = itemView;
            rlContainer.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        public void setClickable(boolean clickable) {
            rlContainer.setClickable(clickable);
            ivArrow.setVisibility(clickable ? View.VISIBLE : View.GONE);
        }

        public void setDivider(boolean hasDivider) {
            divider.setVisibility(hasDivider ? View.VISIBLE : View.GONE);
        }

        public void setTitle(String title) {
            tvBigTitle.setVisibility(View.GONE);
            tvTitle.setText(title);
        }

        public void setBigTitle(String title) {
            tvBigTitle.setVisibility(View.VISIBLE);
            tvBigTitle.setText(title);
        }

        public void setSubTitle(String subtitle) {
            tvSubtitle.setText(subtitle);
        }

        public void setIcon(int icon) {
            ivIcon.setImageResource(icon);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(getLayoutPosition(), mItems.get(getLayoutPosition()), v);
            }
        }
    }
}
