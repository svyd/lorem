package com.svyd.wodes.presentation.project.profile.presenter.implementation;

import android.content.Intent;
import android.os.Bundle;

import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.profile.contract.IProfilePresenter;
import com.svyd.wodes.presentation.project.profile.contract.ProfileView;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;
import com.svyd.wodes.presentation.project.profile.model.ProfileItem;
import com.svyd.wodes.domain.base.PostInteractor;

import rx.Observer;

/**
 * Created by Svyd on 05.07.2016.
 */
public abstract class AbstractProfilePresenter implements IProfilePresenter {
    private PostInteractor<String> mProfileInteractor;
    private ProfileInfo mProfile;
    private ProfileView mView;

    public AbstractProfilePresenter(PostInteractor<String> interactor, ProfileView view) {
        mProfileInteractor = interactor;
        mView = view;
    }

    protected void setProfile(ProfileInfo info) {
        mProfile = info;
    }

    @Override
    public void handleResult(int requestCode, int resultCode, Intent data) {

    }

    protected ProfileInfo getProfileInfo() {
        return mProfile;
    }

    @Override
    public void setArguments(Bundle args) {
        if (args != null && args.containsKey(Constants.Extra.EXTRA_PROFILE)) {
            mProfile = (ProfileInfo) args.getSerializable(Constants.Extra.EXTRA_PROFILE);
            setUpView(mProfile);
        } else {
            getProfile();
        }
    }

    protected PostInteractor<String> getInteractor() {
        return mProfileInteractor;
    }

    protected abstract void getProfile();

    @Override
    public void onStart() {

    }

    @Override
    public void onItemClick(int index, ProfileItem item) {
        switch (item.getMeta()) {
            case Constants.ProfileItems.INVITES:
                onInvitesClick();
                break;
            case Constants.ProfileItems.PENDING:
                onPendingClick();
                break;
            case Constants.ProfileItems.DELIVERIES:
                onDeliveriesClick();
                break;
            case Constants.ProfileItems.PARCELS:
                onParcelsClick(item);
                break;
            case Constants.ProfileItems.NUMBER:
                onNumberClick(item);
                break;
            case Constants.ProfileItems.EMAIL:
                onEmailClick(item);
                break;
            case Constants.ProfileItems.GENDER:
                onGenderClick(item);
                break;
            case Constants.ProfileItems.BIRTHDAY:
                onBirthdayClick(item);
                break;
            case Constants.ProfileItems.LOGOUT:
                onLogoutClick();
                break;
        }
    }

    protected abstract void onLogoutClick();

    protected void onInvitesClick() {
        mView.navigateToInvites();
    }

    protected void onPendingClick() {
        mView.navigateToPending();
    }

    protected void onDeliveriesClick() {
        mView.navigateToMyDeliveries();
    }

    protected void onParcelsClick(ProfileItem item) {
        mView.navigateToMyParcels();
    }

    protected void onNumberClick(ProfileItem item) {
        mView.showToast(item.getTitle());
    }

    protected void onEmailClick(ProfileItem item) {
        mView.showToast(item.getTitle());
    }

    protected void onGenderClick(ProfileItem item) {
        mView.showToast(item.getTitle());
    }

    protected void onBirthdayClick(ProfileItem item) {
        mView.showToast(item.getTitle());
    }

    protected ProfileView getView() {
        return mView;
    }

    @Override
    public void onStop() {
        mProfileInteractor.unSubscribe();
    }

    private void setUpView(ProfileInfo info) {
        mView.setName(info.getName());
        mView.setAvatar(info.getAvatar());
        mView.setRating(info.getRating());
        mView.setListData(info.getItems());
    }

    @Override
    public void onFabClick() {
        mView.startEditProfile(mProfile);
    }

    protected class ProfileObserver implements Observer<ProfileInfo> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            mView.hideProgress();
            mView.showToast(e.getMessage());
        }

        @Override
        public void onNext(ProfileInfo profileInfo) {
            setUpView(profileInfo);
            mView.hideProgress();
        }
    }
}
