package com.svyd.wodes.presentation.project.delivery.my.contract;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.svyd.wodes.presentation.base.OnItemClickListener;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;

/**
 * Created by Svyd on 07.09.2016.
 */
public interface IMyDeliveryPresenter extends BasePresenter, LazyLoadListener.LoadMoreListener, OnItemClickListener<DeliveryItem> {
    void onCreateItem();
    void handleResult(int requestCode, int resultCode, Intent data);
    void onCreateOptionsMenu(MenuInflater menuInflater, Menu menu);
    void onAccept();
}
