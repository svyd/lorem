package com.svyd.wodes.presentation.project.authorization.activity;

import android.os.Bundle;

import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

/**
 * Created by Svyd on 05.08.2016.
 */
public interface AuthListener {
    void fadeInSignUp();
    void onSignUpClick(SignUpRequest request);
    void onNumberPassed(Bundle args);
    void navigateToProfile(ProfileInfo info);
    void onForgotPassword();
    void onAddNumber(ProfileInfo info);
    void blur();
}
