package com.svyd.wodes.presentation.project.delivery.my.contract;

import android.view.View;

import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.base.tool.LazyLoadListener;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;

import java.util.List;

/**
 * Created by Svyd on 07.09.2016.
 */
public interface IMyDeliveryView extends BaseView {
    void setItems(List<DeliveryItem> items);
    void notifyChanges();
    void enableScrollHandling();
    void setToolbarTitle(String title);
    void showStubText();
    void hideStubText();
    void navigateToAddDelivery();
    void navigateToDetails(int index, DeliveryItem item, View view);
    void addItem(DeliveryItem item);
    void editItem(DeliveryItem item, int position);
    void navigateHome();
    void showAcceptDialog();
}
