package com.svyd.wodes.presentation.project.profile.model;

import com.google.gson.annotations.SerializedName;
import com.svyd.wodes.presentation.global.Constants;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Svyd on 04.07.2016.
 */
public class ProfileInfo implements Serializable {


    @SerializedName("dob")
    private String birthday;

    @SerializedName("invites")
    private int invites;

    @SerializedName("pending")
    private int pending;

    @SerializedName("deliveries")
    private int myDeliveries;

    @SerializedName("parcels")
    private int myParcels;

    @SerializedName("rating")
    private int rating;

    @SerializedName("fullName")
    private String fullName;

    @SerializedName("avatar")
    private String avatar;

    @SerializedName("phone")
    private String phone;

    @SerializedName("email")
    private String email;

    @SerializedName("gender")
    private String gender;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("isConfirmedEmail")
    private boolean isConfirmedEmail;

    @SerializedName("isConfirmedPhone")
    private boolean isConfirmedPhone;

    @SerializedName("isCurrent")
    private boolean isCurrent;

    @SerializedName("isSocial")
    private boolean isSocial;

    @SerializedName("isSyncedPayPal")
    private boolean isSyncedPayPal;

    private List<ProfileItem> items;

    private boolean isPhoneEnabled;

    public String getCreatedAt() {
        return createdAt;
    }

    public boolean isPhoneEnabled() {
        return isPhoneEnabled;
    }

    public void setPhoneEnabled(boolean phoneEnabled) {
        isPhoneEnabled = phoneEnabled;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setItems(List<ProfileItem> items) {
        this.items = items;
    }

    public List<ProfileItem> getItems() {
        return items;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setInvites(int invites) {
        this.invites = invites;
    }

    public void setMyDeliveries(int myDeliveries) {
        this.myDeliveries = myDeliveries;
    }

    public void setMyParcels(int myParcels) {
        this.myParcels = myParcels;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }

    public void setPending(int pending) {
        this.pending = pending;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        if (items != null) {
            for (ProfileItem item: items) {
                if (item.getMeta().equals(Constants.ProfileItems.NUMBER)) {
                    item.setTitle(phone);
                }
            }
        }
    }

    public int getInvites() {
        return invites;
    }

    public int getMyDeliveries() {
        return myDeliveries;
    }

    public int getMyParcels() {
        return myParcels;
    }

    public int getPending() {
        return pending;
    }

    public String getBirthday() {

        if (birthday != null && !birthday.equals("")) {
            return birthday;
        }
        return null;
    }

    public String getEmail() {
        if (email != null && !email.equals("")) {
            return email;
        }
        return null;
    }

    public String getGender() {
        if (gender != null && !gender.equals("")) {
            return gender;
        }
        return null;
    }

    public String getPhone() {
        if (phone != null && !phone.equals("")) {
            return phone;
        }
        return null;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setName(String name) {
        this.fullName = name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getRating() {
        return rating;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getName() {
        return fullName;
    }

    public static class Builder {

        private boolean isCurrent;
        private boolean isPhoneEnabled;

        private String name;
        private String avatar;
        private String phone;
        private String email;
        private String gender;
        private String birthday;
        private int rating;
        private int invites;
        private int pending;
        private int myDeliveries;
        private int myParcels;

        public boolean isPhoneEnabled() {
            return isPhoneEnabled;
        }

        public Builder setPhoneEnabled(boolean phoneEnabled) {
            isPhoneEnabled = phoneEnabled;
            return this;
        }

        public Builder setCurrent(boolean current) {
            isCurrent = current;
            return this;
        }

        public Builder setPending(int pending) {
            this.pending = pending;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setMyParcels(int myParcels) {
            this.myParcels = myParcels;
            return this;
        }

        public Builder setMyDeliveries(int myDeliveries) {
            this.myDeliveries = myDeliveries;
            return this;
        }

        public Builder setBirthday(String birthday) {
            this.birthday = birthday;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setGender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder setInvites(int invites) {
            this.invites = invites;
            return this;
        }

        public Builder setRating(int rating) {
            this.rating = rating;
            return this;
        }

        public Builder setAvatar(String avatar) {
            this.avatar = avatar;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public ProfileInfo build() {
            ProfileInfo info = new ProfileInfo();

            info.setAvatar(avatar);
            info.setName(name);
            info.setRating(rating);
            info.setPhone(phone);
            info.setPending(pending);
            info.setCurrent(isCurrent);
            info.setMyParcels(myParcels);
            info.setMyDeliveries(myDeliveries);
            info.setEmail(email);
            info.setGender(gender);
            info.setBirthday(birthday);
            info.setInvites(invites);
            info.setPhoneEnabled(isPhoneEnabled);

            return info;
        }
    }
}
