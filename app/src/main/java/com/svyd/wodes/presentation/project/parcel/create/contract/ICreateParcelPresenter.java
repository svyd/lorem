package com.svyd.wodes.presentation.project.parcel.create.contract;

import android.content.Intent;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;

import java.util.Calendar;

/**
 * Created by Svyd on 14.09.2016.
 */
public interface ICreateParcelPresenter extends BasePresenter {
    void onDepartureTextChanged(String text);
    void onArrivalTextChanged(String text);
    void onDepartureItemSelected(int position);
    void onArrivalItemSelected(int position);
    void onDateClick();
    void onSizeClick();
    void onTitleTextChanged(String text);
    void onDepartureDateChanged(Calendar calendar);
    void onSizeSelected(int position);
    void onDescriptionTextChanged(String text);
    void onPriceTextChanged(String text);
    void onPhotoAttached(String photo);
    void onNextClick();
    void onAccept();
    void onPhotoClick();
}
