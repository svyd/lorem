package com.svyd.wodes.presentation.base.ui;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.TextView;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.base.tool.FragmentNavigator;
import com.svyd.wodes.presentation.base.tool.ToolbarManager;
import com.svyd.wodes.presentation.project.delivery.base.PopupListAdapter;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by Svyd on 02.07.2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private FragmentNavigator mFragmentNavigator;
    private ToolbarManager mToolbarManager;
    private ListPopupWindow mListPopup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();
        uiChangeListener();
        initFields();
    }

    protected TextView getTitleTextView(Toolbar toolbar) {
        try {
            Class<?> toolbarClass = Toolbar.class;
            Field titleTextViewField = toolbarClass.getDeclaredField("mTitleTextView");
            titleTextViewField.setAccessible(true);
            TextView titleTextView = (TextView) titleTextViewField.get(toolbar);

            return titleTextView;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void uiChangeListener() {
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> {
            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                hideSystemUI();
            }
        });
    }

    protected void dismissListPopup() {
        mListPopup.dismiss();
    }

    protected void showListMenu(View anchor, List<String> data, AdapterView.OnItemClickListener listener, boolean wrap) {

        if (mListPopup.isShowing()) {
            mListPopup.dismiss();
        }

        if (wrap) {
            int width = getResources().getDimensionPixelSize(R.dimen.overflow_width);
            mListPopup.setWidth(width);
        } else {
            mListPopup.setWidth(ListPopupWindow.WRAP_CONTENT);
        }

        PopupListAdapter adapter = new PopupListAdapter(this, data);
        adapter.setOnItemClickListener(listener);
        mListPopup.setAnchorView(anchor);
        mListPopup.setAdapter(adapter);

        if (listener != null){
            mListPopup.setOnItemClickListener(listener);
            mListPopup.show();
        }
    }

    protected boolean checkLollipop() {
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        return currentApiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP;
    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    protected TextView getActionBarTitleView() {
        int id = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
        return (TextView) findViewById(id);
    }

    private void initFields() {
        mListPopup = new ListPopupWindow(this);
        mFragmentNavigator = new FragmentNavigator(this, getSupportFragmentManager(), getContainerId());
        mToolbarManager = new ToolbarManager(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (getPresenter() != null) {
            getPresenter().onStop();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        if (getToolbarId() == 0)
            return;

        Toolbar toolbar = (Toolbar) findViewById(getToolbarId());
        if (toolbar != null)
            setSupportActionBar(toolbar);
    }

    protected void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void enableBackButton(boolean enable) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(enable);
            actionBar.setDisplayHomeAsUpEnabled(enable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public FragmentNavigator getFragmentNavigator() {
        return mFragmentNavigator;
    }

    public ToolbarManager getToolbarManager() {
        return mToolbarManager;
    }

    public BasePresenter getPresenter() {
        return null;
    }

    //AbstractMethods
    public abstract
    @IdRes
    int getContainerId();

    public abstract
    @IdRes
    int getToolbarId();
}
