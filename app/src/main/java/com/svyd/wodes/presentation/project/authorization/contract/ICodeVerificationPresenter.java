package com.svyd.wodes.presentation.project.authorization.contract;

import android.os.Bundle;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;

/**
 * Created by Svyd on 13.08.2016.
 */
public interface ICodeVerificationPresenter extends BasePresenter {
    void onCodeFilled(String code);
    void onCodeEmpty();
    void onSendAgain();
    void onRestoreInstanceState(Bundle state);
    void onSaveInstanceState(Bundle state);
    void setArguments(Bundle args);
}
