package com.svyd.wodes.presentation.project.delivery.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Svyd on 09.09.2016.
 */
public class LocationModel {

    private String mCountry;
    private String mCountryCode;
    private String mCity;

    public LocationModel() {
        this(0, 0);
    }

    public LocationModel(double lat, double lng) {
        result = new Result(lat, lng);
    }

    public double getLat() {
        return result.geometry.location.lat;
    }

    public double getLon() {
        return result.geometry.location.lng;
    }

    public void setCity(String city) {
        this.mCity = city;
    }

    public void setCountry(String country) {
        this.mCountry = country;
    }

    public double[] getAsLatLon() {
        return new double[] {getLat(), getLon()};
    }

    public String getCity() {
        return mCity;
    }

    public String getCountry() {
        return mCountry;
    }

    @Expose
    @SerializedName("result")
    public Result result;

    public String[] getLatLngArray() {
        String[] locationArray = new String[2];
        locationArray[0] = String.valueOf(result.geometry.location.lat);
        locationArray[1] = String.valueOf(result.geometry.location.lng);
        return locationArray;
    }

    public class Result implements Serializable {
        @Expose
        @SerializedName("address_components")
        public List<AddressComponent> mListAdressComponent;

        @Expose
        @SerializedName("formatted_address")
        public String address;

        @Expose
        public Geometry geometry;

        public Result(double _lat, double _lon) {
            geometry = new Geometry();
            geometry.location.lat = _lat;
            geometry.location.lng = _lon;
        }

        public class Geometry implements Serializable {
            @Expose
            public Location location;

            public Geometry() {
                location = new Location();
            }

            public class Location implements Serializable {
                @Expose
                double lat;
                @Expose
                double lng;
            }
        }

        public class AddressComponent implements Serializable {
            @Expose
            @SerializedName("long_name")
            public String longName;

            @Expose
            @SerializedName("short_name")
            public String shortName;

            @Expose
            @SerializedName("types")
            public List<String> mTypes;
        }
    }
}
