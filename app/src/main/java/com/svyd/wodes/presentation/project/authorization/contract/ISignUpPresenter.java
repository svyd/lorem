package com.svyd.wodes.presentation.project.authorization.contract;

import android.os.Bundle;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;

/**
 * Created by Svyd on 17.07.2016.
 */
public interface ISignUpPresenter extends BasePresenter {
    void onEmailChanged(String email);
    void onFullNameChanged(String fullName);
    void onPasswordChanged(String password);
    void onConfirmPasswordChanged(String confirm);
    void onSignUpClick();
    void onFacebookClick();
    void onTwitterClick();
    void onInstagramClick();
    void onAlreadyRegisteredClick();
    void onRestoreInstanceState(Bundle state);
    void onSaveInstanceState(Bundle state);
    void onForgotPasswordClick();
}
