package com.svyd.wodes.presentation.global;

/**
 * Created by Svyd on 05.07.2016.
 */
public class Constants {
    public static final String BUNDLE_INSTAGRAM_MODEL = "insta_model";
    public static final String INSTAGRAM_PASSWORD_LINK = "https://www.instagram.com/accounts/password/reset/";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String BUNDLE_REDIRECTED_URI = "redirected_uri";
    public static final int INSTA_AUTH_ACTIVITY_REQUEST_CODE = 0;
    public static final int TRANSITION_DURATION = 250;

    public static final String ITEM_TYPE_MY = "my";
    public static final String ITEM_TYPE_ALL = "all";

    public static final String DIALOG_TYPE_PENDING = "pending";
    public static final String DIALOG_TYPE_INVITE = "invite";
    public static final String DIALOG_TYPE_ALL = "all";

    public static final String AUTH_LOGOUT = "auth_logout";

    public class State {
        public static final String WAITING_FOR_SENDER_PAYMENT = "WAITING_FOR_SENDER_PAYMENT";
        public static final String WAITING_FOR_CARRIER_APPROVAL = "WAITING_FOR_CARRIER_APPROVAL";
    }

    public class Extra {
        public static final String EXTRA_SERIALIZABLE = "extra_serializable";
        public static final String EXTRA_PROFILE = "extra_profile";
        public static final String EXTRA_STRING = "string";
        public static final String EXTRA_DELIVERY = "delivery";
        public static final String EXTRA_PARCEL = "PARCEL";
        public static final String EXTRA_POSITION = "position";
        public static final String EXTRA_OPTIONS = "options";
        public static final String EXTRA_REQUEST = "request";
        public static final String EXTRA_INVITE_REQUEST = "invite_request";

        public static final int EXTRA_OPTION_UPDATE = 0;
    }

    public class Gender {
        public static final String GENDER_MALE = "male";
        public static final String GENDER_FEMALE = "female";
    }

    public class Flow {
        public static final String FLOW_KEY = "flow";

        public static final int PROFILE_CURRENT = 120;
        public static final int PROFILE_NOT_CURRENT = 121;
        public static final int AUTH_SIGN_UP = 122;
        public static final int AUTH_ADD_NUMBER = 124;
        public static final int EDIT_VERIFY_NUMBER = 125;
        public static final int CREATE_ITEM = 126;
        public static final int EDIT_ITEM = 127;
        public static final int DETAILS_MY = 128;
        public static final int DETAILS_ALL = 129;
        public static final int CHOOSE_ITEM = 130;
        public static final int SHOW_ITEMS = 131;
        public static final int DIALOG_ALL = 132;
        public static final int DIALOG_PENDING = 133;
        public static final int DIALOG_INVITES = 134;
        public static final int DIALOG_CARRIER_INVITE = 135;
    }

    public class ProfileItems {
        public static final String INVITES = "invites";
        public static final String PENDING = "pending";
        public static final String DELIVERIES = "deliveries";
        public static final String PARCELS = "parcels";
        public static final String NUMBER = "number";
        public static final String EMAIL = "email";
        public static final String GENDER = "gender";
        public static final String BIRTHDAY = "birthday";
        public static final String LOGOUT = "logout";
        public static final String CREATED_AT = "created_at";
    }

    public class Credentials {
        public static final String REDIRECT_URI = "http://localhost:8846/api/users/auth/instagram/callback";

        //instagram credentials
        public static final String INSTAGRAM_CLIENT_ID = "c0227b90ea154460aac4f0c3e8322e1e";
        public static final String INSTAGRAM_AUTH_URL = "https://api.instagram.com/oauth/authorize/";
    }

    public class AuthType {
        public static final String INSTAGRAM = "instagram";
        public static final String FACEBOOK = "facebook";
        public static final String TWITTER = "twitter";
    }

    public class Role {
        public static final String CARRIER = "carrier";
        public static final String SENDER = "sender";
    }
}
