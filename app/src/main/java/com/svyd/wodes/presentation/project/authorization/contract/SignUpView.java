package com.svyd.wodes.presentation.project.authorization.contract;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.AuthorizationStrategy;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

/**
 * Created by Svyd on 17.07.2016.
 */
public interface SignUpView extends BaseView {
    void setEmailError(String error);
    void setPasswordError(String error);
    void setConfirmError(String error);
    void setNameError(String error);

    void showEmailProgress(boolean show);
    void showNameProgress(boolean show);
    void showPasswordProgress(boolean show);
    void showConfirmProgress(boolean show);

    void setEmailChecked(boolean checked);
    void setNameChecked(boolean checked);
    void setPasswordChecked(boolean checked);
    void setConfirmChecked(boolean checked);

    void setConfirmEnabled(boolean enabled);

    void passActivitySignUp(SignUpRequest request);

    void setDelegateStrategy(AuthorizationStrategy strategy);
    void logIn(AuthorizationStrategy.OauthCallback callback);
    void showToast(String message);
    void dismissKeyboard();
    void showSignUpFields(boolean show);
    void showSignUpControls(boolean show);
    void setButtonTitle(String title);
    void navigateToForgotPassword();

    void animateLayoutChanges();

    void SignUp(ProfileInfo info);
    void addPhone(ProfileInfo info);
    AuthRepository provideScopedRepository();
}
