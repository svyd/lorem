package com.svyd.wodes.presentation.project.authorization.contract;

import android.os.Bundle;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;

/**
 * Created by Svyd on 08.08.2016.
 */
public interface IPhoneNumberPresenter extends BasePresenter {
    void setArguments(Bundle args);
    void onNextClick();
    void onTextChanged(String text);
    void onRestoreInstanceState(Bundle state);
    void onSaveInstanceState(Bundle state);
}
