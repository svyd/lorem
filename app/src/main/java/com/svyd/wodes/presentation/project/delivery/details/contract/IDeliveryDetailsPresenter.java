package com.svyd.wodes.presentation.project.delivery.details.contract;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;

/**
 * Created by Svyd on 11.09.2016.
 */
public interface IDeliveryDetailsPresenter extends BasePresenter {
    void onMapLoaded();
    void onRouteDrawn();
    void onCameraIdle();
    void onEdit();
    void handleResult(int requestCode, int resultCode, Intent data);
    void onOwnerClick();
    void onCreateOptionsMenu(MenuInflater menuInflater, Menu menu);
    void onButtonClick();
    void onAccept();
}
