package com.svyd.wodes.presentation.project.authorization.presenter.impl;

import android.os.Bundle;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.tool.PatternValidator;
import com.svyd.wodes.presentation.project.authorization.contract.ISignUpPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.SignUpView;
import com.svyd.wodes.presentation.project.authorization.model.OauthRequest;
import com.svyd.wodes.presentation.project.authorization.model.SignInRequest;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.AuthStrategyFactory;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.AuthorizationStrategy;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import java.util.concurrent.TimeUnit;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;

/**
 * Created by Svyd on 18.07.2016.
 */
@SuppressWarnings("FieldCanBeLocal")
public class SignUpPresenterImpl implements ISignUpPresenter, AuthorizationStrategy.OauthCallback {

    private final int MODE_SIGN_UP = 0;
    private final int MODE_SIGN_IN = 1;

    private final int DEBOUNCE = 500;
    private final String SIGN_IN_REQUEST_STATE = "sign_in_request_state";
    private final String SIGN_UP_REQUEST_STATE = "sign_up_request_state";

    private SignUpView mView;

    private SignUpRequest mSignUpRequest;
    private SignInRequest mSignInRequest;

    private PublishSubject<String> mEmailSubject;
    private PublishSubject<String> mPasswordSubject;
    private PublishSubject<String> mConfirmSubject;
    private PublishSubject<String> mNameSubject;

    private AuthStrategyFactory mStrategyFactory;
    private PostInteractor<SignInRequest> mSignInInteractor;
    private PostInteractor<OauthRequest> mOauthInteractor;

    private int mMode = 0;

    public SignUpPresenterImpl(SignUpView view,
                               PostInteractor<SignInRequest> interactor,
                               PostInteractor<OauthRequest> oauthInteractor) {
        mView = view;
        mSignInInteractor = interactor;
        mOauthInteractor = oauthInteractor;
    }

    @Override
    public void onEmailChanged(String email) {
        mView.setEmailError(null);
        if (!email.equals("")) {
            mView.showEmailProgress(true);
            mView.setEmailChecked(false);
            mEmailSubject.onNext(email);
        } else {
            mView.showEmailProgress(false);
            mView.setEmailChecked(false);
        }
    }

    @Override
    public void onFullNameChanged(String fullName) {
        mView.setNameError(null);
        if (!fullName.equals("")) {
            mView.showNameProgress(true);
            mView.setNameChecked(false);
            mNameSubject.onNext(fullName);
        } else {
            mView.showNameProgress(false);
            mView.setNameChecked(false);
        }
    }

    @Override
    public void onPasswordChanged(String password) {
        mView.setPasswordError(null);
        if (!password.equals("")) {
            mView.showPasswordProgress(true);
            mView.setPasswordChecked(false);
            mPasswordSubject.onNext(password);
        } else {
            mView.showPasswordProgress(false);
            mView.setPasswordChecked(false);
        }
    }

    @Override
    public void onConfirmPasswordChanged(String confirm) {
        mView.setConfirmError(null);
        if (!confirm.equals("")) {
            mView.showConfirmProgress(true);
            mConfirmSubject.onNext(confirm);
        } else {
            mView.showConfirmProgress(false);
            mView.setConfirmChecked(false);
        }
    }

    @Override
    public void onFacebookClick() {
        mView.setDelegateStrategy(mStrategyFactory.provideFacebookStrategy());
        mView.logIn(this);
    }

    @Override
    public void onTwitterClick() {
        mView.setDelegateStrategy(mStrategyFactory.provideTwitterStrategy());
        mView.logIn(this);
    }

    @Override
    public void onInstagramClick() {
        mView.setDelegateStrategy(mStrategyFactory.provideInstagramStrategy());
        mView.logIn(this);
    }


    // TODO: 23.08.2016 Refactor!
    @Override
    public void onSignUpClick() {
        if (modeSignUp()) {

            if (validateSignUpRequest()) {
                mView.dismissKeyboard();
                mView.passActivitySignUp(mSignUpRequest);
            } else {
                mView.showToast(WodesApplication.getStringResource(R.string.invalid_inputs));
            }
        } else {
            if (validateSignInRequest()) {
                mView.showProgress();
                mSignInInteractor.execute(mSignInRequest, new SignInObserver());
            } else {
                mView.showToast(WodesApplication.getStringResource(R.string.invalid_inputs));
            }
        }
    }

    private boolean validateSignInRequest() {
        return mSignInRequest.getPassword() != null
                && mSignInRequest.getEmail() != null;
    }

    private boolean modeSignUp() {
        return mMode == MODE_SIGN_UP;
    }

    private void swapMode() {
        if (modeSignUp()) {
            mMode = MODE_SIGN_IN;
        } else {
            mMode = MODE_SIGN_UP;
        }
    }

    private void swapBtnTitle() {
        if (modeSignUp()) {
            mView.setButtonTitle(WodesApplication.getStringResource(R.string.btn_txt_signup));
        } else {
            mView.setButtonTitle(WodesApplication.getStringResource(R.string.btn_txt_signin));
        }
    }

    void applyMode() {
        swapBtnTitle();
        mView.showSignUpFields(modeSignUp());
        mView.showSignUpControls(modeSignUp());
    }

    @Override
    public void onAlreadyRegisteredClick() {
        swapMode();
        applyMode();
    }

    @Override
    public void onRestoreInstanceState(Bundle state) {
        if (state != null) {
            mSignUpRequest = (SignUpRequest) state.getSerializable(SIGN_IN_REQUEST_STATE);
            mSignInRequest = (SignInRequest) state.getSerializable(SIGN_UP_REQUEST_STATE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        state.putSerializable(SIGN_IN_REQUEST_STATE, mSignUpRequest);
        state.putSerializable(SIGN_UP_REQUEST_STATE, mSignInRequest);
    }

    @Override
    public void onForgotPasswordClick() {
        mView.dismissKeyboard();
        mView.navigateToForgotPassword();
    }

    @Override
    public void initialize() {
        initSubjects();
        initAuth();
        mSignUpRequest = new SignUpRequest();
        mSignInRequest = new SignInRequest();
    }

    private void initAuth() {
        mStrategyFactory = new AuthStrategyFactory();
    }

    private void initSubjects() {
        mEmailSubject = PublishSubject.create();
        mPasswordSubject = PublishSubject.create();
        mConfirmSubject = PublishSubject.create();
        mNameSubject = PublishSubject.create();

        mEmailSubject
                .debounce(DEBOUNCE, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::validateEmail);

        mPasswordSubject
                .debounce(DEBOUNCE, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::validatePassword);

        mConfirmSubject
                .debounce(DEBOUNCE, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::validateConfirm);

        mNameSubject
                .debounce(DEBOUNCE, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::validateName);
    }

    private void validateEmail(String s) {
        if (!PatternValidator.isEmailValid(s)) {
            mView.setEmailError(WodesApplication.getContext().getString(R.string.error_invalid_email));
            mView.setEmailChecked(false);
        } else {
            mSignUpRequest.setEmail(s);
            mSignInRequest.setEmail(s);
            mView.setEmailChecked(true);
        }
        mView.showEmailProgress(false);
    }

    private void validateName(String s) {
        if (!(s != null && !s.isEmpty())) {
            mView.setNameError(WodesApplication.getContext().getString(R.string.error_invalid_name));
            mView.setNameChecked(false);
        } else {
            mSignUpRequest.setName(s);
            mView.setNameChecked(true);
        }
        mView.showNameProgress(false);
    }

    private void validatePassword(String s) {

        if (!PatternValidator.isPasswordValid(s)) {
            mView.setPasswordError(WodesApplication.getContext().getString(R.string.error_invalid_password));
            mView.setPasswordChecked(false);
            mView.setConfirmEnabled(false);
        } else {
            mView.setPasswordChecked(true);
            mView.setConfirmEnabled(true);
            mSignUpRequest.setPassword(s);
            mSignInRequest.setPassword(s);
        }

        if (mSignUpRequest.getConfirm() != null) {
            validateConfirm(mSignUpRequest.getConfirm());
        }

        mView.showPasswordProgress(false);
    }

    private void validateConfirm(String s) {
        mSignUpRequest.setConfirm(s);

        if (mSignUpRequest.getPassword() != null && !mSignUpRequest.getPassword().equals(s)) {
            mView.setConfirmError(WodesApplication.getContext().getString(R.string.error_invalid_confirm));
            mView.setConfirmChecked(false);
        } else {
            mView.setConfirmError(null);
            mView.setConfirmChecked(true);
        }
        mView.showConfirmProgress(false);
    }

    private boolean validateSignUpRequest() {
        return mSignUpRequest.getPassword() != null
                && mSignUpRequest.getConfirm() != null
                && mSignUpRequest.getEmail() != null
                && mSignUpRequest.getName() != null;
    }

    @Override
    public void onStart() {
        applyMode();
        mView.animateLayoutChanges();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onSuccess(OauthRequest request) {
        request.setProvider("APPLE");
        request.setId(System.currentTimeMillis() + "");
        request.setDeviceToken(System.currentTimeMillis() + "");
        mView.showProgress();
        mOauthInteractor.execute(request, new OauthObserver());
    }

    @Override
    public void onError(Throwable error) {

    }

    private void handleError(Throwable e) {
        if (e instanceof HttpException) {
            if (((HttpException) e).code() == 400) {
                mView.showToast(WodesApplication.getStringResource(R.string.txt_login_error));
            } else {
                mView.showToast(WodesApplication.getStringResource(R.string.txt_unknown_error));
            }
        } else {
            mView.showToast(WodesApplication.getStringResource(R.string.txt_network_error));
        }
    }

    private class OauthObserver implements Observer<ProfileInfo> {
        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            mView.hideProgress();
            mView.showToast(WodesApplication.getStringResource(R.string.txt_network_error));
        }

        @Override
        public void onNext(ProfileInfo info) {
            mView.hideProgress();
            if (info.getPhone() != null) {
                mView.SignUp(info);
            } else {
                mView.addPhone(info);
            }
        }
    }

    private class SignInObserver implements Observer<ProfileInfo> {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            handleError(e);
        }

        @Override
        public void onNext(ProfileInfo info) {
            mView.hideProgress();
            mView.SignUp(info);
        }
    }
}
