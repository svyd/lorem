package com.svyd.wodes.presentation.project.parcel.create.presenter.impl;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.Interactor;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.tool.DateTimeUtility;
import com.svyd.wodes.presentation.project.delivery.model.PredictionsModel;
import com.svyd.wodes.presentation.project.delivery.model.SizeModel;
import com.svyd.wodes.presentation.project.parcel.create.contract.ICreateParcelPresenter;
import com.svyd.wodes.presentation.project.parcel.create.contract.ICreateParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelRequest;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observer;
import rx.subjects.PublishSubject;

/**
 * Created by Svyd on 20.09.2016.
 */
public abstract class AbstractCreateParcelPresenter implements ICreateParcelPresenter {

    private ICreateParcelView mView;
    private PostInteractor<String> mPlacesInteractor;
    private PostInteractor<ParcelRequest> mRequestInteractor;
    private Interactor mSizeInteractor;
    private List<SizeModel> mSizes;
    private List<String> mSizeNames;
    private PredictionsModel mDeparturePredictions;
    private PredictionsModel mArrivalPredictions;
    private List<String> mDeparturePredictionNames;
    private List<String> mArrivalPredictionNames;
    private ParcelRequest mRequest;

    private PublishSubject<String> mDepartureSubject;
    private PublishSubject<String> mArrivalSubject;

    public AbstractCreateParcelPresenter(ICreateParcelView view,
                                         PostInteractor<String> placesInteractor,
                                         PostInteractor<ParcelRequest> requestInteractor,
                                         Interactor sizeInteractor) {
        mView = view;
        mPlacesInteractor = placesInteractor;
        mRequestInteractor = requestInteractor;
        mSizeInteractor = sizeInteractor;
        mRequest = new ParcelRequest();

        initSubjects();
    }

    protected ICreateParcelView getView() {
        return mView;
    }

    protected ParcelRequest getRequest() {
        return mRequest;
    }

    private void initSubjects() {
        mDepartureSubject = PublishSubject.create();
        mArrivalSubject = PublishSubject.create();

        mDepartureSubject.debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(this::completeDeparture);

        mArrivalSubject.debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(this::completeArrival);
    }

    private void completeDeparture(String string) {
        mPlacesInteractor.execute(string, new DepartureLocationObserver());
    }

    private void completeArrival(String string) {
        mPlacesInteractor.execute(string, new ArrivalLocationObserver());
    }

    @Override
    public void onDepartureTextChanged(String text) {
        if (text.length() == 0) {
            return;
        }
        mDepartureSubject.onNext(text);
        mView.showDepartureProgress(true);
    }

    @Override
    public void onArrivalTextChanged(String text) {
        if (text.length() == 0) {
            return;
        }
        mArrivalSubject.onNext(text);
        mView.showArrivalProgress(true);
    }

    @Override
    public void onDepartureItemSelected(int position) {
        mRequest.setReferenceFrom(mDeparturePredictions.predictions[position].reference);
        mView.setDeparture(mDeparturePredictionNames.get(position));
    }

    @Override
    public void onArrivalItemSelected(int position) {
        mRequest.setReferenceTo(mArrivalPredictions.predictions[position].reference);
        mView.setArrival(mArrivalPredictionNames.get(position));
    }

    @Override
    public void onDateClick() {
        DateTime dateTime = DateTime.now().withTimeAtStartOfDay();
        mView.showDatePicker(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth());
    }

    @Override
    public void onSizeClick() {
        if (mSizeNames != null) {
            mView.showSizePicker(mSizeNames);
        } else {
            mView.showSizesProgress(true);
            mSizeInteractor.execute(new SizeObserver());
        }
    }

    @Override
    public void onTitleTextChanged(String text) {
        mRequest.setTitle(text);
    }

    @Override
    public void onDepartureDateChanged(Calendar calendar) {
        mRequest.setStartDate(DateTimeUtility.toIso(calendar));
        mView.setDate(DateTimeUtility.formatDate(mRequest.getStartDate(), true));
    }

    @Override
    public void onSizeSelected(int position) {
        mRequest.setSize(mSizes.get(position).getId());
        mView.setSize(mSizes.get(position).getName());
    }

    @Override
    public void onDescriptionTextChanged(String text) {
        mRequest.setDescription(text);
    }

    @Override
    public void onPriceTextChanged(String text) {
        mRequest.setPrice(text);
    }

    @Override
    public void onPhotoAttached(String photo) {
        mRequest.setPhoto(photo);
        mView.setPhotoAttached(WodesApplication.getStringResource(R.string.photo_attached));
    }

    @Override
    public void onNextClick() {
        if (validateInputs()) {
            mView.showPublishAlert();
        } else {
            mView.showError(WodesApplication.getStringResource(R.string.invalid_inputs));
        }
    }

    @Override
    public void onAccept() {
        mView.showProgress();
        mRequestInteractor.execute(mRequest, new CreateParcelObserver());
    }

    @Override
    public void onPhotoClick() {
        mView.showImagePicker();
    }

    private boolean validateInputs() {
        return mRequest.getReferenceFrom() != null
                && mRequest.getReferenceTo() != null
                && mRequest.getTitle() != null
                && mRequest.getSize() != null
                && mRequest.getPrice() != null
                && mRequest.getDescription() != null;
    }

    @Override
    public void initialize() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mPlacesInteractor.unSubscribe();
        mRequestInteractor.unSubscribe();
        mSizeInteractor.unSubscribe();
    }

    private void handleSizes(List<SizeModel> sizeModels) {
        mSizes = sizeModels;
        mSizeNames = new ArrayList<>();
        for (SizeModel size : mSizes) {
            mSizeNames.add(size.getName());
        }
        mView.showSizePicker(mSizeNames);
    }

    private void handleDeparturePredictions(PredictionsModel predictions) {
        mDeparturePredictions = predictions;
        mDeparturePredictionNames = new ArrayList<>();
        for (PredictionsModel.Prediction prediction : predictions.predictions) {
            mDeparturePredictionNames.add(prediction.description);
        }
        mView.showDeparturePopup(mDeparturePredictionNames);
    }

    private void handleArrivalPredictions(PredictionsModel predictions) {
        mArrivalPredictions = predictions;
        mArrivalPredictionNames = new ArrayList<>();
        for (PredictionsModel.Prediction prediction : predictions.predictions) {
            mArrivalPredictionNames.add(prediction.description);
        }
        mView.showArrivalPopup(mArrivalPredictionNames);
    }

    private class DepartureLocationObserver implements Observer<PredictionsModel> {

        @Override
        public void onCompleted() {
            mView.showDepartureProgress(false);
        }

        @Override
        public void onError(Throwable e) {
            mView.showDepartureProgress(false);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            handleDeparturePredictions(predictionsModel);
        }
    }

    private class ArrivalLocationObserver implements Observer<PredictionsModel> {

        @Override
        public void onCompleted() {
            mView.showArrivalProgress(false);
        }

        @Override
        public void onError(Throwable e) {
            mView.showArrivalProgress(false);
        }

        @Override
        public void onNext(PredictionsModel predictionsModel) {
            handleArrivalPredictions(predictionsModel);
        }
    }

    private class CreateParcelObserver implements Observer<ParcelItem> {

        @Override
        public void onCompleted() {
            mView.hideProgress();
        }

        @Override
        public void onError(Throwable e) {
            mView.hideProgress();
            mView.showError(WodesApplication.getStringResource(R.string.err_no_city));
        }

        @Override
        public void onNext(ParcelItem deliveryItem) {
            mView.navigateBackWithResult(deliveryItem);
        }
    }

    private class SizeObserver implements Observer<List<SizeModel>> {

        @Override
        public void onCompleted() {
            mView.showSizesProgress(false);
        }

        @Override
        public void onError(Throwable e) {
            mView.showSizesProgress(false);
        }

        @Override
        public void onNext(List<SizeModel> sizeModels) {
            handleSizes(sizeModels);
        }

    }
}
