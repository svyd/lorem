package com.svyd.wodes.presentation.project.parcel.list.contract;

import android.view.View;

import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import java.util.List;

/**
 * Created by Svyd on 05.07.2016.
 */
public interface IParcelView extends BaseView {
    void setToolbarTitle(String title);
    void setItems(List<ParcelItem> items);
    void navigateToDetails(int index, View view, ParcelItem item);
    void navigateToCreate();
    void notifyChanges();
    void enableLoadMore();
    void showStubText();
    void hideStubText();
    void addItem(ParcelItem item);
    void editItem(int position, ParcelItem item);
    void showFab();
    void hideFab();
    void showListProgress();
    void hideListProgress();
    void showAcceptDialog();
    void navigateHome();
}
