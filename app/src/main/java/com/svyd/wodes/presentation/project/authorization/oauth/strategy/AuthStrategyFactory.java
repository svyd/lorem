package com.svyd.wodes.presentation.project.authorization.oauth.strategy;

import com.facebook.CallbackManager;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.networks.FacebookStrategy;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.networks.InstagramStrategy;
import com.svyd.wodes.presentation.project.authorization.oauth.strategy.networks.TwitterStrategy;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

/**
 * Created by Svyd on 18.07.2016.
 */
public class AuthStrategyFactory {

    public AuthorizationStrategy provideFacebookStrategy() {
        return new FacebookStrategy(CallbackManager.Factory.create());
    }

    public AuthorizationStrategy provideTwitterStrategy() {
        return new TwitterStrategy(new TwitterAuthClient());
    }

    public AuthorizationStrategy provideInstagramStrategy() {
        return new InstagramStrategy();
    }

}
