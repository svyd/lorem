package com.svyd.wodes.presentation.project.delivery.details;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.svyd.wodes.R;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.presentation.base.tool.CircularTransform;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.create.CreateDeliveryActivity;
import com.svyd.wodes.presentation.project.delivery.details.contract.IDeliveryDetailsPresenter;
import com.svyd.wodes.presentation.project.delivery.details.contract.IDeliveryDetailsView;
import com.svyd.wodes.presentation.project.delivery.details.presenter.DeliveryDetailsPresenterFactory;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.parcel.list.my.view.MyParcelActivity;
import com.svyd.wodes.presentation.project.parcel.matched.view.MatchedParcelActivity;
import com.svyd.wodes.presentation.project.profile.ProfileActivity;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by Svyd on 10.09.2016.
 */
public class DeliveryDetailsActivity extends BaseActivity implements IDeliveryDetailsView, OnCameraIdleListener, OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    public static final int REQUEST_CODE_EDIT = 32153;
    public static final int REQUEST_CODE_VIEW = 32154;

    private TextView mToolbarTextView;

    @BindView(R.id.toolbar_ADD)
    Toolbar mToolbar;

    @BindView(R.id.appbar_ADD)
    AppBarLayout appbar;

    @BindView(R.id.rlHeaderContainer_ADD)
    RelativeLayout rlHeaderContainer;

    @BindView(R.id.flMap_ADD)
    FrameLayout flMap;

    @BindView(R.id.ivMapIcon_ADD)
    ImageView ivMapIcon;

    @BindView(R.id.pbMap_ADD)
    SmoothProgressBar pbMap;

    @BindView(R.id.tvStart_ADD)
    TextView tvStart;

    @BindView(R.id.tvDeviation_ADD)
    TextView tvDeviation;

    @BindView(R.id.tvDepartureDate_ADD)
    TextView tvDepartureDate;

    @BindView(R.id.tvDepartureCity_ADD)
    TextView tvDepartureCity;

    @BindView(R.id.tvArrivalDate_ADD)
    TextView tvArrivalDate;

    @BindView(R.id.tvArrivalCity_ADD)
    TextView tvArrivalCity;

    @BindView(R.id.tvAvailable_ADD)
    TextView tvAvailable;

    @BindView(R.id.tvDescription_ADD)
    TextView tvDescription;

    @BindView(R.id.tvOwnerName_ADD)
    TextView tvOwnerName;

    @BindView(R.id.tvVisits_ADD)
    TextView tvVisits;

    @BindView(R.id.tvDeliveredParcels_ADD)
    TextView tvDeliveredParcels;

    @BindView(R.id.tvLastVisit_ADD)
    TextView tvLastVisit;

    @BindView(R.id.tvUserSince_ADD)
    TextView tvUserSince;

    @BindView(R.id.btnContact_ADD)
    Button btnContact;

    @BindView(R.id.rlOwnerDetailsContainer_ADD)
    RelativeLayout rlOwnerDetailsContainer;

    @BindView(R.id.ivAvatar_ADD)
    ImageView ivAvatar;
    //region init

    private GoogleMap mMap;
    private IDeliveryDetailsPresenter mPresenter;
    private CircularTransform mTransformation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_delivery_details);
        setTitle("Delivery Details");
        ButterKnife.bind(this);
        enableBackButton(true);

        initHeader();
        addMapFragment();
        pbMap.progressiveStart();
        initPresenter();
        initAnimation();

        mTransformation = new CircularTransform(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.handleResult(requestCode, resultCode, data);
    }

    private void initAnimation() {
        mAnimator = new PathAnimator();
        mHandler = new Handler();
    }

    private void initPresenter() {
        mPresenter = new DeliveryDetailsPresenterFactory().providePresenter(getIntent().getExtras(), this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacks(mAnimator);
    }

    private void initHeader() {
        mToolbarTextView = getTitleTextView(mToolbar);
        appbar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float percentage = ((float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange());
            rlHeaderContainer.setAlpha(Math.abs(percentage - 1));
            mToolbarTextView.setScaleX(percentage);
            mToolbarTextView.setScaleY(percentage);
            getWindow().setStatusBarColor(Color.argb((int) (percentage * 255), 68, 138, 255));
            float min = 0.88f;
            float max = 1;
            if (percentage > min) {
                mToolbarTextView.setAlpha(Math.abs(percentage - min) * (max / (max - min)));
            } else {
                mToolbarTextView.setAlpha(0);
            }
        });
    }

    private void addMapFragment() {
        SupportMapFragment fragment = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.add(R.id.flMap_ADD, fragment);
        fragmentTransaction.commit();
        fragment.getMapAsync(this);
    }

    @Override
    public int getContainerId() {
        return 0;
    }

    @Override
    public int getToolbarId() {
        return R.id.toolbar_ADD;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();
        mPresenter.initialize();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mPresenter.onCreateOptionsMenu(getMenuInflater(), menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.miEdit_DE) {
            mPresenter.onEdit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpMap() {
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.setOnMapLoadedCallback(this);
    }

    @Override
    public void drawRoute(List<LatLng> points) {
        mCoordinates = points;
        mAnimator.initialize();
        mHandler.post(mAnimator);
    }

    @Override
    public void setBounds(LatLngBounds bounds) {
        float padding = getResources().getDimension(R.dimen.margin_double);
        CameraUpdate update = CameraUpdateFactory.newLatLngBounds(bounds, (int) padding);
        mMap.animateCamera(update);
        mMap.setOnCameraIdleListener(this);
//        mMap.setOnMapLoadedCallback(() -> mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, (int) padding)));
    }

    @Override
    public void addMarker(MarkerOptions options) {
        mMap.addMarker(options);
    }

    @Override
    public void showMap() {
        ivMapIcon.setVisibility(View.GONE);
        pbMap.progressiveStop();
    }

    @Override
    public void setStart(String text) {
        tvStart.setText(text);
    }

    @Override
    public void setDeviation(String text) {
        tvDeviation.setText(text);
    }

    @Override
    public void setDepartureDate(String text) {
        tvDepartureDate.setText(text);
    }

    @Override
    public void setDepartureCity(String text) {
        tvDepartureCity.setText(text);
    }

    @Override
    public void setArrivalDate(String text) {
        tvArrivalDate.setText(text);
    }

    @Override
    public void setArrivalCity(String text) {
        tvArrivalCity.setText(text);
    }

    @Override
    public void setAvailable(String text) {
        tvAvailable.setText(text);
    }

    @Override
    public void setDescription(String text) {
        tvDescription.setText(text);
    }

    @Override
    public void setOwnerName(String text) {
        tvOwnerName.setText(text);
    }

    @Override
    public void setVisits(String text) {
        tvVisits.setText(text);
    }

    @Override
    public void setDeliveredParcels(String text) {
        tvDeliveredParcels.setText(text);
    }

    @Override
    public void setLastVisit(String text) {
        tvLastVisit.setText(text);
    }

    @Override
    public void setUserSince(String text) {
        tvUserSince.setText(text);
    }

    @Override
    public void setAvatar(String text) {
        Glide.with(this)
                .load(text)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .centerCrop()
                .transform(mTransformation)
                .animate(android.R.anim.fade_in)
                .into(ivAvatar);
    }

    @Override
    public void navigateToEdit(DeliveryItem item) {
        Intent intent = new Intent(DeliveryDetailsActivity.this, CreateDeliveryActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_DELIVERY, item);
        intent.putExtra(Constants.Flow.FLOW_KEY, Constants.Flow.EDIT_ITEM);
        if (checkLollipop()) {

            TypedValue tv = new TypedValue();
            int toolbarHeight = 0;
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                toolbarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
            }
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int width = displaymetrics.widthPixels;
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra("x", width - 36);
            intent.putExtra("y", toolbarHeight);
        }

        startActivityForResult(intent, CreateDeliveryActivity.REQUEST_EDIT_DELIVERY);
    }

    @Override
    public void setButtonText(String text) {
        btnContact.setText(text);
    }

    @Override
    public void showOwner(boolean show) {
        rlOwnerDetailsContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void navigateToOwner(ProfileInfo owner) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_PROFILE, owner);
        startActivity(intent);
    }

    @Override
    public void setResult(DeliveryItem item) {
        Intent intent = new Intent();
        int position = getIntent().getIntExtra(Constants.Extra.EXTRA_POSITION, -1);
        intent.putExtra(Constants.Extra.EXTRA_POSITION, position);
        intent.putExtra(Constants.Extra.EXTRA_DELIVERY, item);
        setResult(RESULT_OK, intent);
    }

    @Override
    public void setViewResult() {
        Intent intent = new Intent();
        int position = getIntent().getIntExtra(Constants.Extra.EXTRA_POSITION, -1);
        intent.putExtra(Constants.Extra.EXTRA_POSITION, position);

        setResult(RESULT_OK, intent);
    }

    @Override
    public void navigateToMatchedParcels(SearchRequest request, InviteRequest inviteRequest) {
        Intent intent = new Intent(DeliveryDetailsActivity.this, MatchedParcelActivity.class);
        intent.putExtra(Constants.Extra.EXTRA_REQUEST, request);
        intent.putExtra(Constants.Extra.EXTRA_INVITE_REQUEST, inviteRequest);
        startActivity(intent);
    }

    @Override
    public void showAlert() {
        showAcceptDialog();
    }

    @Override
    public void navigateToMyParcels(InviteRequest request) {
        Intent intent = new Intent(DeliveryDetailsActivity.this, MyParcelActivity.class);
        intent.putExtra(Constants.Flow.FLOW_KEY, Constants.Flow.CHOOSE_ITEM);
        intent.putExtra(Constants.Extra.EXTRA_INVITE_REQUEST, request);
        startActivity(intent);
    }

    @OnClick(R.id.rlOwnerContainer_ADD)
    void onOwnerClick() {
        mPresenter.onOwnerClick();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onMapLoaded() {
        mPresenter.onMapLoaded();
    }

    @OnClick(R.id.btnContact_ADD)
    void onButtonClick() {
        mPresenter.onButtonClick();
    }

    void showAcceptDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.alert_title_choose_parcel)
                .setMessage(R.string.alert_message_choose_parcel)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, (dialog1, which) -> {
                    mPresenter.onAccept();
                    dialog1.dismiss();
                })
                .setNegativeButton(R.string.cancel,
                        (dialog, id) -> {
                            dialog.cancel();
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    //endregion

    //region animation

    private List<LatLng> mCoordinates;
    private Handler mHandler;
    private PathAnimator mAnimator;

    private float bearingBetweenLatLngs(LatLng begin, LatLng end) {
        Location beginL = convertLatLngToLocation(begin);
        Location endL = convertLatLngToLocation(end);

        return beginL.bearingTo(endL);
    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location loc = new Location("location");
        loc.setLatitude(latLng.latitude);
        loc.setLongitude(latLng.longitude);
        return loc;
    }

    @Override
    public void onCameraIdle() {
        mMap.setOnCameraIdleListener(null);
        mPresenter.onCameraIdle();
    }

    public class PathAnimator implements Runnable {

        private static final int ANIMATE_SPEED = 25;
        private static final int ANIMATE_SPEED_TURN = 1000;
        private static final int BEARING_OFFSET = 20;

        private final Interpolator interpolator = new LinearInterpolator();

        int currentIndex = 0;

        float tilt = 90;
        float zoom = 15.5f;
        boolean upward = true;

        long start = SystemClock.uptimeMillis();

        LatLng endLatLng = null;
        LatLng beginLatLng = null;


        public void reset() {
            start = SystemClock.uptimeMillis();
            currentIndex = 0;
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();

        }

        public void stop() {
            mHandler.removeCallbacks(mAnimator);

        }

        public void initialize() {
            reset();

            polyLine = initializePolyLine();

            // We first need to put the camera in the correct position for the first run (we need 2 markers for this).....
            LatLng markerPos = mCoordinates.get(0);
            LatLng secondPos = mCoordinates.get(1);

            setupCameraPositionForMovement(markerPos, secondPos);

        }

        private void setupCameraPositionForMovement(LatLng markerPos,
                                                    LatLng secondPos) {

            float bearing = bearingBetweenLatLngs(markerPos, secondPos);

            CameraPosition cameraPosition =
                    new CameraPosition.Builder()
                            .target(markerPos)
                            .bearing(bearing + BEARING_OFFSET)
                            .tilt(90)
                            .build();

//            mMap.animateCamera(
//                    CameraUpdateFactory.newCameraPosition(cameraPosition),
//                    ANIMATE_SPEED_TURN,
//                    new GoogleMap.CancelableCallback() {
//
//                        @Override
//                        public void onFinish() {
//                            System.out.println("finished camera");
//                            mAnimator.reset();
//                            Handler handler = new Handler();
//                            handler.post(mAnimator);
//                        }
//
//                        @Override
//                        public void onCancel() {
//                            System.out.println("cancelling camera");
//                        }
//                    }
//            );
        }

        private Polyline polyLine;
        private PolylineOptions polylineOptions = new PolylineOptions();


        private Polyline initializePolyLine() {
            //polyLinePoints = new ArrayList<LatLng>();
            polylineOptions.add(mCoordinates.get(0));
            polylineOptions.color(getResources().getColor(R.color.colorPrimary));
            polylineOptions.width(getResources().getDimension(R.dimen.path_thickness));
            return mMap.addPolyline(polylineOptions);
        }

        /**
         * Add the marker to the polyline.
         */
        private void updatePolyLine(LatLng latLng) {
            List<LatLng> points = polyLine.getPoints();
            points.add(latLng);
            polyLine.setPoints(points);
        }


        public void stopAnimation() {
            mAnimator.stop();
            mPresenter.onRouteDrawn();
        }

        public void startAnimation() {
            if (mCoordinates.size() > 2) {
                mAnimator.initialize();
            }
        }


        @Override
        public void run() {

            long elapsed = SystemClock.uptimeMillis() - start;
            double t = interpolator.getInterpolation((float) elapsed / ANIMATE_SPEED);

            double lat = t * endLatLng.latitude + (1 - t) * beginLatLng.latitude;
            double lng = t * endLatLng.longitude + (1 - t) * beginLatLng.longitude;
            LatLng newPosition = new LatLng(lat, lng);

            updatePolyLine(newPosition);

            // It's not possible to move the marker + center it through a cameraposition update while another camerapostioning was already happening.
            //navigateToPoint(newPosition,tilt,bearing,currentZoom,false);
            //navigateToPoint(newPosition,false);

            if (t < 1) {
                mHandler.postDelayed(this, 16);
            } else {

                System.out.println("Move to next marker.... current = " + currentIndex + " and size = " + mCoordinates.size());
                // imagine 5 elements -  0|1|2|3|4 currentindex must be smaller than 4
                if (currentIndex < mCoordinates.size() - 4) {

                    currentIndex += 3;

                    endLatLng = getEndLatLng();
                    beginLatLng = getBeginLatLng();


                    start = SystemClock.uptimeMillis();

                    LatLng begin = getBeginLatLng();
                    LatLng end = getEndLatLng();

                    float bearingL = bearingBetweenLatLngs(begin, end);

                    CameraPosition cameraPosition =
                            new CameraPosition.Builder()
                                    .target(end) // changed this...
                                    .bearing(bearingL + BEARING_OFFSET)
                                    .tilt(tilt)
                                    .zoom(mMap.getCameraPosition().zoom)
                                    .build();


//                    mMap.animateCamera(
//                            CameraUpdateFactory.newCameraPosition(cameraPosition),
//                            ANIMATE_SPEED_TURN,
//                            null
//                    );

                    start = SystemClock.uptimeMillis();
                    mHandler.postDelayed(mAnimator, 16);

                } else {
                    currentIndex++;
                    stopAnimation();
                }

            }
        }

        private LatLng getEndLatLng() {
            return mCoordinates.get(currentIndex + 1);
        }

        private LatLng getBeginLatLng() {
            return mCoordinates.get(currentIndex);
        }
    }

    //endregion
}
