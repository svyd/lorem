package com.svyd.wodes.presentation.project.delivery.details.presenter;

import android.os.Bundle;

import com.svyd.wodes.R;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.delivery.details.contract.IDeliveryDetailsView;

import rx.Observer;

/**
 * Created by Svyd on 11.09.2016.
 */
@SuppressWarnings("Convert2streamapi")
public class AllDeliveryDetailsPresenter extends AbstractDeliveryDetailsPresenter {

    private PostInteractor<String> mViewsInteractor;


    public AllDeliveryDetailsPresenter(PostInteractor<String> interactor, IDeliveryDetailsView view, Bundle args) {
        super(view, args);
        mViewsInteractor = interactor;
    }

    @Override
    public void initialize() {
        mViewsInteractor.execute(getItem().getEntity().getId(), new UpdateViewsObserver());
        super.initialize();
        getView().setButtonText(WodesApplication.getStringResource(R.string.btn_contact_carrier));
        getView().showOwner(true);
    }

    @Override
    public void onOwnerClick() {
        getView().navigateToOwner(getItem().getEntity().getOwner());
    }

    @Override
    public void onButtonClick() {
        getView().showAlert();
    }

    @Override
    public void onAccept() {
        InviteRequest request = new InviteRequest(Constants.Role.CARRIER);
        request.setDelivery(getItem().getEntity().getId());
        getView().navigateToMyParcels(request);
    }

    private class UpdateViewsObserver implements Observer<ResponseModel> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(ResponseModel responseModel) {
            int visits = Integer.parseInt(getItem().getViews()) + 1;
            getView().setVisits(WodesApplication.getStringResource(R.string.visits, visits));
            getView().setViewResult();
        }
    }
}
