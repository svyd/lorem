package com.svyd.wodes.presentation.project.parcel.list.presenter.implementation;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.svyd.wodes.R;
import com.svyd.wodes.data.dialog.model.DialogEntity;
import com.svyd.wodes.data.dialog.model.InviteRequest;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.parcel.list.contract.IParcelView;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import rx.Observer;

/**
 * Created by Svyd on 23.09.2016.
 */
public class ChooseParcelPresenter extends MyParcelPresenter {

    private static final String TAG = ChooseParcelPresenter.class.getSimpleName();
    private PostInteractor<InviteRequest> mInviteInteractor;
    private InviteRequest mRequest;

    public ChooseParcelPresenter(IParcelView view,
                                 Bundle args,
                                 PostInteractor<ListRequest> interactor,
                                 PostInteractor<SearchRequest> searchInteractor,
                                 PostInteractor<InviteRequest> inviteInteractor) {
        super(view, interactor, searchInteractor);
        mInviteInteractor = inviteInteractor;
        initRequest(args);
    }

    private void initRequest(Bundle args) {
        mRequest = (InviteRequest) args.getSerializable(Constants.Extra.EXTRA_INVITE_REQUEST);
    }

    @Override
    public void initialize() {
        super.initialize();
        getView().setToolbarTitle(WodesApplication.getStringResource(R.string.title_choose_parcel));
    }

    @Override
    public void onCreateOptionsMenu(MenuInflater inflater, Menu menu) {

    }

    @Override
    public void onAccept() {
        mInviteInteractor.execute(mRequest, new InviteObserver());
    }

    @Override
    public void onItemClick(int index, ParcelItem item, View view) {
        mRequest.setParcel(item.getEntity().getId());
        getView().showAcceptDialog();
    }

    private class InviteObserver implements Observer<DialogEntity> {

        @Override
        public void onCompleted() {
            Log.d(TAG, "onCompleted() called with: " + "");
        }

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "onError() called with: " + "e = [" + e + "]");
        }

        @Override
        public void onNext(DialogEntity dialog) {
            getView().navigateHome();
            Log.d(TAG, "onNext() called with: " + "dialog = [" + dialog + "]");
        }
    }
}
