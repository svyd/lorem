package com.svyd.wodes.presentation.project.profile.contract;

import android.content.Intent;
import android.os.Bundle;

import com.svyd.wodes.presentation.base.mvp.BasePresenter;
import com.svyd.wodes.presentation.project.profile.model.ProfileItem;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface IProfilePresenter extends BasePresenter {
    void onFabClick();
    void onItemClick(int index, ProfileItem item);
    void setArguments(Bundle args);

    void handleResult(int requestCode, int resultCode, Intent data);
}
