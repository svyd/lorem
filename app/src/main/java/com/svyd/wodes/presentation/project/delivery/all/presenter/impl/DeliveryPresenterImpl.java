package com.svyd.wodes.presentation.project.delivery.all.presenter.impl;

import android.app.Activity;
import android.content.Intent;

import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.all.contract.IDeliveryPresenter;
import com.svyd.wodes.presentation.project.delivery.all.contract.IDeliveryView;
import com.svyd.wodes.presentation.project.delivery.details.DeliveryDetailsActivity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.filtering.view.FilteringActivity;

import java.util.List;

import rx.Observer;

/**
 * Created by Svyd on 24.08.2016.
 */
public class DeliveryPresenterImpl implements IDeliveryPresenter {

    private final int MODE_FILTER = 0;
    private final int MODE_ALL = 1;

    private PostInteractor<ListRequest> mInteractor;
    private PostInteractor<SearchRequest> mSearchInteractor;
    private IDeliveryView mView;
    private List<DeliveryItem> mItems;
    private ListRequest mRequest;
    private SearchRequest mSearchRequest;
    private int mMode = MODE_ALL;

    public DeliveryPresenterImpl(IDeliveryView view,
                                 PostInteractor<ListRequest> interactor,
                                 PostInteractor<SearchRequest> searchInteractor) {
        mInteractor = interactor;
        mSearchInteractor = searchInteractor;
        mView = view;
    }

    @Override
    public void onItemClick(DeliveryItem item) {

    }

    @Override
    public void handleResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case DeliveryDetailsActivity.REQUEST_CODE_VIEW:
                    onIncrementView(data);
                    break;
                case FilteringActivity.REQUEST_CODE_FILTER:
                    onFilter(data);
                    break;
            }
        }
    }

    @Override
    public void onFabClick() {
        swapMode();
        mView.hideFab();
        mItems.clear();
        mView.notifyChanges();
        mRequest.setOffset(0);
        getItems();
    }

    private void onFilter(Intent data) {
        mSearchRequest = getSearchRequest(data);
        if (mSearchRequest == null) {
            return;
        }
        swapMode();
        mView.showFab();
        mItems.clear();
        mView.notifyChanges();
        mView.showProgress();

        mSearchInteractor.execute(mSearchRequest, new DeliveryObserver());
    }

    private SearchRequest getSearchRequest(Intent data) {
        if (data != null && data.hasExtra(Constants.Extra.EXTRA_REQUEST)) {
            return (SearchRequest) data.getSerializableExtra(Constants.Extra.EXTRA_REQUEST);
        }
        return null;
    }

    private boolean isFilterMode() {
        return mMode == MODE_FILTER;
    }

    private void swapMode() {
        if (mMode == MODE_FILTER) {
            mMode = MODE_ALL;
        } else {
            mMode = MODE_FILTER;
        }
    }

    private void onIncrementView(Intent data) {
        int position = data.getIntExtra(Constants.Extra.EXTRA_POSITION, -1);
        DeliveryItem item = mItems.get(position);
        item.setViews(String.valueOf(Integer.parseInt(item.getViews()) + 1));
        mView.notifyChanges();
    }

    @Override
    public void initialize() {
        mView.setTitle("All deliveries");
        mRequest = new ListRequest();
        mRequest.setType(Constants.ITEM_TYPE_ALL);
        mRequest.setLimit(10);
        mRequest.setOffset(0);
        getItems();
    }

    private void getItems() {
        mView.showProgress();
        mInteractor.execute(mRequest, new DeliveryObserver());
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mInteractor.unSubscribe();
    }

    @Override
    public void onLoadMore() {
        mView.showListProgress();
        if (isFilterMode()) {
            mSearchRequest.incrementOffset();
            mSearchInteractor.execute(mSearchRequest, new DeliveryObserver());
        } else {
            mRequest.incrementOffset();
            mInteractor.execute(mRequest, new DeliveryObserver());
        }
    }

    void onSetItems(List<DeliveryItem> items) {
        mItems = items;
        mView.setItems(mItems);
    }

    void onAddItems(List<DeliveryItem> items) {
        mItems.addAll(items);
        mView.notifyChanges();
    }

    boolean toAdd() {
        if (isFilterMode()) {
            return mSearchRequest.getOffset() > 0;
        } else {
            return mRequest.getOffset() > 0;
        }
    }

    void doOnNext(List<DeliveryItem> items) {
        if (items.size() > 0) {
            mView.hideStubText();
            mView.enableLoadMore();
            if (toAdd()) {
                onAddItems(items);
            } else {
                onSetItems(items);
            }
        } else {
            verifyEmptyList();
        }
    }

    private void verifyEmptyList() {
        if (isFilterMode()) {
            if (mSearchRequest.getOffset() == 0) {
                mView.showStubText();
            }
        } else {
            if (mRequest.getOffset() == 0) {
                mView.showStubText();
            }
        }
    }

    private class DeliveryObserver implements Observer<List<DeliveryItem>> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            mView.hideListProgress();
            mView.enableLoadMore();
        }

        @Override
        public void onNext(List<DeliveryItem> items) {
            mView.hideListProgress();
            doOnNext(items);
        }
    }

}