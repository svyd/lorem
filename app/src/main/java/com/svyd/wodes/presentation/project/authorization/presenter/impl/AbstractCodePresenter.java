package com.svyd.wodes.presentation.project.authorization.presenter.impl;

import android.os.Bundle;
import android.util.Log;

import com.svyd.wodes.R;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.authorization.contract.ICodeVerificationPresenter;
import com.svyd.wodes.presentation.project.authorization.contract.CodeVerificationView;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;

import rx.Observer;

/**
 * Created by Svyd on 24.08.2016.
 */
public abstract class AbstractCodePresenter implements ICodeVerificationPresenter {

    private static final String REQUEST_STATE = "request_state";
    private static final String TAG = CodeSignUpPresenter.class.getSimpleName();

    private PostInteractor<PhoneWrapper> mPhoneInteractor;
    private CodeVerificationView mView;
    private SignUpRequest mRequest;

    public AbstractCodePresenter(CodeVerificationView view,
                                 PostInteractor<PhoneWrapper> phoneInteractor) {
        mView = view;
        mPhoneInteractor = phoneInteractor;
    }

    @Override
    public void initialize() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        mPhoneInteractor.unSubscribe();
    }

    @Override
    public void onCodeFilled(String code) {
        getView().setProgressTitle(WodesApplication.getStringResource(R.string.txt_sending_code));
        getView().showProgress();
        getView().dismissKeyboard();
        if (mRequest == null) {
            mRequest = new SignUpRequest();
        }
        mRequest.setCode(code);
        execute();
    }

    protected abstract void execute();

    protected CodeVerificationView getView() {
        return mView;
    }

    protected SignUpRequest getRequest() {
        return mRequest;
    }

    @Override
    public void onCodeEmpty() {

    }

    @Override
    public void onSendAgain() {
        mView.setProgressTitle(WodesApplication.getStringResource(R.string.txt_sending_phone));
        mView.showProgress();
        mPhoneInteractor.execute(new PhoneWrapper(mRequest.getNumber()), new PhoneObserver());
    }

    @Override
    public void setArguments(Bundle args) {
        if (args != null && args.containsKey(Constants.Extra.EXTRA_SERIALIZABLE)) {
            mRequest = (SignUpRequest) args.getSerializable(Constants.Extra.EXTRA_SERIALIZABLE);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle state) {
        if (state != null) {
            mRequest = (SignUpRequest) state.getSerializable(REQUEST_STATE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        state.putSerializable(REQUEST_STATE, mRequest);
    }

    private class PhoneObserver implements Observer<ResponseModel> {

        @Override
        public void onCompleted() {
            mView.clearCode();
        }

        @Override
        public void onError(Throwable e) {
            mView.showToast(WodesApplication.getStringResource(R.string.txt_network_error));
        }

        @Override
        public void onNext(ResponseModel responseModel) {
            Log.i(TAG, "onNext: " + responseModel.response);
            mView.hideProgress();
        }
    }
}