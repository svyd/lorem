package com.svyd.wodes.presentation.project.profile.presenter.implementation;

import com.svyd.wodes.presentation.project.profile.contract.ProfileView;
import com.svyd.wodes.domain.base.PostInteractor;

/**
 * Created by Svyd on 05.07.2016.
 */
public class ProfilePresenterImpl extends AbstractProfilePresenter {

    public ProfilePresenterImpl(PostInteractor<String> interactor, ProfileView view) {
        super(interactor, view);
    }

    @Override
    public void initialize() {
        getView().setToolbar();
        getView().showBackButton();
        getView().hideFab();
        getView().hideBorder();
    }

    @Override
    protected void getProfile() {
        getInteractor().execute("", new ProfileObserver());
        getView().showProgress();
    }

    @Override
    protected void onLogoutClick() {
        throw new UnsupportedOperationException();
    }
}
