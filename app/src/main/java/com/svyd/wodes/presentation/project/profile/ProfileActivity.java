package com.svyd.wodes.presentation.project.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.base.ui.BaseActivity;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.profile.view.ProfileFragment;

/**
 * Created by Svyd on 05.07.2016.
 */
public class ProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);
        initFragment();
    }

    private void initFragment() {
        Bundle args = new Bundle();
        args.putInt(Constants.Flow.FLOW_KEY, Constants.Flow.PROFILE_NOT_CURRENT);
        args.putSerializable(Constants.Extra.EXTRA_PROFILE, getIntent().getSerializableExtra(Constants.Extra.EXTRA_PROFILE));
        getFragmentNavigator().addFragmentWithoutBackStack(ProfileFragment.newInstance(args));
    }

    @Override
    public int getContainerId() {
        return R.id.llContainer_AP;
    }

    @Override
    public int getToolbarId() {
        return 0;
    }
}
