package com.svyd.wodes.presentation.project.delivery.model;

import com.svyd.wodes.data.delivery.DeliveryService;

import java.io.Serializable;

/**
 * Created by Svyd on 24.08.2016.
 */
public class DeliveryItem implements Serializable {

    private String from;
    private String to;
    private String start;
    private String available;
    private String carrier;
    private String time;
    private String views;
    private String avatar;
    private DeliveryEntity mEntity;

    public void setEntity(DeliveryEntity entity) {
        mEntity = entity;
    }

    public DeliveryEntity getEntity() {
        return mEntity;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getAvailable() {
        return available;
    }

    public String getFrom() {
        return from;
    }

    public String getCarrier() {
        return carrier;
    }

    public String getTime() {
        return time;
    }

    public String getStart() {
        return start;
    }

    public String getTo() {
        return to;
    }

    public String getViews() {
        return views;
    }

    public static class Builder {
        private String from;
        private String to;
        private String start;
        private String available;
        private String carrier;
        private String time;
        private String views;

        public Builder setStart(String start) {
            this.start = start;
            return this;
        }

        public Builder setAvailable(String available) {
            this.available = available;
            return this;
        }

        public Builder setFrom(String from) {
            this.from = from;
            return this;
        }

        public Builder setCarrier(String carrier) {
            this.carrier = carrier;
            return this;
        }

        public Builder setTime(String time) {
            this.time = time;
            return this;
        }

        public Builder setTo(String to) {
            this.to = to;
            return this;
        }

        public Builder setViews(String views) {
            this.views = views;
            return this;
        }

        public DeliveryItem build() {
            DeliveryItem item = new DeliveryItem();

            item.setAvailable(available);
            item.setFrom(from);
            item.setTo(to);
            item.setCarrier(carrier);
            item.setStart(start);
            item.setTime(time);
            item.setViews(views);

            return item;
        }
    }
}
