package com.svyd.wodes.presentation.project.profile.contract;

import com.svyd.wodes.presentation.base.mvp.BaseView;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;
import com.svyd.wodes.presentation.project.profile.model.ProfileItem;

import java.util.List;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface ProfileView extends BaseView {
    void setAvatar(String url);
    void setName(String name);
    void setRating(int level);
    void setListData(List<ProfileItem> items);
    void showToast(String toast);
    void showBackButton();
    void startEditProfile(ProfileInfo info);
    void hideFab();
    void setToolbar();
    void setExtraToolbarHeight();
    void hideBorder();
    void navigateToMyDeliveries();
    void navigateToMyParcels();
    void navigateToInvites();
    void navigateToPending();
    void logout();
}
