package com.svyd.wodes.presentation.base.tool;

/**
 * Created by Svyd on 15.09.2016.
 */
public class WordUtils {

    public static String capitalize(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }
}
