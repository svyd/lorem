package com.svyd.wodes.presentation.project.authorization.presenter.impl;

import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.authorization.contract.CodeVerificationView;
import com.svyd.wodes.presentation.project.authorization.model.CodeWrapper;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;

/**
 * Created by Svyd on 30.08.2016.
 */
public class CodeEditPresenter extends CodeAddPresenter {

    public CodeEditPresenter(CodeVerificationView view, PostInteractor<PhoneWrapper> phoneInteractor, PostInteractor<CodeWrapper> codeInteractor) {
        super(view, phoneInteractor, codeInteractor);
    }

    @Override
    public void initialize() {
        super.initialize();
        getView().setDarkTextColor();
    }

    @Override
    protected void doOnNext() {
        getView().onEditedPhoneValidated();
    }
}
