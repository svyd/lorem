package com.svyd.wodes.data.net.retrofit;

import com.svyd.wodes.data.net.global.ApiConstants;

import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

/**
 * Created by Svyd on 18.08.2016.
 */
public class WodesRetrofitWrapper {

    private static WodesRetrofitWrapper sInstance;

    private Retrofit mRetrofit;

    private WodesRetrofitWrapper(OkHttpClient client, Converter.Factory factory) {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.WODES_API_ENDPOINT)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(factory)
                .client(client)
                .build();
    }

    public static WodesRetrofitWrapper getInstance(OkHttpClient client, Converter.Factory factory) {
        if (sInstance == null) {
            sInstance = new WodesRetrofitWrapper(client, factory);
        }
        return sInstance;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

}
