package com.svyd.wodes.data.authorization;

import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.authorization.model.CodeWrapper;
import com.svyd.wodes.presentation.project.authorization.model.EmailWrapper;
import com.svyd.wodes.presentation.project.authorization.model.OauthRequest;
import com.svyd.wodes.presentation.project.authorization.model.SignInRequest;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observable;

/**
 * Created by Svyd on 18.08.2016.
 */
public interface AuthRepository {
    Observable<ResponseModel> sendCode(CodeWrapper request);
    Observable<ProfileInfo> signIn(SignInRequest request);
    Observable<ProfileInfo> signIn(OauthRequest request);
    Observable<ProfileInfo> signUp(SignUpRequest request);
    Observable<ResponseModel> sendPhoneNumber(PhoneWrapper phone);
    Observable<ResponseModel> forgotPassword(EmailWrapper email);
}
