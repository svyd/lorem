package com.svyd.wodes.data.places;

import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.net.global.ApiConstants;
import com.svyd.wodes.presentation.project.delivery.model.LocationModel;
import com.svyd.wodes.presentation.project.delivery.model.PredictionsModel;
import com.svyd.wodes.presentation.project.delivery.model.SimpleLocationModel;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 09.09.2016.
 */
public class GooglePlacesRepository implements IPlacesRepository {

    private GooglePlacesService mService;
    private TypeMapper<List<LocationModel>, List<SimpleLocationModel>> mPlacesDetailsMapper;
    private TypeMapper<LocationModel, LocationModel> mLocationMapper;

    public GooglePlacesRepository(GooglePlacesService service) {
        mService = service;
        mPlacesDetailsMapper = new PlacesDetilesMapper();
        mLocationMapper = new LocationMapper();
    }

    @Override
    public Observable<PredictionsModel> getAddressList(final String _query) {
        return mService.getPlacesPredictions(_query, "(cities)", ApiConstants.GOOGLE_PLACES_API_KEY);
    }

    @Override
    public Observable<LocationModel> getPlaceDetails(String _placeReference) {
        return mService.getPlaceDetails(_placeReference, ApiConstants.GOOGLE_PLACES_API_KEY)
                .map(mLocationMapper::map);
    }

    @Override
    public Observable<List<SimpleLocationModel>> getPlacesDetails(List<String> references) {
        return Observable.from(references)
                .flatMap(this::getPlaceDetails)
                .toList()
                .map(mPlacesDetailsMapper::map);
    }
}
