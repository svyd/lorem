package com.svyd.wodes.data.net.global;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Svyd on 18.08.2016.
 */
public final class ApiConstants {

    private ApiConstants(){

    }

    public static final String MULTIPART = "multipart/form-data";

    public static final HttpLoggingInterceptor.Level LOGGING_LEVEL = HttpLoggingInterceptor.Level.BODY;

    public static final int CONNECTION_TIME_OUT = 20;
    public static final int READ_TIME_OUT = 10;

    public static final String GOOGLE_PLACES_API_KEY = "key";
    public static final String GOOGLE_API_ENDPOINT = "https://maps.googleapis.com/";
    public static final String WODES_API_ENDPOINT = "endpoint";
}
