package com.svyd.wodes.data.authorization;


import com.svyd.wodes.presentation.project.authorization.model.EmailWrapper;
import com.svyd.wodes.presentation.project.authorization.model.OauthRequest;
import com.svyd.wodes.presentation.project.authorization.model.SignInRequest;
import com.svyd.wodes.presentation.project.authorization.model.CodeWrapper;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import retrofit.http.GET;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Svyd on 18.08.2016.
 */
public interface AuthService {

    @POST("users/sendAuthenticationCode")
    Observable<ResponseModel> sendPhoneNumber(@Body PhoneWrapper phone);

    @POST("users/checkAuthenticationCode")
    Observable<ResponseModel> sendCode(@Body CodeWrapper code);

    @POST("users/signUp")
    Observable<ProfileInfo> signUp(@Body SignUpRequest request);

    @POST("users/signIn")
    Observable<ProfileInfo> signIn(@Body SignInRequest request);

    @POST("users/auth/{network}")
    Observable<ProfileInfo> signIn(@Body OauthRequest request, @Path("network") String network);

    @POST("users/forgotPassword")
    Observable<ResponseModel> forgotPassword(@Body EmailWrapper email);

    @GET("users/profile")
    Observable<ProfileInfo> getProfile();
}
