package com.svyd.wodes.data.size;

import com.svyd.wodes.presentation.project.delivery.model.SizeModel;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 09.09.2016.
 */
public interface ISizesCache {
    Observable<List<SizeModel>> getSizes();
    Observable<List<SizeModel>> cacheSizes(List<SizeModel> sizes);
}
