package com.svyd.wodes.data.profile.repository;

import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.profile.cache.ProfileCache;
import com.svyd.wodes.data.profile.mapper.ProfileMapper;

import rx.Observable;

/**
 * Created by Svyd on 04.07.2016.
 */
public class StubProfileRepositoryImpl implements IProfileRepository {

    final String stubAvatar = "http://hdwallpaperbackgrounds.net/wp-content/uploads/2016/06/Beautiful-Girl-Wallpaper-6.jpg";
    final String stubNotCurrentAvatar = "https://pp.vk.me/c626919/v626919206/8d7d/mWEvzvVmhas.jpg";

    private ProfileCache mCache;
    private TypeMapper<ProfileInfo, ProfileInfo> mMapper;

    public StubProfileRepositoryImpl(ProfileCache cache) {
        mCache = cache;
        mMapper = new ProfileMapper();
    }

    @Override
    public Observable<ProfileInfo> getCurrentProfile() {
        return Observable.merge(getLocal(), getRemote()
                .map(mMapper::map)
                .flatMap(mCache::cacheProfile));
    }

    @Override
    public Observable<ProfileInfo> getProfile(String id) {
        return getNotCurrent().map(mMapper::map);
    }

    @Override
    public Observable<ResponseModel> logout() {
        return Observable.just(new ResponseModel("stub"));
    }

    Observable<ProfileInfo> getNotCurrent() {
        return Observable.create(subscriber -> {
            try {
                ProfileInfo.Builder builder = new ProfileInfo.Builder();
                subscriber.onNext(builder
                        .setAvatar(stubNotCurrentAvatar)
                        .setName("Andrew Svyd")
                        .setRating(2)
                        .setCurrent(true)
                        .setInvites(3)
                        .setBirthday("29.07.1995")
                        .setPending(4)
                        .setMyDeliveries(6)
                        .setMyParcels(4)
                        .setPhoneEnabled(false)
                        .setPhone("(050) 123 3263")
                        .setEmail("andrew.svyd@gmail.com")
                        .setGender("Male")
                        .setCurrent(false)
                        .build());
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }


    Observable<ProfileInfo> getLocal() {
        return mCache.getProfile();
    }

    Observable<ProfileInfo> getRemote() {
        return Observable.create(subscriber -> {
            try {
                ProfileInfo.Builder builder = new ProfileInfo.Builder();
                subscriber.onNext(builder
                        .setAvatar(stubAvatar)
                        .setName("Sofiya Ali")
                        .setRating(4)
                        .setCurrent(true)
                        .setInvites(5)
                        .setBirthday("29.09.1991")
                        .setPending(6)
                        .setPhoneEnabled(true)
                        .setMyDeliveries(2)
                        .setMyParcels(7)
                        .setPhone("(050) 823 2346")
                        .setEmail("eva.smith@gmail.com")
                        .setGender("Female")
                        .build());
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }
}
