package com.svyd.wodes.data.delivery;

import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.CreateDeliveryRequest;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.delivery.model.SizeModel;

import java.util.List;

import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Svyd on 24.08.2016.
 */
public interface IDeliveryRepository {
    Observable<List<DeliveryItem>> getDeliveries(ListRequest request);
    Observable<List<DeliveryItem>> findDeliveries(SearchRequest request);
    Observable<DeliveryItem> createDelivery(CreateDeliveryRequest request);
    Observable<DeliveryItem> editDelivery(CreateDeliveryRequest request);
    Observable<ResponseModel> updateVisits(String id);

}
