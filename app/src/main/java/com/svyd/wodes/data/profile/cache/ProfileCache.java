package com.svyd.wodes.data.profile.cache;

import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observable;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface ProfileCache {
    Observable<ProfileInfo> getProfile();
    Observable<ProfileInfo> cacheProfile(ProfileInfo info);
    Observable<ResponseModel> clear();
}
