package com.svyd.wodes.data.parcel.mapper;

import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 13.09.2016.
 */
public class ParcelsMapper implements TypeMapper<DataWrapper<List<ParcelEntity>>, List<ParcelItem>> {

    private TypeMapper<ParcelEntity, ParcelItem> mMapper;

    public ParcelsMapper(TypeMapper<ParcelEntity, ParcelItem> mapper) {
        mMapper = mapper;
    }

    @Override
    public List<ParcelItem> map(DataWrapper<List<ParcelEntity>> target) {
        List<ParcelEntity> entities = target.getData();
        List<ParcelItem> items = new ArrayList<>();
        for (ParcelEntity entity: entities) {
            if (entity.getSize() == null) {
                continue;
            }
            ParcelItem item = mMapper.map(entity);
            items.add(item);
        }
        return items;
    }
}
