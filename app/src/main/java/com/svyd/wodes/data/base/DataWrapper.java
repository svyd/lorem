package com.svyd.wodes.data.base;

/**
 * Created by Svyd on 08.09.2016.
 */
public class DataWrapper<T> {
    private int count;
    private T data;

    public int getCount() {
        return count;
    }

    public T getData() {
        return data;
    }
}
