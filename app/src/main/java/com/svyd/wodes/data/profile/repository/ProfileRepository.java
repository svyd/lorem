package com.svyd.wodes.data.profile.repository;

import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.net.retrofit.HttpClientWrapper;
import com.svyd.wodes.data.profile.cache.ProfileCache;
import com.svyd.wodes.data.profile.mapper.ProfileMapper;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observable;

/**
 * Created by Svyd on 05.09.2016.
 */
public class ProfileRepository implements IProfileRepository {

    private ProfileService mService;
    private ProfileCache mCache;
    private TypeMapper<ProfileInfo, ProfileInfo> mMapper;

    public ProfileRepository(ProfileService service, ProfileCache cache) {
        mService = service;
        mCache = cache;
        mMapper = new ProfileMapper();
    }

    @Override
    public Observable<ProfileInfo> getCurrentProfile() {
        return mService.getProfile()
                .map(mMapper::map)
                .flatMap(mCache::cacheProfile);
    }

    @Override
    public Observable<ProfileInfo> getProfile(String id) {
        return mService.getProfile(id);
    }

    Observable<ResponseModel> clearCookies() {
        return Observable.create(subscriber -> {
            try {
                HttpClientWrapper.getInstance(WodesApplication.getContext()).clearCookies();
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<ResponseModel> logout() {
        return mService.logout()
                .flatMap(responseModel -> clearCookies())
                .flatMap(response -> mCache.clear());

    }

}
