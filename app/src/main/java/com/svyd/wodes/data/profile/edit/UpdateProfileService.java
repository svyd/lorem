package com.svyd.wodes.data.profile.edit;

import com.svyd.wodes.data.profile.edit.model.UpdateProfileRequest;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import rx.Observable;

/**
 * Created by Svyd on 30.08.2016.
 */
public interface UpdateProfileService {

    @POST("users/sendAuthenticationCode")
    Observable<ResponseModel> sendPhoneNumber(@Body PhoneWrapper phone);

    @PUT("users/profile")
    Observable<ProfileInfo> updateProfileInfo(@Body UpdateProfileRequest request);

    @Multipart
    @PUT("users/avatar")
    Observable<ResponseModel> updateAvatar(@Part("avatar\"; filename=\"avatar.jpg") RequestBody _file);
}
