package com.svyd.wodes.data.size;

import com.svyd.wodes.presentation.project.delivery.model.SizeModel;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Svyd on 15.09.2016.
 */
public interface SizeService {
    @GET("sizes")
    Observable<List<SizeModel>> getSizes();
}
