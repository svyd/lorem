package com.svyd.wodes.data.parcel.repository;

import com.google.gson.Gson;
import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.net.global.ApiConstants;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.base.tool.BitmapReducer;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelEntity;
import com.svyd.wodes.presentation.project.parcel.model.ParcelRequest;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by Svyd on 13.09.2016.
 */
public class ParcelRepository implements IParcelRepository {

    private ParcelService mService;
    private Gson mGson;
    private TypeMapper<DataWrapper<List<ParcelEntity>>, List<ParcelItem>> mParcelsMapper;
    private TypeMapper<ParcelEntity, ParcelItem> mParcelMapper;
    private BitmapReducer mBitmapReducer;

    public ParcelRepository(Gson gson,
                            ParcelService service,
                            TypeMapper<DataWrapper<List<ParcelEntity>>, List<ParcelItem>> mapper,
                            TypeMapper<ParcelEntity, ParcelItem> parcelMapper) {
        mService = service;
        mParcelsMapper = mapper;
        mParcelMapper = parcelMapper;
        mGson = gson;
        mBitmapReducer = new BitmapReducer();
    }

    @Override
    public Observable<List<ParcelItem>> getParcels(ListRequest request) {
        return mService.getParcels(request.getType(), request.getLimit(), request.getOffset())
                .map(mParcelsMapper::map);
    }

    @Override
    public Observable<List<ParcelItem>> findParcels(SearchRequest request) {
        return mService.findParcels(
                request.getLimit(),
                request.getOffset(),
                request.getDeparture().getLat(),
                request.getDeparture().getLon(),
                request.getArrival().getLat(),
                request.getArrival().getLon()
        ).map(mParcelsMapper::map);
    }

    @Override
    public Observable<ParcelItem> createParcel(ParcelRequest request) {

        String from = mGson.toJson(request.getFrom());
        String to = mGson.toJson(request.getTo());

        return mService.createParcel(getBody(from),
                getBody(to),
                getBody(request.getTitle()),
                getBody(request.getDescription()),
                getBody(request.getStartDate()),
                getBody(request.getSize()),
                getBody(request.getPrice()),
                getImage(request.getPhoto()))
                .map(mParcelMapper::map);
    }

    @Override
    public Observable<ParcelItem> editParcel(ParcelRequest request) {
        return mService.editParcel(request.getId(),
                getBody(request.getTitle()),
                getBody(request.getDescription()),
                getBody(request.getStartDate()),
                getBody(request.getSize()),
                getBody(request.getPrice()),
                getImage(request.getPhoto()))
                .map(mParcelMapper::map);
    }

    @Override
    public Observable<ResponseModel> updateViews(String id) {
        return mService.updateViews(id);
    }

    private RequestBody getImage(String filePath) {
        File imageFile = null;
        RequestBody image = null;

        if (filePath != null) {
            imageFile = new File(filePath);
        }

        if (filePath != null) {
            imageFile = mBitmapReducer.getReducedBitmap(filePath, 768);
        }

        if (imageFile != null && imageFile.exists()) {
            image = getBody(imageFile);
        }

        return image;
    }

    private RequestBody getBody(File file) {
        return RequestBody.create(MediaType.parse(ApiConstants.MULTIPART), file);
    }

    private RequestBody getBody(String param) {
        if (param != null) {
            return RequestBody.create(MediaType.parse(ApiConstants.MULTIPART), param);
        } else {
            return null;
        }
    }
}
