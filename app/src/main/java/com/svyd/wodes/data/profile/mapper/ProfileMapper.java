package com.svyd.wodes.data.profile.mapper;

import android.content.Context;
import android.media.Ringtone;

import com.svyd.wodes.R;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.tool.DateTimeUtility;
import com.svyd.wodes.presentation.base.tool.WordUtils;
import com.svyd.wodes.presentation.global.Constants;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;
import com.svyd.wodes.presentation.project.profile.model.ProfileItem;
import com.svyd.wodes.data.base.TypeMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 04.07.2016.
 */
public class ProfileMapper implements TypeMapper<ProfileInfo, ProfileInfo> {

    private List<ProfileItem> getItems(ProfileInfo info) {
        List<ProfileItem> items = new ArrayList<>();
        Context context = WodesApplication.getContext();

        ProfileItem.Builder invites = new ProfileItem.Builder();
        invites.setIcon(R.drawable.ic_invite);
        invites.setClickable(info.isCurrent());
        invites.setHasDivider(false);
        invites.setTitle(context.getString(R.string.invites_title));
        invites.setSubtitle(String.valueOf(info.getInvites()));
        invites.setMeta(Constants.ProfileItems.INVITES);
        items.add(invites.build());

        ProfileItem.Builder pending = new ProfileItem.Builder();
        pending.setIcon(R.drawable.ic_pending);
        pending.setClickable(info.isCurrent());
        pending.setHasDivider(false);
        pending.setTitle(context.getString(R.string.pending_title));
        pending.setSubtitle(String.valueOf(info.getPending()));
        pending.setMeta(Constants.ProfileItems.PENDING);
        items.add(pending.build());


        ProfileItem.Builder delivery = new ProfileItem.Builder();
        delivery.setIcon(R.drawable.ic_deliv);
        delivery.setClickable(info.isCurrent());
        delivery.setHasDivider(false);
        delivery.setTitle(context.getString(info.isCurrent() ? R.string.my_delivery_title : R.string.delivery_title));
        delivery.setSubtitle(String.valueOf(info.getMyDeliveries()));
        delivery.setMeta(Constants.ProfileItems.DELIVERIES);
        items.add(delivery.build());

        ProfileItem.Builder parcel = new ProfileItem.Builder();
        parcel.setIcon(R.drawable.ic_parcels);
        parcel.setClickable(info.isCurrent());
        parcel.setHasDivider(false);
        parcel.setTitle(context.getString(info.isCurrent() ? R.string.my_parcel_title : R.string.parcel_title));
        parcel.setSubtitle(String.valueOf(info.getMyParcels()));
        parcel.setMeta(Constants.ProfileItems.PARCELS);
        items.add(parcel.build());

        if (info.getPhone() != null) {

            ProfileItem.Builder number = new ProfileItem.Builder();
            number.setIcon(R.drawable.ic_phone);
            number.setClickable(false);
            number.setHasDivider(true);
            number.setTitle(hideLastSymbols(info.getPhone(), !info.isCurrent()));
            number.setSubtitle(context.getString(R.string.phone_title));
            number.setMeta(Constants.ProfileItems.NUMBER);
            items.add(number.build());
        }

        if (info.getEmail() != null) {
            ProfileItem.Builder email = new ProfileItem.Builder();
            email.setIcon(R.drawable.ic_email);
            email.setClickable(false);
            email.setHasDivider(false);
            email.setTitle(info.getEmail());
            email.setSubtitle(context.getString(R.string.email_title));
            email.setMeta(Constants.ProfileItems.EMAIL);
            items.add(email.build());
        }

        if (info.getBirthday() != null && !info.getBirthday().equals("")) {
            ProfileItem.Builder birthday = new ProfileItem.Builder();
            birthday.setIcon(R.drawable.ic_birthday);
            birthday.setClickable(false);
            birthday.setHasDivider(false);
            birthday.setTitle(DateTimeUtility.formatDate(info.getBirthday(), false));
            birthday.setSubtitle(context.getString(R.string.birthday_title));
            birthday.setMeta(Constants.ProfileItems.BIRTHDAY);
            items.add(birthday.build());
        }

        if (info.getGender() != null && !info.getGender().equals("")) {
            ProfileItem.Builder gender = new ProfileItem.Builder();
            gender.setIcon(R.drawable.ic_male);
            gender.setClickable(false);
            gender.setHasDivider(false);
            gender.setTitle(WordUtils.capitalize(info.getGender()));
            gender.setSubtitle(context.getString(R.string.gender_title));
            gender.setMeta(Constants.ProfileItems.GENDER);
            items.add(gender.build());
        }

        if (info.getCreatedAt() != null && !info.getCreatedAt().equals("")) {
            ProfileItem.Builder birthday = new ProfileItem.Builder();
            birthday.setIcon(R.drawable.ic_birthday);
            birthday.setClickable(false);
            birthday.setHasDivider(false);
            birthday.setTitle(DateTimeUtility.formatDate(info.getCreatedAt(), false));
            birthday.setSubtitle(context.getString(R.string.created_at_title));
            birthday.setMeta(Constants.ProfileItems.CREATED_AT);
            items.add(birthday.build());
        }

        if (info.isCurrent()) {

            ProfileItem.Builder logout = new ProfileItem.Builder();
            logout.setIcon(R.drawable.vector_logout);
            logout.setClickable(true);
            logout.setHasDivider(true);
            logout.setHasOnlyTitle(true);
            logout.setTitle(WodesApplication.getStringResource(R.string.sign_out));
            logout.setMeta(Constants.ProfileItems.LOGOUT);
            items.add(logout.build());
        }

        return items;
    }

    private String hideLastSymbols(String number, boolean hide) {
        if (hide) {
            number = number.substring(0, number.length() - 3) + "***";
        }
        return number;
    }

    @Override
    public ProfileInfo map(ProfileInfo target) {
        List<ProfileItem> items = getItems(target);
        target.setItems(items);
        return target;
    }
}
