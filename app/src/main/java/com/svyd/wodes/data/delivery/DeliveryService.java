package com.svyd.wodes.data.delivery;

import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.delivery.model.CreateDeliveryRequest;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryEntity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.model.SizeModel;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Svyd on 08.09.2016.
 */
public interface DeliveryService {

    @POST("deliveries")
    Observable<DeliveryEntity> createDelivery(@Body CreateDeliveryRequest request);

    @GET("deliveries/get/{type}")
    Observable<DataWrapper<List<DeliveryEntity>>> getDeliveries(
            @Path("type") String type,
            @Query("limit") int limit,
            @Query("offset") int offset);

    @PUT("deliveries/{id}")
    Observable<DeliveryEntity> editDelivery(@Path("id") String id, @Body CreateDeliveryRequest request);

    @PUT("deliveries/visit/{id}")
    Observable<ResponseModel> updateVisits(@Path("id") String id);

    @GET("deliveries/find/")
    Observable<DataWrapper<List<DeliveryEntity>>> findDeliveries(@Query("limit") int limit,
                                                                 @Query("offset") int offset,
                                                                 @Query("from[]") double departureLat,
                                                                 @Query("from[]") double departureLon,
                                                                 @Query("to[]") double arrivalLat,
                                                                 @Query("to[]") double arrivalLon);
}
