package com.svyd.wodes.data.places;

import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.presentation.project.delivery.model.LocationModel;
import com.svyd.wodes.presentation.project.delivery.model.SimpleLocationModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 10.09.2016.
 */
public class PlacesDetilesMapper implements TypeMapper<List<LocationModel>,List<SimpleLocationModel>> {
    @Override
    public List<SimpleLocationModel> map(List<LocationModel> target) {
        List<SimpleLocationModel> result = new ArrayList<>();

        for (LocationModel location: target) {
            result.add(new SimpleLocationModel
                    .Builder()
                    .setCity(location.getCity())
                    .setCountry(location.getCountry())
                    .setLocation(location.getAsLatLon())
                    .build());
        }

        return result;
    }
}
