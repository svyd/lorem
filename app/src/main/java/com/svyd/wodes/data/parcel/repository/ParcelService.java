package com.svyd.wodes.data.parcel.repository;

import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryEntity;
import com.svyd.wodes.presentation.project.parcel.model.ParcelEntity;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Svyd on 13.09.2016.
 */
public interface ParcelService {
    @GET("parcels/get/{type}")
    Observable<DataWrapper<List<ParcelEntity>>> getParcels(
            @Path("type") String type,
            @Query("limit") int limit,
            @Query("offset") int offset);

    @PUT("parcels/visit/{id}")
    Observable<ResponseModel> updateViews(@Path("id") String id);

    @Multipart
    @POST("parcels")
    Observable<ParcelEntity> createParcel(@Part("from") RequestBody from,
                                          @Part("to") RequestBody to,
                                          @Part("title") RequestBody title,
                                          @Part("description") RequestBody description,
                                          @Part("startDate") RequestBody startDate,
                                          @Part("size") RequestBody size,
                                          @Part("price") RequestBody price,
                                          @Part("image\"; filename=\"image.jpg") RequestBody _file);

    @Multipart
    @PUT("parcels/{id}")
    Observable<ParcelEntity> editParcel(@Path("id") String id,
                                        @Part("title") RequestBody title,
                                        @Part("description") RequestBody description,
                                        @Part("startDate") RequestBody startDate,
                                        @Part("size") RequestBody size,
                                        @Part("price") RequestBody price,
                                        @Part("image\"; filename=\"image.jpg") RequestBody _file);


    @GET("parcels/find/")
    Observable<DataWrapper<List<ParcelEntity>>> findParcels(@Query("limit") int limit,
                                                          @Query("offset") int offset,
                                                          @Query("from[]") double departureLat,
                                                          @Query("from[]") double departureLon,
                                                          @Query("to[]") double arrivalLat,
                                                          @Query("to[]") double arrivalLon);
}
