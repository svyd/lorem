package com.svyd.wodes.data.base;

/**
 * Created by Svyd on 23.08.2016.
 */
public interface Provider<T> {
    public T get();
}
