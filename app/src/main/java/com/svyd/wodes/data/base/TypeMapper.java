package com.svyd.wodes.data.base;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface TypeMapper<T, V> {
    V map(T target);
}
