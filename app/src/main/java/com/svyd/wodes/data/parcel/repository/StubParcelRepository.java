package com.svyd.wodes.data.parcel.repository;

import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelRequest;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 06.07.2016.
 */
public class StubParcelRepository implements IParcelRepository {

    @Override
    public Observable<List<ParcelItem>> getParcels(ListRequest request) {
        return getStubList();
    }

    Observable<List<ParcelItem>> getStubList() {
        return Observable.create(subscriber -> {
            try {
                ParcelItem item = getStub();

                List<ParcelItem> items = new ArrayList<>();
                for (int i = 0; i < 10; i++) {
                    items.add(item);
                }
                subscriber.onNext(items);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<List<ParcelItem>> findParcels(SearchRequest request) {
        return getStubList();
    }

    @Override
    public Observable<ParcelItem> createParcel(ParcelRequest request) {
        return Observable.just(getStub());
    }

    @Override
    public Observable<ParcelItem> editParcel(ParcelRequest request) {
        return Observable.just(getStub());
    }

    @Override
    public Observable<ResponseModel> updateViews(String id) {
        return Observable.just(new ResponseModel("stub"));
    }

    ParcelItem getStub() {
        return new ParcelItem.Builder()
                .setFrom("Uzhgorod")
                .setTo("Mukachevo")
                .setTitle("Goat")
                .setTime("5 hours ago")
                .setDeliveryTime("2 hours")
                .setSize("medium")
                .setViews("12")
                .build();
    }
}
