package com.svyd.wodes.data.places;

import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.presentation.project.delivery.model.LocationModel;

/**
 * Created by Svyd on 10.09.2016.
 */
public class LocationMapper implements TypeMapper<LocationModel, LocationModel> {
    @Override
    public LocationModel map(LocationModel target) {
        for (LocationModel.Result.AddressComponent component : target.result.mListAdressComponent) {
            for (String type : component.mTypes) {
                if (type.equals("country")) {
                    target.setCountry(component.shortName);
                }

                if (type.equals("locality")) {
                    target.setCity(component.longName);
                }
            }
        }
        return target;
    }
}
