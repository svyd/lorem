package com.svyd.wodes.data.delivery;

import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.size.ISizesCache;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.CreateDeliveryRequest;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryEntity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 24.08.2016.
 */
public class DeliveryRepository implements IDeliveryRepository {

    private DeliveryService mService;
    private TypeMapper<DataWrapper<List<DeliveryEntity>>, List<DeliveryItem>> mDeliveriesMapper;
    private TypeMapper<DeliveryEntity, DeliveryItem> mDeliveryMapper;
    private ISizesCache mSizeCache;

    public DeliveryRepository(DeliveryService service,
                              ISizesCache cache,
                              TypeMapper<DeliveryEntity, DeliveryItem> deliveryMapper,
                              TypeMapper<DataWrapper<List<DeliveryEntity>>, List<DeliveryItem>> deliveriesMapper) {
        mService = service;
        mSizeCache = cache;
        mDeliveriesMapper = deliveriesMapper;
        mDeliveryMapper = deliveryMapper;
    }

    @Override
    public Observable<List<DeliveryItem>> getDeliveries(ListRequest request) {

        return mService.getDeliveries(request.getType(), request.getLimit(), request.getOffset())
                .map(mDeliveriesMapper::map);
    }

    @Override
    public Observable<List<DeliveryItem>> findDeliveries(SearchRequest request) {
        return mService.findDeliveries(
                request.getLimit(),
                request.getOffset(),
                request.getDeparture().getLat(),
                request.getDeparture().getLon(),
                request.getArrival().getLat(),
                request.getArrival().getLon()
        ).map(mDeliveriesMapper::map);
    }

    @Override
    public Observable<DeliveryItem> createDelivery(CreateDeliveryRequest request) {
        return mService.createDelivery(request)
                .map(mDeliveryMapper::map);
    }

    @Override
    public Observable<DeliveryItem> editDelivery(CreateDeliveryRequest request) {
        return mService.editDelivery(request.getId(), request)
                .map(mDeliveryMapper::map);
    }

    @Override
    public Observable<ResponseModel> updateVisits(String id) {
        return mService.updateVisits(id);
    }
}
