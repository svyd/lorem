package com.svyd.wodes.data.profile.repository;

import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Svyd on 05.09.2016.
 */
public interface ProfileService {

    @GET("users/profile")
    Observable<ProfileInfo> getProfile();

    @GET("users/signOut")
    Observable<ResponseModel> logout();

    @GET("users/profile/{id}")
    Observable<ProfileInfo> getProfile(@Path("id") String id);


}
