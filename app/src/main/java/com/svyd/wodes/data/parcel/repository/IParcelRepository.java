package com.svyd.wodes.data.parcel.repository;

import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.global.model.SearchRequest;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelRequest;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 06.07.2016.
 */
public interface IParcelRepository {
    Observable<List<ParcelItem>> getParcels(ListRequest request);
    Observable<List<ParcelItem>> findParcels(SearchRequest request);
    Observable<ParcelItem> createParcel(ParcelRequest request);
    Observable<ParcelItem> editParcel(ParcelRequest request);
    Observable<ResponseModel> updateViews(String id);
}
