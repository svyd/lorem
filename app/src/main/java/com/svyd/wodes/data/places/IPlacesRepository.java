package com.svyd.wodes.data.places;

import com.svyd.wodes.presentation.project.delivery.model.LocationModel;
import com.svyd.wodes.presentation.project.delivery.model.PredictionsModel;
import com.svyd.wodes.presentation.project.delivery.model.SimpleLocationModel;

import java.util.List;

import rx.Observable;
import rx.Observer;

/**
 * Created by Svyd on 09.09.2016.
 */
public interface IPlacesRepository {
    Observable<PredictionsModel> getAddressList(String _query);
    Observable<LocationModel> getPlaceDetails(String _placeReference);
    Observable<List<SimpleLocationModel>> getPlacesDetails(List<String> references);
}
