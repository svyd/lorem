package com.svyd.wodes.data.size;

import com.svyd.wodes.presentation.project.delivery.model.SizeModel;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 15.09.2016.
 */
public interface ISizeRepository {
    Observable<List<SizeModel>> getSizes();
}
