package com.svyd.wodes.data.delivery;

import com.google.android.gms.maps.model.LatLng;
import com.svyd.wodes.R;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.base.tool.DateTimeUtility;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryEntity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 10.09.2016.
 */
@SuppressWarnings("Convert2streamapi")
public class DeliveryMapper implements TypeMapper<DeliveryEntity, DeliveryItem> {

    private TypeMapper<ProfileInfo, ProfileInfo> mProfileMapper;

    public DeliveryMapper(TypeMapper<ProfileInfo, ProfileInfo> mapper) {
        mProfileMapper = mapper;
    }

    @Override
    public DeliveryItem map(DeliveryEntity entity) {
        DeliveryItem item = new DeliveryItem();

        if (entity.getSize() == null){
            item.setAvailable(WodesApplication.getStringResource(R.string.unknown));
        } else {
            item.setAvailable(entity.getSize().getName());
        }
        if (entity.getOwner() != null) {
            item.setAvatar(entity.getOwner().getAvatar());
            item.setCarrier(entity.getOwner().getName());
            entity.setOwner(mProfileMapper.map(entity.getOwner()));
        }
        item.setEntity(entity);
        item.setFrom(entity.getFromLocation().getCity());
        item.setTo(entity.getToLocation().getCity());
        item.setStart(DateTimeUtility.formatDate(entity.getStartDate(), true));
        item.setViews(String.valueOf(entity.getVisits()));
        item.setTime("5m ago");

        return item;
    }
}
