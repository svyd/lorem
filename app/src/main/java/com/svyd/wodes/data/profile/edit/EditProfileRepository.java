package com.svyd.wodes.data.profile.edit;

import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.profile.edit.model.UpdateProfileRequest;
import com.svyd.wodes.data.profile.mapper.ProfileMapper;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.base.tool.BitmapReducer;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by Svyd on 30.08.2016.
 */
public class EditProfileRepository implements IEditProfileRepository {

    private final TypeMapper<ProfileInfo, ProfileInfo> mMapper;
    private UpdateProfileService mService;
    private BitmapReducer mBitmapReducer;

    public EditProfileRepository(UpdateProfileService service) {
        mService = service;
        mMapper = new ProfileMapper();
        mBitmapReducer = new BitmapReducer();
    }

    @Override
    public Observable<ProfileInfo> updateProfile(UpdateProfileRequest request) {

        File avatarFile = null;
        RequestBody avatar = null;

        String file = request.getAvatar();
        if (file != null) {
            avatarFile = mBitmapReducer.getReducedBitmap(request.getAvatar(), 768);
        }

        if (avatarFile != null && avatarFile.exists()) {
            avatar = RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
        }

        return mService.updateAvatar(avatar)
                .onErrorReturn(throwable -> null)
                .flatMap(responseModel1 -> sendPhone(request))
                .flatMap(responseModel -> mService.updateProfileInfo(request))
                .map(mMapper::map);
    }

    private Observable<ResponseModel> sendPhone(UpdateProfileRequest request) {
        if (request.getPhone() != null) {
            return mService.sendPhoneNumber(new PhoneWrapper(request.getPhone()));
        } else {
            return Observable.just(new ResponseModel("skipped!"));
        }
    }
}
