package com.svyd.wodes.data.authorization;

import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.data.profile.cache.ProfileCache;
import com.svyd.wodes.data.profile.mapper.ProfileMapper;
import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.authorization.model.CodeWrapper;
import com.svyd.wodes.presentation.project.authorization.model.EmailWrapper;
import com.svyd.wodes.presentation.project.authorization.model.OauthRequest;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;
import com.svyd.wodes.presentation.project.authorization.model.SignInRequest;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observable;

/**
 * Created by Svyd on 18.08.2016.
 */
public class AuthRepositoryImpl implements AuthRepository {

    private AuthService mService;
    private TypeMapper<ProfileInfo, ProfileInfo> mProfileMapper;
    private ProfileCache mCache;

    public AuthRepositoryImpl(AuthService service, ProfileCache cache) {
        mService = service;
        mProfileMapper = new ProfileMapper();
        mCache = cache;
    }

    @Override
    public Observable<ResponseModel> sendPhoneNumber(PhoneWrapper phone) {
        return mService.sendPhoneNumber(phone);
    }

    @Override
    public Observable<ResponseModel> forgotPassword(EmailWrapper email) {
        return mService.forgotPassword(email);
    }

    @Override
    public Observable<ResponseModel> sendCode(CodeWrapper request) {
        return mService.sendCode(request);
    }

    @Override
    public Observable<ProfileInfo> signIn(SignInRequest request) {
        return mService.signIn(request)
                .map(mProfileMapper::map)
                .flatMap(mCache::cacheProfile);
    }

    @Override
    public Observable<ProfileInfo> signIn(OauthRequest request) {
        return mService.signIn(request, request.getNetwork())
                .map(mProfileMapper::map)
                .flatMap(mCache::cacheProfile);
    }

    @Override
    public Observable<ProfileInfo> signUp(SignUpRequest request) {
        return mService.sendCode(request.getCode())
                .flatMap(responseModel -> mService.signUp(request))
                .map(mProfileMapper::map)
                .flatMap(mCache::cacheProfile);
    }
}