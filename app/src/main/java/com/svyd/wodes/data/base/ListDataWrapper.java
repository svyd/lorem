package com.svyd.wodes.data.base;

import com.svyd.wodes.presentation.project.delivery.model.ListRequest;

import java.util.List;

/**
 * Created by Svyd on 23.09.2016.
 */
public class ListDataWrapper<T> {
    private int count;
    private List<T> data;

    public int getCount() {
        return count;
    }

    public List<T> getData() {
        return data;
    }
}
