package com.svyd.wodes.data.net.retrofit;

import com.svyd.wodes.data.net.global.ApiConstants;

import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

/**
 * Created by Svyd on 09.09.2016.
 */
public class GoogleApiRetrofitWrapper {

    private static GoogleApiRetrofitWrapper sInstance;

    private Retrofit mRetrofit;

    private GoogleApiRetrofitWrapper(OkHttpClient client, Converter.Factory factory) {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.GOOGLE_API_ENDPOINT)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(factory)
                .client(client)
                .build();
    }

    public static GoogleApiRetrofitWrapper getInstance(OkHttpClient client, Converter.Factory factory) {
        if (sInstance == null) {
            sInstance = new GoogleApiRetrofitWrapper(client, factory);
        }
        return sInstance;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

}
