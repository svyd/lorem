package com.svyd.wodes.data.profile.repository;

import com.svyd.wodes.domain.base.ResponseModel;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observable;

/**
 * Created by Svyd on 04.07.2016.
 */
public interface IProfileRepository {
    Observable<ProfileInfo> getCurrentProfile();
    Observable<ProfileInfo> getProfile(String id);
    Observable<ResponseModel> logout();
}
