package com.svyd.wodes.data.net.cookie;

import java.io.IOException;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Svyd on 18.08.2016.
 */
public class AddCookieInterceptor implements Interceptor {

    private CookieStore mCookieStore;

    public AddCookieInterceptor(CookieStore _cookieStore) {
        mCookieStore = _cookieStore;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        List<HttpCookie> cookies = mCookieStore.getCookies();
        for (HttpCookie cookie : cookies) {
            builder.addHeader("Cookie", cookie.getValue());
        }

        return chain.proceed(builder.build());
    }
}