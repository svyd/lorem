package com.svyd.wodes.data.places;

import com.svyd.wodes.presentation.project.delivery.model.LocationModel;
import com.svyd.wodes.presentation.project.delivery.model.SimpleLocationModel;
import com.svyd.wodes.presentation.project.delivery.model.PredictionsModel;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Svyd on 09.09.2016.
 */
public interface GooglePlacesService {

    @GET("maps/api/place/autocomplete/json?")
    Observable<PredictionsModel> getPlacesPredictions(@Query("input") String _input, @Query("types") String types, @Query("key") String _key);

    @GET("maps/api/place/details/json?")
    Observable<LocationModel> getPlaceDetails(@Query("reference") String _reference, @Query("key") String _key);

}
