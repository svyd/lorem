package com.svyd.wodes.data.delivery;

import com.svyd.wodes.data.base.DataWrapper;
import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryEntity;
import com.svyd.wodes.presentation.project.delivery.model.DeliveryItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svyd on 08.09.2016.
 */
public class DeliveriesMapper implements TypeMapper<DataWrapper<List<DeliveryEntity>>, List<DeliveryItem>> {

    private TypeMapper<DeliveryEntity, DeliveryItem> mMapper;

    public DeliveriesMapper(TypeMapper<DeliveryEntity, DeliveryItem> mapper) {
        mMapper = mapper;
    }

    @Override
    public List<DeliveryItem> map(DataWrapper<List<DeliveryEntity>> target) {
        List<DeliveryItem> items = new ArrayList<>();
        if (target.getCount() > 0) {
            for (DeliveryEntity entity : target.getData()) {
                items.add(mMapper.map(entity));
            }
        }
        return items;
    }
}
