package com.svyd.wodes.data.profile.edit.model;

import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

/**
 * Created by Svyd on 30.08.2016.
 */
public class UpdateProfileRequest {

    public UpdateProfileRequest(ProfileInfo info) {
        email = info.getEmail();
        fullName = info.getName();
        phone = info.getPhone();
        gender = info.getGender();
        dob = info.getBirthday();
    }

    private String email;
    private String fullName;
    private String phone;
    private String gender;
    private String dob;
    private String avatar;

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setBirthday(String dob) {
        this.dob = dob;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }
}
