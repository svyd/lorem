package com.svyd.wodes.data.size;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.svyd.wodes.data.size.ISizesCache;
import com.svyd.wodes.presentation.application.WodesApplication;
import com.svyd.wodes.presentation.project.delivery.model.SizeModel;

import java.lang.reflect.Type;
import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 09.09.2016.
 */
public class PreferenceSizeCache implements ISizesCache {

    private final String CACHE_PREFERENCES_NAME = "sizes_cache";
    private final String SIZE_CACHE_KEY = "sizes";

    private SharedPreferences mPreferences;
    private Gson mGson;
    private Type mType;

    public PreferenceSizeCache() {
        mPreferences = WodesApplication
                .getContext()
                .getSharedPreferences(CACHE_PREFERENCES_NAME, Context.MODE_PRIVATE);

        mType = new TypeToken<List<SizeModel>>() {
        }.getType();
        mGson = new Gson();
    }

    @Override
    public Observable<List<SizeModel>> getSizes() {

        return Observable.create(subscriber -> {
            try {
                String json = mPreferences.getString(CACHE_PREFERENCES_NAME, "");
                if (!json.equals("")) {
                    List<SizeModel> info = mGson.fromJson(json, mType);
                    subscriber.onNext(info);
                    subscriber.onCompleted();
                }
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }

    @Override
    public Observable<List<SizeModel>> cacheSizes(List<SizeModel> sizes) {
        return Observable.create(subscriber -> {
            try {
                String json = mGson.toJson(sizes);
                mPreferences.edit()
                        .putString(SIZE_CACHE_KEY, json)
                        .commit();
                subscriber.onNext(sizes);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }
}
