package com.svyd.wodes.data.parcel.mapper;

import com.svyd.wodes.data.base.TypeMapper;
import com.svyd.wodes.presentation.base.tool.DateTimeUtility;
import com.svyd.wodes.presentation.project.parcel.model.ParcelItem;
import com.svyd.wodes.presentation.project.parcel.model.ParcelEntity;

/**
 * Created by Svyd on 13.09.2016.
 */
public class ParcelMapper implements TypeMapper<ParcelEntity, ParcelItem> {
    @Override
    public ParcelItem map(ParcelEntity target) {
        return new ParcelItem.Builder()
                .setEntity(target)
                .setDeliveryTime(DateTimeUtility.formatDate(target.getStartDate(), true))
                .setFrom(target.getFromLocation().getCity())
                .setTo(target.getToLocation().getCity())
                .setSize(target.getSize().getName())
                .setTime("5m ago")
                .setTitle(target.getTitle())
                .setViews(String.valueOf(target.getVisits()))
                .build();
    }
}
