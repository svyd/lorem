package com.svyd.wodes.data.profile.edit;

import com.svyd.wodes.data.profile.edit.model.UpdateProfileRequest;
import com.svyd.wodes.presentation.project.profile.model.ProfileInfo;

import rx.Observable;

/**
 * Created by Svyd on 30.08.2016.
 */
public interface IEditProfileRepository {
    Observable<ProfileInfo> updateProfile(UpdateProfileRequest info);
}
