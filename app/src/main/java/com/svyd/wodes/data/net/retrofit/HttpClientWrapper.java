package com.svyd.wodes.data.net.retrofit;

import android.content.Context;
import android.content.SharedPreferences;

import com.svyd.wodes.data.net.global.ApiConstants;
import com.svyd.wodes.data.net.cookie.AddCookieInterceptor;
import com.svyd.wodes.data.net.cookie.PersistentCookieStore;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.util.concurrent.TimeUnit;

import okhttp3.CookieJar;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Svyd on 18.08.2016.
 */
public class HttpClientWrapper {

    private static final String PREFERENCES_NAME_COOKIE = "cookie_preferences";
    private OkHttpClient mClient;
    private SharedPreferences mStore;

    private static HttpClientWrapper sInstance;

    public void clearCookies() {
        mStore.edit()
                .clear()
                .apply();
    }

    private HttpClientWrapper(Context context) {
        mStore = context.getSharedPreferences(PREFERENCES_NAME_COOKIE, Context.MODE_PRIVATE);
        CookieStore cookieStore = new PersistentCookieStore(mStore);
        CookieHandler handler = new CookieManager(cookieStore, CookiePolicy.ACCEPT_ALL);
        CookieJar jar = new JavaNetCookieJar(handler);
        Interceptor interceptor = new AddCookieInterceptor(cookieStore);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(ApiConstants.LOGGING_LEVEL);

        mClient = new OkHttpClient.Builder()
                .cookieJar(jar)
                .addInterceptor(interceptor)
                .addInterceptor(loggingInterceptor)
                .connectTimeout(ApiConstants.CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(ApiConstants.READ_TIME_OUT, TimeUnit.SECONDS)
                .build();
    }

    public static HttpClientWrapper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new HttpClientWrapper(context);
        }
        return sInstance;
    }

    public OkHttpClient getClient() {
        return mClient;
    }

}
