package com.svyd.wodes.data.size;

import com.svyd.wodes.presentation.project.delivery.model.SizeModel;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 15.09.2016.
 */
public class SizeRepository implements ISizeRepository {

    private SizeService mService;
    private ISizesCache mCache;

    public SizeRepository(SizeService service, ISizesCache cache) {
        mService = service;
        mCache = cache;
    }

    Observable<List<SizeModel>> getRemoteSizes() {
        return mService.getSizes()
                .flatMap(mCache::cacheSizes);
    }

    Observable<List<SizeModel>> getLocalSizes() {
        return mCache.getSizes();
    }

    @Override
    public Observable<List<SizeModel>> getSizes() {
        return getRemoteSizes();
//        return Observable.merge(getLocalSizes(), getRemoteSizes());
    }
}
