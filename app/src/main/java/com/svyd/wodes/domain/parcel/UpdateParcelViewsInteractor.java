package com.svyd.wodes.domain.parcel;

import com.svyd.wodes.data.parcel.repository.IParcelRepository;
import com.svyd.wodes.domain.base.PostInteractor;

import rx.Observable;

/**
 * Created by Svyd on 20.09.2016.
 */
public class UpdateParcelViewsInteractor extends PostInteractor<String> {

    IParcelRepository mRepository;

    public UpdateParcelViewsInteractor(IParcelRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.updateViews(_data);
    }
}
