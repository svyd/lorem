package com.svyd.wodes.domain.parcel;

import com.svyd.wodes.data.parcel.repository.IParcelRepository;
import com.svyd.wodes.data.places.IPlacesRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.parcel.model.ParcelRequest;

import java.util.Arrays;
import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 14.09.2016.
 */
public class CreateParcelInteractor extends PostInteractor<ParcelRequest> {

    private IPlacesRepository mPlacesRepository;
    private IParcelRepository mParcelRepository;

    public CreateParcelInteractor(IParcelRepository parcelRepository, IPlacesRepository placesRepository) {
        mParcelRepository = parcelRepository;
        mPlacesRepository = placesRepository;
    }

    @Override
    protected Observable buildPostObservable(ParcelRequest _data) {
        List<String> references = Arrays.asList(_data.getReferenceFrom(), _data.getReferenceTo());
        return mPlacesRepository.getPlacesDetails(references)
                .flatMap(locations -> {
                    _data.setFrom(locations.get(0));
                    _data.setTo(locations.get(1));
                    return mParcelRepository.createParcel(_data);
                });
    }
}
