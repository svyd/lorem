package com.svyd.wodes.domain.parcel;

import com.svyd.wodes.presentation.project.delivery.model.ListRequest;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.data.parcel.repository.IParcelRepository;

import rx.Observable;

/**
 * Created by Svyd on 06.07.2016.
 */
public class ParcelInteractor extends PostInteractor<ListRequest> {

    IParcelRepository mRepository;

    public ParcelInteractor(IParcelRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(ListRequest _data) {
        return mRepository.getParcels(_data);
    }
}
