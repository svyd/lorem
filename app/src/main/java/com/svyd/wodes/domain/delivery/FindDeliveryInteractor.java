package com.svyd.wodes.domain.delivery;

import com.svyd.wodes.data.delivery.IDeliveryRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.global.model.SearchRequest;

import rx.Observable;

/**
 * Created by Svyd on 20.09.2016.
 */
public class FindDeliveryInteractor extends PostInteractor<SearchRequest> {

    private IDeliveryRepository mRepository;

    public FindDeliveryInteractor(IDeliveryRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(SearchRequest _data) {
        return mRepository.findDeliveries(_data);
    }
}
