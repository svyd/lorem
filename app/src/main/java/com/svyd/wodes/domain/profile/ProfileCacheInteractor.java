package com.svyd.wodes.domain.profile;

import com.svyd.wodes.data.profile.cache.ProfileCache;
import com.svyd.wodes.domain.base.Interactor;

import rx.Observable;

/**
 * Created by Svyd on 05.09.2016.
 */
public class ProfileCacheInteractor extends Interactor {

    private ProfileCache mCache;

    public ProfileCacheInteractor(ProfileCache cache) {
        mCache = cache;
    }

    @Override
    protected Observable buildObserver() {
        return mCache.getProfile();
    }
}
