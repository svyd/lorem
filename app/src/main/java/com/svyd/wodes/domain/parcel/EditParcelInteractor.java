package com.svyd.wodes.domain.parcel;

import com.svyd.wodes.data.parcel.repository.IParcelRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.parcel.model.ParcelRequest;

import rx.Observable;

/**
 * Created by Svyd on 20.09.2016.
 */
public class EditParcelInteractor extends PostInteractor<ParcelRequest> {

    private IParcelRepository mRepository;

    public EditParcelInteractor(IParcelRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(ParcelRequest _data) {
        return mRepository.editParcel(_data);
    }
}
