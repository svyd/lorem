package com.svyd.wodes.domain.places;

import com.svyd.wodes.data.places.IPlacesRepository;
import com.svyd.wodes.domain.base.PostInteractor;

import rx.Observable;

/**
 * Created by Svyd on 09.09.2016.
 */
public class PredictionsInteractor extends PostInteractor<String> {

    private IPlacesRepository mRepository;

    public PredictionsInteractor(IPlacesRepository repository) {
        mRepository = repository;
    }


    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.getAddressList(_data);
    }
}
