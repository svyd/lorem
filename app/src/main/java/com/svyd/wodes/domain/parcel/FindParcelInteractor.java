package com.svyd.wodes.domain.parcel;

import com.svyd.wodes.data.parcel.repository.IParcelRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.global.model.SearchRequest;

import rx.Observable;

/**
 * Created by Svyd on 20.09.2016.
 */
public class FindParcelInteractor extends PostInteractor<SearchRequest> {

    private IParcelRepository mRepository;

    public FindParcelInteractor(IParcelRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(SearchRequest _data) {
        return mRepository.findParcels(_data);
    }
}
