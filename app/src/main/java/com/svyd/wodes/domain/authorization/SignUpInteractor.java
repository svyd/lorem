package com.svyd.wodes.domain.authorization;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.authorization.model.SignUpRequest;

import rx.Observable;

/**
 * Created by Svyd on 13.08.2016.
 */
public class SignUpInteractor extends PostInteractor<SignUpRequest> {

    private AuthRepository mRepository;

    public SignUpInteractor(AuthRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(SignUpRequest _data) {
        return mRepository.signUp(_data);
    }
}
