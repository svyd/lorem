package com.svyd.wodes.domain.authorization;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.authorization.model.OauthRequest;

import rx.Observable;

/**
 * Created by Svyd on 23.08.2016.
 */
public class OauthInteractor extends PostInteractor<OauthRequest> {

    private AuthRepository mRepository;

    public OauthInteractor(AuthRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(OauthRequest _data) {
        return mRepository.signIn(_data);
    }
}
