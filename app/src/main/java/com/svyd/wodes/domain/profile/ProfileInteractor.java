package com.svyd.wodes.domain.profile;

import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.data.profile.repository.IProfileRepository;

import rx.Observable;

/**
 * Created by Svyd on 05.07.2016.
 */
public class ProfileInteractor extends PostInteractor<String> {

    private IProfileRepository mRepository;

    public ProfileInteractor(IProfileRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.getProfile(_data);
    }

}
