package com.svyd.wodes.domain.profile;

import com.svyd.wodes.data.profile.repository.IProfileRepository;
import com.svyd.wodes.domain.base.Interactor;

import rx.Observable;

/**
 * Created by Svyd on 15.09.2016.
 */
public class LogoutInteractor extends Interactor {

    private IProfileRepository mRepository;

    public LogoutInteractor(IProfileRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildObserver() {
        return mRepository.logout();
    }
}
