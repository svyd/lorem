package com.svyd.wodes.domain.authorization;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.authorization.model.SignInRequest;

import rx.Observable;

/**
 * Created by Svyd on 23.08.2016.
 */
public class SignInInteractor extends PostInteractor<SignInRequest> {

    private AuthRepository mRepository;

    public SignInInteractor(AuthRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(SignInRequest _data) {
        return mRepository.signIn(_data);
    }
}
