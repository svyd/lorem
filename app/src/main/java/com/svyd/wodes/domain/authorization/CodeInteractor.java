package com.svyd.wodes.domain.authorization;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.authorization.model.CodeWrapper;

import rx.Observable;

/**
 * Created by Svyd on 24.08.2016.
 */
public class CodeInteractor extends PostInteractor<CodeWrapper> {

    private AuthRepository mRepository;

    public CodeInteractor(AuthRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(CodeWrapper _data) {
        return mRepository.sendCode(_data);
    }
}
