package com.svyd.wodes.domain.profile.edit;

import com.svyd.wodes.data.profile.edit.IEditProfileRepository;
import com.svyd.wodes.data.profile.edit.model.UpdateProfileRequest;
import com.svyd.wodes.domain.base.PostInteractor;

import rx.Observable;

/**
 * Created by Svyd on 30.08.2016.
 */
public class EditProfileInteractor extends PostInteractor<UpdateProfileRequest> {

    private IEditProfileRepository mRepository;

    public EditProfileInteractor(IEditProfileRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(UpdateProfileRequest _data) {
        return mRepository.updateProfile(_data);
    }
}
