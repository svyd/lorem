package com.svyd.wodes.domain.delivery;

import com.svyd.wodes.data.delivery.IDeliveryRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.delivery.model.CreateDeliveryRequest;

import rx.Observable;

/**
 * Created by Svyd on 17.09.2016.
 */
public class EditDeliveryInteractor extends PostInteractor<CreateDeliveryRequest> {

    private IDeliveryRepository mRepository;

    public EditDeliveryInteractor(IDeliveryRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(CreateDeliveryRequest _data) {
        return mRepository.editDelivery(_data);
    }
}
