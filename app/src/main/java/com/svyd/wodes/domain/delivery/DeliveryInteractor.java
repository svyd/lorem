package com.svyd.wodes.domain.delivery;

import com.svyd.wodes.data.delivery.IDeliveryRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.delivery.model.ListRequest;

import rx.Observable;

/**
 * Created by Svyd on 24.08.2016.
 */
public class DeliveryInteractor extends PostInteractor<ListRequest> {

    private IDeliveryRepository mRepository;

    public DeliveryInteractor(IDeliveryRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(ListRequest _data) {
        return mRepository.getDeliveries(_data);
    }
}
