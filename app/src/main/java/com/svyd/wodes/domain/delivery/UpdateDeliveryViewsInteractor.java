package com.svyd.wodes.domain.delivery;

import com.svyd.wodes.data.delivery.IDeliveryRepository;
import com.svyd.wodes.domain.base.PostInteractor;

import rx.Observable;

/**
 * Created by Svyd on 20.09.2016.
 */
public class UpdateDeliveryViewsInteractor extends PostInteractor<String> {

    private IDeliveryRepository mRepository;

    public UpdateDeliveryViewsInteractor(IDeliveryRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(String _data) {
        return mRepository.updateVisits(_data);
    }
}
