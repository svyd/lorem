package com.svyd.wodes.domain.base;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by Svyd on 04.07.2016.
 */
public abstract class PostInteractor<T> extends Interactor {

    private Subscription mPostSubscription = Subscriptions.empty();

    @SuppressWarnings("unchecked")
    public void execute(T _data, Observer _subscriber) {
        mPostSubscription = buildPostObservable(_data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(_subscriber);
    }

    protected abstract Observable buildPostObservable(T _data);

    @Override
    public void unSubscribe() {
        super.unSubscribe();
        if (!mPostSubscription.isUnsubscribed())
            mPostSubscription.unsubscribe();
    }

    @Override
    protected Observable buildObserver() {
        return null;
    }
}
