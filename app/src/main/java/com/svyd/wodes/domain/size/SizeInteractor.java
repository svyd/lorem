package com.svyd.wodes.domain.size;

import com.svyd.wodes.data.delivery.IDeliveryRepository;
import com.svyd.wodes.data.size.ISizeRepository;
import com.svyd.wodes.domain.base.Interactor;

import rx.Observable;

/**
 * Created by Svyd on 09.09.2016.
 */
public class SizeInteractor extends Interactor {

    private ISizeRepository mRepository;

    public SizeInteractor(ISizeRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildObserver() {
        return mRepository.getSizes();
    }
}
