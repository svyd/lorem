package com.svyd.wodes.domain.authorization;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.authorization.model.PhoneWrapper;

import rx.Observable;

/**
 * Created by Svyd on 10.08.2016.
 */
public class PhoneNumberInteractor extends PostInteractor<PhoneWrapper> {

    private AuthRepository mRepository;

    public PhoneNumberInteractor(AuthRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(PhoneWrapper _data) {
        return mRepository.sendPhoneNumber(_data);
    }
}
