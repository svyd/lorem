package com.svyd.wodes.domain.base;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Svyd on 10.08.2016.
 */
public class ResponseModel {

    public ResponseModel(String response) {
        this.response = response;
    }

    @SerializedName("success")
    public String response;
}
