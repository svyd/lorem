package com.svyd.wodes.domain.places;

import com.svyd.wodes.data.places.IPlacesRepository;
import com.svyd.wodes.domain.base.PostInteractor;

import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 10.09.2016.
 */
public class PlaceDetailsInteractor extends PostInteractor<List<String>> {

    private IPlacesRepository mRepository;

    public PlaceDetailsInteractor(IPlacesRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(List<String> _data) {
        return mRepository.getPlacesDetails(_data);
    }
}
