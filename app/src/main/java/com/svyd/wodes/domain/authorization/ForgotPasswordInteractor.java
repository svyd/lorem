package com.svyd.wodes.domain.authorization;

import com.svyd.wodes.data.authorization.AuthRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.authorization.model.EmailWrapper;

import rx.Observable;

/**
 * Created by Svyd on 24.08.2016.
 */
public class ForgotPasswordInteractor extends PostInteractor<EmailWrapper> {

    private AuthRepository mRepository;

    public ForgotPasswordInteractor(AuthRepository repository) {
        mRepository = repository;
    }

    @Override
    protected Observable buildPostObservable(EmailWrapper _data) {
        return mRepository.forgotPassword(_data);
    }
}
