package com.svyd.wodes.domain.delivery;

import com.svyd.wodes.data.delivery.IDeliveryRepository;
import com.svyd.wodes.data.places.IPlacesRepository;
import com.svyd.wodes.domain.base.PostInteractor;
import com.svyd.wodes.presentation.project.delivery.model.CreateDeliveryRequest;
import com.svyd.wodes.presentation.project.delivery.model.LocationModel;
import com.svyd.wodes.presentation.project.delivery.model.SimpleLocationModel;

import java.util.Arrays;
import java.util.List;

import rx.Observable;

/**
 * Created by Svyd on 10.09.2016.
 */
public class CreateDeliveryInteractor extends PostInteractor<CreateDeliveryRequest> {

    private IPlacesRepository mPlacesRepository;
    private IDeliveryRepository mDeliveryRepository;

    public CreateDeliveryInteractor(IPlacesRepository placesRepository, IDeliveryRepository deliveryRepository) {
        mPlacesRepository = placesRepository;
        mDeliveryRepository = deliveryRepository;
    }

    @Override
    protected Observable buildPostObservable(CreateDeliveryRequest _data) {
        List<String> references = Arrays.asList(_data.getReferenceFrom(), _data.getReferenceTo());
        return mPlacesRepository.getPlacesDetails(references)
                .flatMap(locations -> {
                    _data.setFrom(locations.get(0));
                    _data.setTo(locations.get(1));
                    return mDeliveryRepository.createDelivery(_data);
                });
    }
}
